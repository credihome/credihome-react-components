"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Tag = _interopRequireDefault(require("../Tag/Tag"));

var _Select = _interopRequireDefault(require("../Select/Select"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var TagSelectStyled = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: relative;\n\n  & .tag-row {\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-start;\n    flex-wrap: wrap;\n    align-items: center;\n    margin-left: -2px;\n    margin-top: calc(0.5rem - 2px);\n  }\n\n  & .tag-row > div {\n    margin: 8px 24px 0px 0px;\n  }\n"])));
/**
 * Componente: TagSelect<br>
 * Select onde é possível escolher multiplas opções.<br>
 * Tais opções aparecem abaixo do componente na forma de tag e podem ser deletadas.
 * @component
 */


function TagSelect(props) {
  var className = props.className,
      value = props.value,
      options = props.options,
      placeholder = props.placeholder,
      handleChange = props.handleChange,
      handleBlur = props.handleBlur,
      handleFocus = props.handleFocus,
      disabled = props.disabled,
      name = props.name,
      id = props.id,
      touched = props.touched,
      noOptionString = props.noOptionString,
      maxHeight = props.maxHeight,
      optionsLabel = props.optionsLabel,
      optionsKey = props.optionsKey,
      errors = props.errors,
      autocomplete = props.autocomplete,
      isValidating = props.isValidating,
      hasHideTooltip = props.hasHideTooltip;

  function separateValues() {
    var filterSelected = [];
    var filterNotSelected = [];
    options.forEach(function (option) {
      (value.some(function (v) {
        return v === option[optionsKey];
      }) ? filterSelected : filterNotSelected).push(option);
    });
    return {
      filterSelected: filterSelected,
      filterNotSelected: filterNotSelected
    };
  }

  var initialValues = separateValues();

  var _useState = (0, _react.useState)(value),
      _useState2 = _slicedToArray(_useState, 2),
      selected = _useState2[0],
      setSelected = _useState2[1];

  var _useState3 = (0, _react.useState)(initialValues.filterSelected),
      _useState4 = _slicedToArray(_useState3, 2),
      tags = _useState4[0],
      setTags = _useState4[1];

  var _useState5 = (0, _react.useState)(initialValues.filterNotSelected),
      _useState6 = _slicedToArray(_useState5, 2),
      notSelected = _useState6[0],
      setNotSelected = _useState6[1];

  var didMountRef = (0, _react.useRef)(false);
  (0, _react.useEffect)(function () {
    if (didMountRef.current && value !== selected) {
      var newValues = separateValues();
      setSelected(value);
      setTags(newValues.filterSelected);
      setNotSelected(newValues.filterNotSelected);
    }
  }, [value]);
  (0, _react.useEffect)(function () {
    if (!disabled && didMountRef.current) {
      handleChange(selected);
    } else if (!didMountRef.current) {
      didMountRef.current = true;
    }
  }, [selected]);

  function addTag(newTag) {
    var tag = notSelected.find(function (option) {
      return option[optionsKey] === newTag;
    });

    if (!disabled && tag !== undefined) {
      setSelected([].concat(_toConsumableArray(selected), [newTag]));
      setTags([].concat(_toConsumableArray(tags), [tag]));
      setNotSelected(notSelected.filter(function (option) {
        return option[optionsKey] !== newTag;
      }));
    }
  }

  function removeTag(tag, index) {
    setTags(tags.filter(function (_, i) {
      return i !== index;
    }));
    setSelected(selected.filter(function (v) {
      return v !== tag[optionsKey];
    }));
    setNotSelected([].concat(_toConsumableArray(notSelected), [tag]));
  }

  return /*#__PURE__*/_react.default.createElement(TagSelectStyled, {
    className: className
  }, /*#__PURE__*/_react.default.createElement(_Select.default, {
    options: notSelected,
    placeholder: placeholder,
    handleChange: function handleChange(v) {
      return addTag(v);
    },
    handleBlur: handleBlur,
    handleFocus: handleFocus,
    disabled: disabled,
    isValidating: isValidating,
    name: name,
    id: id,
    touched: touched,
    noOptionString: noOptionString,
    maxHeight: maxHeight,
    optionsLabel: optionsLabel,
    optionsKey: optionsKey,
    errors: errors,
    autocomplete: autocomplete,
    hasHideTooltip: hasHideTooltip
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: "tag-row"
  }, tags.map(function (tag, i) {
    return /*#__PURE__*/_react.default.createElement(_Tag.default, {
      text: tag[optionsLabel],
      disabled: disabled,
      key: tag[optionsKey],
      onClick: function onClick() {
        return removeTag(tag, i);
      }
    });
  })));
}

TagSelect.propTypes = {
  /**
   * Define as classes CSS do tag select
   */
  className: _propTypes.default.string,

  /**
   * Define o valor que o tag select deve assumir contanto que esse valor seja uma key no array de options
   */
  value: _propTypes.default.arrayOf(_propTypes.default.shape({
    label: _propTypes.default.string,
    key: _propTypes.default.string
  })),

  /**
   * Este atributo é um array de objetos, que deve possuir as seguintes propriedades:<br /> <b>label: string|required</b><br /><b>key: string|required</b>.
   */
  options: _propTypes.default.arrayOf(_propTypes.default.shape({
    label: _propTypes.default.string,
    key: _propTypes.default.string
  })).isRequired,

  /**
   * Este atributo deve receber uma string para exibir no placeholder do componente.
   */
  placeholder: _propTypes.default.string,

  /**
   * A função é chamada sempre que um evento de clique (seleção da opção) acontece.
   */
  handleChange: _propTypes.default.func,

  /**
   * A função é chamada sempre que o tag select é desfocado.
   */
  handleBlur: _propTypes.default.func,

  /**
   * A função é chamada sempre que o tag select é focado.
   */
  handleFocus: _propTypes.default.func,

  /**
   * Propriedade que define se o tag select está desabilitado ou não
   */
  disabled: _propTypes.default.bool,

  /**
   * Identificador do tag select
   */
  id: _propTypes.default.string,

  /**
   * Identificador/nome do tag select
   */
  name: _propTypes.default.string,

  /**
   * Objeto com os IDs dos campos do formulário que já foram clicados pelo usuário, essa propriedade é obrigatória para o funcionamento do formik
   */
  touched: _propTypes.default.object,

  /**
   * Texto que será exibido caso a opção buscada não seja encontrada.
   */
  noOptionString: _propTypes.default.string,

  /**
   * Define a altura máxima de expansão da caixa de opções do tag select.
   */
  maxHeight: _propTypes.default.string,

  /**
   * Caso o objeto options não siga o padrão [{key, label}], settar o optionsLabel irá substituir o valor de label<br>
   * <i>Ex: [{key: "true", nome: "Sim"}, {key: "false", nome: "Não"}] funciona se optionsLabel="nome"</i>
   */
  optionsLabel: _propTypes.default.string,

  /**
   * Caso o objeto options não siga o padrão [{key, label}], settar o optionsKey irá substituir o valor de key<br>
   * <i>Ex: [{valor: "true", label: "Sim"}, {valor: "false", label: "Não"}] funciona se optionsKey="valor"</i>
   */
  optionsKey: _propTypes.default.string,

  /**
   * Objeto com os IDs dos campos do formulário setados como error true, essa propriedade é obrigatória para o funcionamento do formik
   */
  errors: _propTypes.default.object,

  /**
   * Representa o atributo HTML "autocomplete" do input utilizado no tag select, recebe "on" ou "off"
   */
  autocomplete: _propTypes.default.string,

  /**
   * Prop própria do formik, deve ser true quando o formulário estiver validando
   */
  isValidating: _propTypes.default.bool,

  /**
   * Essa propriedade define se o tooltip que mostra erro deve ter um botão para fechá-lo
   */
  hasHideTooltip: _propTypes.default.bool
};
TagSelect.defaultProps = {
  className: null,
  value: [],
  placeholder: "",
  handleChange: function handleChange() {
    return [];
  },
  handleBlur: function handleBlur() {
    return [];
  },
  handleFocus: function handleFocus() {
    return [];
  },
  disabled: false,
  id: null,
  name: null,
  touched: null,
  noOptionString: "Nenhuma opção encontrada",
  maxHeight: null,
  optionsLabel: "label",
  optionsKey: "key",
  errors: {},
  autocomplete: "off"
};
var _default = TagSelect;
exports.default = _default;