"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = BlueHeaderTable;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var BlueHeaderTableStyled = _styledComponents.default.table(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  width: 100%;\n  border-spacing: 0px;\n  border-collapse: collapse;\n\n  & thead tr {\n    text-transform: uppercase;\n    background-color: #0C173B;\n    color: #fff;\n    font: Bold 12px/14px Arial;\n  }\n\n  & thead tr th {\n    text-align: center;\n    border: 0px;\n    padding: 5px;\n  }\n\n  & thead tr th:first-of-type {\n    border-top-left-radius: 20px;\n    border-bottom-left-radius: 20px;\n  }\n\n  & thead tr th:last-of-type {\n    border-top-right-radius: 20px;\n    border-bottom-right-radius: 20px;\n  }\n\n  & tbody:before {\n    height: 3px;\n    display: table-row;\n    content: '';\n  }\n\n  & tbody tr {\n    border-bottom: 2px solid #F1F1F1;\n  }\n\n  & tbody tr:last-of-type {\n    border-bottom: none;\n  }\n\n  & tbody tr:hover {\n    background-color: #F1F1F1;\n  }\n\n  & tbody tr td {\n    padding: 8px;\n    text-align: center;\n    vertical-align: center;\n    color: #444444;\n  }\n"])));

function BlueHeaderTable(props) {
  var className = props.className,
      children = props.children;
  return /*#__PURE__*/_react.default.createElement(BlueHeaderTableStyled, {
    className: className
  }, children);
}

BlueHeaderTable.propTypes = {
  className: _propTypes.default.string,
  children: _propTypes.default.oneOfType([_propTypes.default.arrayOf(_propTypes.default.node), _propTypes.default.node]).isRequired
};
BlueHeaderTable.defaultProps = {
  className: null
};