
# BlueHeaderTable
BlueHeaderTable é uma tag de table, customizada pra ter um header azul e uma linha cinza como separação entre as linhas.

>Note que BlueHeaderTable não vai construir a tabela por você, ela só substitui a tag `<table></table>` e adiciona o estilo, você ainda precisa adicionar o resto da estrutura como uma tabela normal (`<thead>`, `<tbody>`, `<tr>`, `<th>` e `<td>`).

## Props
| Prop | Tipo | Default | Descrição |
|---|---|---|---|
| className | string | `null` | Classes a serem adicionadas no componente, separadas por `' '` (espaço) |
| children | node | `null` | HTML que ficará dentro do componente. Utilizar como se fosse uma tag normal, no exemplo: `<BlueHeaderTable><thead><tr><th>Children</th></tr></thead></BlueHeaderTable>`, `<thead><tr><th>Children</th></tr></thead>` vai ser o valor dessa prop |
