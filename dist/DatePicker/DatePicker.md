
# DatePicker
DatePicker é um input tipo data que sempre abre um calendário para ajudar o usuário a escolher uma data.

## Props
| Prop | Tipo | Default | Descrição |
|---|---|---|---|
| dateInput | bool | `false` | Determina o tipo de input utilizado, se `true` ao invés de um `<input type="text>` ele irá utilizar `<input type="date">`. Input de data trás alguns problemas, por isso a escolha de toggle|
| value | string | `null` | Valor que será inicialmente mostrado no campo de input, cabe a você atualiza-lo utilizando `onChange`. Se for ter um value pré settado, utilizar o mesmo formato de `valueFormat` |
| onChange | function | `() => {}` | Função que é chama para cada mudança no campo, ela recebe um parâmetro: o novo valor mostrado no campo. Usar: `onChange = {(newValue) => {}}` |
| className | string | `null` | Classes a serem adicionadas no componente, separadas por `' '` (espaço) |
| weekdaysLabel | arrayOf(string) | `['D', 'S', 'T', 'Q', 'Q', 'S', 'S']` | Array de tamanho 7 com a label para aparecer no lugar dos dias da semana |
| weekdaysLabel | arrayOf(string) | `['Janeiro', 'Fevereiro', ..., 'Novembro', 'Dezembro']` | Array de tamanho 12 com a label para aparecer no nome dos meses |
| padZeros | bool | `true` | Se `true` os dias `1, 2, ..., 9` são mostrados como `01, 02, ..., 09` |
| placeholder | string | `'MM-DD-YYYY'` | Texto que será mostrado quando o campo estiver vazio. **Não funciona se inputDate for true**, nesse caso o template é escolhido com base no locale do usuário |
| displayFormat | string | `'MM-DD-YYYY'` | Formato que a data será mostrada e escrita no input, utilizamos [Moment.js]([https://momentjs.com/](https://momentjs.com/)) para fazer a formatação então basear-se nos formatos deles
| valueFormat | string | `'DD-MM-YYYY'` | Formato em que a data será transformada no `valor` e no parâmetro de `onChange`, utilizamos [Moment.js]([https://momentjs.com/](https://momentjs.com/)) para fazer a formatação então basear-se nos formatos deles
| setFieldError | function | `() => {}` | Função necessaria quando é preciso validação de data mínima e máxima via digitação no input, além disso é necessario utilizar o terceiro parâmetro `false` na função `setFieldTouched` chamada na `handleBlur`
| initYear | Number | `undefined` | Data utilizada para definir um limite máximo do ano que o usuário pode selecionar
| endYear | Number | `undefined` | Data utilizada para definir um limite mínimo do ano que o usuário pode selecionar