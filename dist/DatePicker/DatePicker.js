"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _moment = _interopRequireDefault(require("moment"));

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _InputWithButton = _interopRequireDefault(require("../InputWithButton/InputWithButton"));

var _ptBr = _interopRequireDefault(require("./Utils/pt-br"));

var _OutsideAlerter = _interopRequireDefault(require("../OutsideAlerter/OutsideAlerter"));

var _Select = _interopRequireDefault(require("../Select/Select"));

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

_moment.default.locale('pt-br', _ptBr.default);

var Overlay = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\ndisplay: ", ";\nposition: fixed;\ntop: 0;\nleft: 0;\nz-index: 2;\nopacity: 0.6;\nbackground-color: #707070;\nheight: 100%;\nwidth: 100%;\n\n@media (min-width: 768px) {\n  display: none;\n}\n"])), function (props) {
  return props.active ? 'block' : 'none';
});

var DatePickerStyled = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  position: relative;\n  margin: ", ";\n\n  & input[type=date]::-webkit-clear-button, /* blue cross */\n  & input[type=date]::-webkit-inner-spin-button, /* up */\n  & input[type=date]::-webkit-outer-spin-button, /* down */\n  & input[type=date]::-webkit-calendar-picker-indicator /* datepicker*/ {\n    display: none;\n  }\n\n  & input {\n    &:focus {\n      border: ", " solid\n        ", ";\n    }\n    transition: border 0.5s;\n  }\n\n  & .datetime-reset-button {\n    display: none;\n  }\n\n  & .calendar-div {\n    position: fixed;\n    left: 50%;\n    top: 50%;\n    transform: translate(-50%, -50%);\n    width: fit-content;\n    height: fit-content;\n    background-color: #FFFFFF;\n    box-shadow: ", ";\n    border-radius: 7px;\n    padding: ", ";\n    -webkit-user-select: none;\n    -moz-user-select: none;\n    -ms-user-select: none;\n    user-select: none;\n    display: none;\n    z-index: 3;\n  }\n\n  & .calendar-div.active {\n    display: block;\n  }\n\n  & .header {\n    width: 100%;\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n    justify-content: center;\n    margin-bottom: .5rem;\n\n    & .month-select {\n      width: 140px;\n      padding-right: 4px;\n    }\n\n    & .year-select {\n      width: 80px;\n    }\n\n    & input {\n      color: #0C173B;\n      font-weight: 500;\n      font-family: 'Roboto', sans-serif;\n      font-size: 14px;\n    }\n\n    & .chicon-arrow-bottom {\n      transform: none;\n      opacity: 0.25;\n      background: ", ";\n      border-radius: 10px;\n      color: white;\n      height: 18px;\n      width: 18px;\n    }\n\n    & button > i {\n      top: calc(50% - 0.55rem);\n      left: 3px;\n    }\n\n    & .chicon-arrow-bottom::before {\n      line-height: 1.55;\n      margin-left: 1.15px;\n    }\n\n    & .choice-div {\n      height: 200px;\n      margin: 4px 0 0 0;\n\n      & button {\n        color: #707070;\n      }\n    }\n\n    & ::-webkit-scrollbar {\n      width: 6px;\n    }\n\n    & ::-webkit-scrollbar-track {\n      box-shadow: inset 0 0 5px white;\n      border-radius: 10px;\n    }\n\n    & ::-webkit-scrollbar-thumb {\n      background: ", ";\n      border-radius: 10px;\n    }\n  }\n\n  & .icon {\n    cursor: pointer;\n    color: #707070;\n  }\n\n  & .table-days {\n    width: fit-content;\n  }\n\n  & .weekdays {\n    color: ", ";\n    font-weight: 700;\n    font-size: ", ";\n  }\n\n  & th {\n    height: ", ";\n    min-width: ", ";\n    padding: 0;\n    text-align: center;\n  }\n\n  & td {\n    cursor: pointer;\n    text-align: center;\n    vertical-align: middle;\n    color: #707070;\n    height: ", ";\n    min-width: ", ";\n    padding: 0;\n    font-size: ", ";\n    border-radius: 50%;\n  }\n\n  & td.month-day:hover {\n    background-color: #eee;\n  }\n\n  & td.month-day.active {\n    color: #FFF;\n    background-color: ", ";\n  }\n\n  & td.non-month-day {\n    cursor: default;\n    opacity: 0.3;\n  }\n\n  @media (min-width: 768px) {\n    & .calendar-div {\n     top: ", ";\n     right: 0;\n     transform: translate(0, 0);\n     bottom: ", ";\n     position: ", ";\n     left: ", ";\n    }\n  }\n"])), function (props) {
  var _props$theme, _props$theme$input;

  return ((_props$theme = props.theme) === null || _props$theme === void 0 ? void 0 : (_props$theme$input = _props$theme.input) === null || _props$theme$input === void 0 ? void 0 : _props$theme$input.margin) || '0';
}, function (props) {
  var _props$theme2, _props$theme2$input;

  return ((_props$theme2 = props.theme) === null || _props$theme2 === void 0 ? void 0 : (_props$theme2$input = _props$theme2.input) === null || _props$theme2$input === void 0 ? void 0 : _props$theme2$input.borderWeightFocus) || '1px';
}, function (props) {
  var _props$theme3, _props$theme3$input;

  return ((_props$theme3 = props.theme) === null || _props$theme3 === void 0 ? void 0 : (_props$theme3$input = _props$theme3.input) === null || _props$theme3$input === void 0 ? void 0 : _props$theme3$input.borderColorFocus) || 'red';
}, function (props) {
  return props.toDefineShadowPropertie ? ' 0px -3px 6px #00000029' : ' 0px 3px 6px #00000029';
}, function (props) {
  var _props$theme4, _props$theme4$datePic;

  return ((_props$theme4 = props.theme) === null || _props$theme4 === void 0 ? void 0 : (_props$theme4$datePic = _props$theme4.datePicker) === null || _props$theme4$datePic === void 0 ? void 0 : _props$theme4$datePic.calendarPadding) || '1rem';
}, function (props) {
  var _props$theme5, _props$theme5$colors;

  return ((_props$theme5 = props.theme) === null || _props$theme5 === void 0 ? void 0 : (_props$theme5$colors = _props$theme5.colors) === null || _props$theme5$colors === void 0 ? void 0 : _props$theme5$colors.primary) || '#E26226';
}, function (props) {
  var _props$theme6, _props$theme6$colors;

  return ((_props$theme6 = props.theme) === null || _props$theme6 === void 0 ? void 0 : (_props$theme6$colors = _props$theme6.colors) === null || _props$theme6$colors === void 0 ? void 0 : _props$theme6$colors.primary) || '#e26339';
}, function (props) {
  var _props$theme7, _props$theme7$colors;

  return ((_props$theme7 = props.theme) === null || _props$theme7 === void 0 ? void 0 : (_props$theme7$colors = _props$theme7.colors) === null || _props$theme7$colors === void 0 ? void 0 : _props$theme7$colors.primary) || '#E26226';
}, function (props) {
  var _props$theme8, _props$theme8$datePic;

  return ((_props$theme8 = props.theme) === null || _props$theme8 === void 0 ? void 0 : (_props$theme8$datePic = _props$theme8.datePicker) === null || _props$theme8$datePic === void 0 ? void 0 : _props$theme8$datePic.weekFontSize) || '1rem';
}, function (props) {
  var _props$theme9, _props$theme9$datePic;

  return ((_props$theme9 = props.theme) === null || _props$theme9 === void 0 ? void 0 : (_props$theme9$datePic = _props$theme9.datePicker) === null || _props$theme9$datePic === void 0 ? void 0 : _props$theme9$datePic.calendarSquare) || '32px';
}, function (props) {
  var _props$theme10, _props$theme10$datePi;

  return ((_props$theme10 = props.theme) === null || _props$theme10 === void 0 ? void 0 : (_props$theme10$datePi = _props$theme10.datePicker) === null || _props$theme10$datePi === void 0 ? void 0 : _props$theme10$datePi.calendarSquare) || '32px';
}, function (props) {
  var _props$theme11, _props$theme11$datePi;

  return ((_props$theme11 = props.theme) === null || _props$theme11 === void 0 ? void 0 : (_props$theme11$datePi = _props$theme11.datePicker) === null || _props$theme11$datePi === void 0 ? void 0 : _props$theme11$datePi.calendarSquare) || '32px';
}, function (props) {
  var _props$theme12, _props$theme12$datePi;

  return ((_props$theme12 = props.theme) === null || _props$theme12 === void 0 ? void 0 : (_props$theme12$datePi = _props$theme12.datePicker) === null || _props$theme12$datePi === void 0 ? void 0 : _props$theme12$datePi.calendarSquare) || '32px';
}, function (props) {
  var _props$theme13, _props$theme13$datePi;

  return ((_props$theme13 = props.theme) === null || _props$theme13 === void 0 ? void 0 : (_props$theme13$datePi = _props$theme13.datePicker) === null || _props$theme13$datePi === void 0 ? void 0 : _props$theme13$datePi.daysFontSize) || '0.9rem';
}, function (props) {
  var _props$theme14, _props$theme14$colors;

  return ((_props$theme14 = props.theme) === null || _props$theme14 === void 0 ? void 0 : (_props$theme14$colors = _props$theme14.colors) === null || _props$theme14$colors === void 0 ? void 0 : _props$theme14$colors.primary) || '#E26226';
}, function (props) {
  return props.dynamicAlign ? "".concat(props.toAlignBottomPropertie, "px") : 'auto';
}, function (props) {
  return props.toAlignTopPropertie;
}, function (props) {
  return props.dynamicAlign ? 'fixed' : 'absolute';
}, function (props) {
  return props.dynamicAlign ? "".concat(props.toAlignLeftPropertie, "px") : 'auto';
});
/**
  * Component: Date Picker
  * @component
  */


function DatePicker(props) {
  var _theme$colors;

  var value = props.value,
      handleChange = props.handleChange,
      handleBlur = props.handleBlur,
      className = props.className,
      weekdaysLabel = props.weekdaysLabel,
      monthsLabel = props.monthsLabel,
      padZeros = props.padZeros,
      placeholder = props.placeholder,
      displayFormat = props.displayFormat,
      valueFormat = props.valueFormat,
      dateInput = props.dateInput,
      disabled = props.disabled,
      id = props.id,
      name = props.name,
      touched = props.touched,
      errors = props.errors,
      theme = props.theme,
      invalidDateString = props.invalidDateString,
      mask = props.mask,
      autocomplete = props.autocomplete,
      dynamicAlign = props.dynamicAlign,
      initYear = props.initYear,
      endYear = props.endYear,
      setFieldError = props.setFieldError,
      isValidating = props.isValidating,
      hasHideTooltip = props.hasHideTooltip,
      getActualDate = props.getActualDate;

  function parseSelectedValue() {
    if (value && typeof value === 'string') {
      var parsedString = (0, _moment.default)(value, valueFormat);

      if (!parsedString.isValid()) {
        // eslint-disable-next-line no-console
        console.warn("DATE PICKER WARNING: Invalid Date. ".concat(value, " is not a valid date or is not in the specified format."));
        return {
          displayString: invalidDateString
        };
      }

      return {
        day: parseInt(parsedString.format('D'), 10),
        month: parseInt(parsedString.format('M'), 10) - 1,
        year: parseInt(parsedString.format('YYYY'), 10),
        displayString: parsedString.format(displayFormat)
      };
    }

    if (value && value instanceof Date) {
      return {
        day: value.getDate(),
        month: value.getMonth(),
        year: value.getFullYear(),
        displayString: (0, _moment.default)().year(value.getFullYear()).month(value.getMonth()).date(value.getDate()).format(displayFormat)
      };
    }

    return null;
  }

  var selected = parseSelectedValue();

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      calendarOpen = _useState2[0],
      setCalendarOpen = _useState2[1];

  var _useState3 = (0, _react.useState)(selected !== null && selected !== void 0 && selected.day ? selected.day : null),
      _useState4 = _slicedToArray(_useState3, 2),
      selectedDay = _useState4[0],
      setSelectedDay = _useState4[1];

  var _useState5 = (0, _react.useState)(selected !== null && selected !== void 0 && selected.month ? selected.month : null),
      _useState6 = _slicedToArray(_useState5, 2),
      selectedMonth = _useState6[0],
      setSelectedMonth = _useState6[1];

  var _useState7 = (0, _react.useState)(selected !== null && selected !== void 0 && selected.year ? selected.year : null),
      _useState8 = _slicedToArray(_useState7, 2),
      selectedYear = _useState8[0],
      setSelectedYear = _useState8[1];

  var _useState9 = (0, _react.useState)(selected !== null && selected !== void 0 && selected.month ? selected.month : new Date().getMonth()),
      _useState10 = _slicedToArray(_useState9, 2),
      displayedMonth = _useState10[0],
      setDisplayedMonth = _useState10[1];

  var _useState11 = (0, _react.useState)(selected !== null && selected !== void 0 && selected.year ? selected.year : new Date().getFullYear()),
      _useState12 = _slicedToArray(_useState11, 2),
      displayedYear = _useState12[0],
      setDisplayedYear = _useState12[1];

  var _useState13 = (0, _react.useState)([]),
      _useState14 = _slicedToArray(_useState13, 2),
      calendar = _useState14[0],
      setCalendar = _useState14[1];

  var _useState15 = (0, _react.useState)(!dateInput && (selected && selected.displayString || '')),
      _useState16 = _slicedToArray(_useState15, 2),
      textDisplay = _useState16[0],
      setTextDisplay = _useState16[1];

  var _useState17 = (0, _react.useState)(false),
      _useState18 = _slicedToArray(_useState17, 2),
      lastWritten = _useState18[0],
      setLastWritten = _useState18[1];

  var _useState19 = (0, _react.useState)(0),
      _useState20 = _slicedToArray(_useState19, 2),
      heightOfCalendar = _useState20[0],
      setHeightOfCalendar = _useState20[1];

  var _useState21 = (0, _react.useState)(0),
      _useState22 = _slicedToArray(_useState21, 2),
      toAlignBottom = _useState22[0],
      setToAlignBottom = _useState22[1];

  var _useState23 = (0, _react.useState)(false),
      _useState24 = _slicedToArray(_useState23, 2),
      toAlignTop = _useState24[0],
      setToAlignTop = _useState24[1];

  var _useState25 = (0, _react.useState)(0),
      _useState26 = _slicedToArray(_useState25, 2),
      toAlignLeft = _useState26[0],
      setToAlignLeft = _useState26[1];

  var _useState27 = (0, _react.useState)(false),
      _useState28 = _slicedToArray(_useState27, 2),
      alignShadowTop = _useState28[0],
      setAlignShadowTop = _useState28[1];

  var outsideRef = (0, _react.useRef)(null);
  var didMountRef = (0, _react.useRef)(false);
  var calendarRef = (0, _react.useRef)(null);
  (0, _OutsideAlerter.default)(outsideRef, function () {
    return setCalendarOpen(false);
  });

  var _useState29 = (0, _react.useState)([]),
      _useState30 = _slicedToArray(_useState29, 2),
      yearArray = _useState30[0],
      renderYearArray = _useState30[1];

  (0, _react.useEffect)(function () {
    if (didMountRef.current && !lastWritten) {
      var newSelected = parseSelectedValue();
      setSelectedDay(newSelected === null || newSelected === void 0 ? void 0 : newSelected.day);
      setSelectedMonth(newSelected === null || newSelected === void 0 ? void 0 : newSelected.month);
      setSelectedYear(newSelected === null || newSelected === void 0 ? void 0 : newSelected.year);
      setDisplayedMonth((newSelected === null || newSelected === void 0 ? void 0 : newSelected.month) >= 0 ? newSelected === null || newSelected === void 0 ? void 0 : newSelected.month : new Date().getMonth());
      setDisplayedYear((newSelected === null || newSelected === void 0 ? void 0 : newSelected.year) || new Date().getFullYear());
      setTextDisplay(newSelected === null || newSelected === void 0 ? void 0 : newSelected.displayString);
    }

    setLastWritten(false);
  }, [value]);
  (0, _react.useEffect)(function () {
    if (!disabled && didMountRef.current && selectedDay && (selectedMonth || selectedMonth === 0) && selectedYear) {
      handleChange((0, _moment.default)().year(selectedYear).month(selectedMonth).date(selectedDay).format(valueFormat));
    } else if (!didMountRef.current) {
      didMountRef.current = true;
    } else if (!selectedDay || !selectedMonth && selectedMonth !== 0 || !selectedYear) {
      handleChange(undefined);
    }
  }, [selectedDay, selectedMonth, selectedYear]);

  function monthData(month, year) {
    var firstDay = new Date(year, month, 1).getDay();
    var daysInMonth = new Date(year, month + 1, 0).getDate();
    var daysInPrevMonth = new Date(year, month, 0).getDate();
    return {
      firstDay: firstDay,
      daysInMonth: daysInMonth,
      daysInPrevMonth: daysInPrevMonth,
      hasSixWeeks: firstDay + daysInMonth > 35
    };
  }

  function calculateDays(month, year) {
    var days = [];
    var data = monthData(month, year);
    var count = 0;

    for (var _i2 = data.firstDay - 1; _i2 >= 0; _i2 -= 1) {
      days[count] = {
        day: data.daysInPrevMonth - _i2,
        monthDay: false
      };
      count += 1;
    }

    for (var _i3 = 0; _i3 < data.daysInMonth; _i3 += 1) {
      days[count] = {
        day: _i3 + 1,
        monthDay: true
      };
      count += 1;
    }

    var i = 1;

    for (count; count < (data.hasSixWeeks ? 42 : 35); count += 1) {
      days[count] = {
        day: i,
        monthDay: false
      };
      i += 1;
    }

    var calendarDays = [];

    for (i = 0; i < (data.hasSixWeeks ? 6 : 5); i += 1) {
      calendarDays[i] = days.slice(i * 7, (i + 1) * 7);
    }

    setCalendar(calendarDays);
  }

  (0, _react.useEffect)(function () {
    calculateDays(displayedMonth, displayedYear);
  }, [displayedMonth, displayedYear]);

  function renderSelectOptions(inityear, endyear) {
    var options = []; // eslint-disable-next-line no-plusplus

    for (var i = inityear; i >= endyear; i--) {
      options.push({
        label: "".concat(i),
        key: i
      });
    }

    renderYearArray(options);
  }

  (0, _react.useEffect)(function () {
    renderSelectOptions(initYear, endYear);
  }, [initYear, endYear]); // Used when onBlur text input and after selected

  function textFormatter(newDay) {
    if (displayedYear && (displayedMonth || displayedMonth === 0) && (newDay || selectedDay)) {
      setTextDisplay((0, _moment.default)().year(displayedYear).month(displayedMonth).date(newDay || selectedDay).format(displayFormat));
    } else {
      setTextDisplay('');
    }
  }

  function changeSelected(newDay) {
    setSelectedDay(newDay);
    setSelectedMonth(displayedMonth);
    setSelectedYear(displayedYear);

    if (!dateInput) {
      textFormatter(newDay);
    }
  }

  function changeSelectedMonth(newMonth) {
    setSelectedMonth(newMonth);
    setSelectedYear(displayedYear);
  }

  function changeSelectedYear(newYear) {
    if (newYear >= endYear) {
      setDisplayedYear(newYear);
      setSelectedYear(newYear);
      setSelectedMonth(displayedMonth);
    }
  }

  function dateWritten(e) {
    if (e.target.valueAsDate) {
      setSelectedDay(e.target.valueAsDate.getUTCDate());
      setSelectedMonth(e.target.valueAsDate.getUTCMonth());
      setSelectedYear(e.target.valueAsDate.getUTCFullYear());
      setDisplayedMonth(e.target.valueAsDate.getUTCMonth());
      setDisplayedYear(e.target.valueAsDate.getUTCFullYear());
    }
  }

  function dateFormatter() {
    if (selectedDay && (selectedMonth || selectedMonth === 0) && selectedYear) {
      return "".concat(selectedYear.toString().padStart(4, '0'), "-").concat((selectedMonth + 1).toString().padStart(2, '0'), "-").concat(selectedDay.toString().padStart(2, '0'));
    }

    return undefined;
  }

  function textWritten(e) {
    var date = (0, _moment.default)(e.target.value, displayFormat);
    setLastWritten(true);

    if (date.isValid()) {
      setSelectedDay(parseInt(date.format('D'), 10));
      setSelectedMonth(parseInt(date.format('M'), 10) - 1);
      setSelectedYear(parseInt(date.format('YYYY'), 10));
      setDisplayedMonth(parseInt(date.format('M'), 10) - 1);
      setDisplayedYear(parseInt(date.format('YYYY'), 10));
      setTextDisplay(e.target.value);
    } else {
      setSelectedDay(null);
      setSelectedMonth(null);
      setSelectedYear(null);
      setTextDisplay(e.target.value);
    }
  }

  function renderDay(day, index) {
    var display = padZeros ? day.day.toString().padStart(2, '0') : day.day;

    if (day.monthDay) {
      if (day.day === selectedDay && selectedMonth === displayedMonth && selectedYear === displayedYear) {
        return (
          /*#__PURE__*/
          // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
          _react.default.createElement("td", {
            key: "day-".concat(index),
            className: "month-day active",
            onClick: function onClick() {
              return changeSelected(day.day);
            }
          }, display)
        );
      }

      return (
        /*#__PURE__*/
        // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
        _react.default.createElement("td", {
          key: "day-".concat(index),
          className: "month-day",
          onClick: function onClick() {
            return changeSelected(day.day);
          }
        }, display)
      );
    }

    return /*#__PURE__*/_react.default.createElement("td", {
      key: "non-day-".concat(index),
      className: "non-month-day"
    }, display);
  }

  var toDynamicallyAlign = function toDynamicallyAlign() {
    var detailsParentElement = outsideRef.current.getBoundingClientRect();
    var calendarProperties = calendarRef.current.getBoundingClientRect();

    if (window.screen.availHeight - detailsParentElement.bottom - detailsParentElement.height > heightOfCalendar) {
      setAlignShadowTop(false);
      setToAlignBottom(detailsParentElement.bottom);
      setToAlignLeft(detailsParentElement.right - calendarProperties.width);
    } else {
      setAlignShadowTop(true);
      setToAlignBottom(detailsParentElement.bottom - heightOfCalendar - detailsParentElement.height);
    }
  };

  var toDefineDropAlign = function toDefineDropAlign() {
    var detailsParentElement = outsideRef.current.getBoundingClientRect();

    if (window.screen.availHeight - detailsParentElement.bottom - detailsParentElement.height > heightOfCalendar) {
      setToAlignTop(false);
      setAlignShadowTop(false);
    } else {
      setToAlignTop(true);
      setAlignShadowTop(true);
    }
  }; // eslint-disable-next-line consistent-return


  function defineAlignTop() {
    if (dynamicAlign) {
      if (toAlignBottom > 0) {
        return "".concat(toAlignBottom, "px");
      }

      return 'initial';
    }

    if (!dynamicAlign) {
      if (toAlignTop) {
        return '25px';
      }

      return 'initial';
    }
  }

  function validateYear() {
    if (setFieldError) {
      var year = selectedYear;

      if (year && year < endYear) {
        setFieldError(id, "Ano n\xE3o pode ser inferior a ".concat(endYear));
      }

      if (year && year > initYear) {
        setFieldError(id, "Ano n\xE3o pode ser superior a ".concat(initYear));
      }
    }
  }

  (0, _react.useEffect)(function () {
    setHeightOfCalendar(calendarRef.current.clientHeight);

    if (calendarOpen === true && !dynamicAlign) {
      toDefineDropAlign();
    }

    if (calendarOpen === true && dynamicAlign) {
      toDynamicallyAlign();
    }
  }, [calendarOpen, heightOfCalendar]);

  function handleClickCalendar() {
    setCalendarOpen(!calendarOpen);

    if (!selectedDay && getActualDate) {
      setSelectedDay(new Date().getDate());
      setSelectedMonth(new Date().getMonth());
      setSelectedYear(new Date().getFullYear());
    }
  }

  return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(Overlay, {
    active: calendarOpen && !disabled
  }), /*#__PURE__*/_react.default.createElement(DatePickerStyled, {
    className: className,
    ref: outsideRef,
    alignBottom: toAlignTop,
    dynamicAlign: dynamicAlign,
    toAlignBottomPropertie: toAlignBottom,
    toAlignLeftPropertie: toAlignLeft,
    toAlignTopPropertie: defineAlignTop(),
    toDefineShadowPropertie: alignShadowTop
  }, /*#__PURE__*/_react.default.createElement(_InputWithButton.default, {
    name: name,
    buttonIcon: "chicon-calendar",
    iconColour: (theme === null || theme === void 0 ? void 0 : (_theme$colors = theme.colors) === null || _theme$colors === void 0 ? void 0 : _theme$colors.primary) || '#E26226',
    alt: "Open calendar",
    onButtonClick: handleClickCalendar,
    type: dateInput ? 'date' : 'text',
    id: id,
    value: dateInput ? dateFormatter() : textDisplay,
    handleChange: dateInput ? function (e) {
      return dateWritten(e);
    } : function (e) {
      return textWritten(e);
    },
    handleBlur: dateInput ? handleBlur : function () {
      textFormatter();
      handleBlur();
      validateYear();
    },
    placeholder: placeholder,
    disabled: disabled,
    touched: touched,
    extraAttributes: {
      name: name
    },
    errors: errors,
    error: touched && errors && touched[id] && errors[id],
    mask: dateInput ? null : mask,
    autocomplete: autocomplete,
    isValidating: isValidating,
    hasHideTooltip: hasHideTooltip
  }), /*#__PURE__*/_react.default.createElement("div", {
    ref: calendarRef,
    className: "calendar-div ".concat(calendarOpen && !disabled ? 'active' : '')
  }, calendarOpen && !disabled && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("div", {
    className: "header"
  }, /*#__PURE__*/_react.default.createElement(_Select.default, {
    buttonIcon: "chicon-arrow-bottom",
    className: "month-select",
    handleChange: function handleChange(newMonth) {
      return changeSelectedMonth(newMonth);
    },
    value: selectedMonth,
    options: monthsLabel,
    readOnly: true
  }), /*#__PURE__*/_react.default.createElement(_Select.default, {
    buttonIcon: "chicon-arrow-bottom",
    className: "year-select",
    handleChange: function handleChange(newYear) {
      return changeSelectedYear(newYear);
    },
    value: selectedYear,
    options: yearArray,
    readOnly: true
  })), /*#__PURE__*/_react.default.createElement("table", {
    className: "table-days"
  }, /*#__PURE__*/_react.default.createElement("thead", {
    className: "weekdays"
  }, /*#__PURE__*/_react.default.createElement("tr", null, /*#__PURE__*/_react.default.createElement("th", null, weekdaysLabel[0]), /*#__PURE__*/_react.default.createElement("th", null, weekdaysLabel[1]), /*#__PURE__*/_react.default.createElement("th", null, weekdaysLabel[2]), /*#__PURE__*/_react.default.createElement("th", null, weekdaysLabel[3]), /*#__PURE__*/_react.default.createElement("th", null, weekdaysLabel[4]), /*#__PURE__*/_react.default.createElement("th", null, weekdaysLabel[5]), /*#__PURE__*/_react.default.createElement("th", null, weekdaysLabel[6]))), /*#__PURE__*/_react.default.createElement("tbody", {
    className: "days"
  }, calendar.map(function (week, i) {
    return /*#__PURE__*/_react.default.createElement("tr", {
      key: "week-".concat(i)
    }, week.map(function (day, index) {
      return renderDay(day, index);
    }));
  })))))));
}

DatePicker.propTypes = {
  /**
    * Deve receber o estado/variável onde o valor setado pelo componente é guardado.
    */
  value: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.instanceOf(Date)]),

  /**
   * Deve receber uma função que pega o valor que o usuário escolheu e que guardará o valor dentro da propriedade "value".
   */
  handleChange: _propTypes.default.func.isRequired,

  /**
   * Essa propriedade é executada quando o usuário clica fora do input.
   */
  handleBlur: _propTypes.default.func,

  /**
   * Define as classes CSS do DatePicker.
   */
  className: _propTypes.default.string,

  /**
   * Essa propriedade deve receber um array onde cada item é o nome do dia da semana que será exibido.
   */
  weekdaysLabel: _propTypes.default.arrayOf(_propTypes.default.string),

  /**
   * Essa propriedade deve receber um array onde cada item é o nome de cada mês que será exibido.
   */
  monthsLabel: _propTypes.default.arrayOf(_propTypes.default.string),

  /**
   * Essa propriedade remove o número 0 quando setado como false, da frente dos números de 1-9, dentro do seletor de datas do calendário.
   */
  padZeros: _propTypes.default.bool,

  /**
   * Essa propriedade deve receber uma string que será apresentado no input.
   */
  placeholder: _propTypes.default.string,

  /**
   * Deve receber uma string que informa como será o formato do valor exibido ao usuário, exemplo: <b>'YYYY-MM-DD'</b>.
   */
  displayFormat: _propTypes.default.string,

  /**
   * Deve receber uma string que informa como será o formato do valor que será guardado, exemplo: <b>'DD-MM-YYYY'</b>.
   */
  valueFormat: _propTypes.default.string,

  /**
   * Essa propriedade permite que o usuário modifique a data sem que haja uma quebra de layout.
   * E também simula alguns tipos de seletores de data que estão dentro de alguns sistemas operacionais.
   */
  dateInput: _propTypes.default.bool,

  /**
   * A propriedade deve receber um objeto onde contém os erros relacionados ao seu componente.
   */
  errors: _propTypes.default.object,

  /**
   * Propriedade que define se o DatePicker está desabilitado ou não.
   */
  disabled: _propTypes.default.bool,

  /**
   * Identificador do select.
   */
  id: _propTypes.default.string,

  /**
   * Identificador/nome do select.
   */
  name: _propTypes.default.string,

  /**
   * Objeto com os IDs dos campos do formulário que já foram clicados pelo usuário, essa propriedade é obrigatória para o funcionamento do formik.
   */
  touched: _propTypes.default.object,

  /**
   * Recebe um objeto com propriedades que se referem a algum elemento CSS, o conteúdo final da propriedade deve ser CSS.
   */
  theme: _propTypes.default.object.isRequired,

  /**
   * Quando uma data for inválida, o texto contido nessa propriedade será exibido.
   */
  invalidDateString: _propTypes.default.string,

  /**
   * Propriedade para exibir a máscara de data no padrão regex.
   */
  mask: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.array]),

  /**
   * Representa o atributo HTML "autocomplete" do input utilizado no DatePicker, recebe "on" ou "off".
   */
  autocomplete: _propTypes.default.string,

  /**
   * Propriedade para definir o tipo de posicionamento da caixa de opções, sendo as variações internas: 'fixed' ou 'absolute'
   * que foram implementadas para situações de uso em modais, por exemplo pois por default quando a caixa de opções abre pode gerar scroll.
   */
  dynamicAlign: _propTypes.default.bool,

  /**
   * Essa propriedade determina à partir de que ano o calendário se inicia.
   */
  initYear: _propTypes.default.number,

  /**
   * Essa propriedade determina até que ano o calendário será vigente.
   */
  endYear: _propTypes.default.number,

  /**
   * Prop própria do formik, deve ser true quando o formulário estiver validando
   */
  isValidating: _propTypes.default.bool,

  /**
   * Essa propriedade define se o tooltip que mostra erro deve ter um botão para fechá-lo
   */
  hasHideTooltip: _propTypes.default.bool,

  /**
   * Define se o datePicker deve abrir o calendário com o dia atual já selecionado, caso não haja valor nenhum selecionado.
   */
  getActualDate: _propTypes.default.bool
};
DatePicker.defaultProps = {
  value: null,
  handleBlur: function handleBlur() {},
  className: null,
  weekdaysLabel: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
  monthsLabel: [{
    label: 'Janeiro',
    key: 0
  }, {
    label: 'Fevereiro',
    key: 1
  }, {
    label: 'Março',
    key: 2
  }, {
    label: 'Abril',
    key: 3
  }, {
    label: 'Maio',
    key: 4
  }, {
    label: 'Junho',
    key: 5
  }, {
    label: 'Julho',
    key: 6
  }, {
    label: 'Agosto',
    key: 7
  }, {
    label: 'Setembro',
    key: 8
  }, {
    label: 'Outubro',
    key: 9
  }, {
    label: 'Novembro',
    key: 10
  }, {
    label: 'Dezembro',
    key: 11
  }],
  padZeros: true,
  placeholder: 'MM-DD-YYYY',
  displayFormat: 'MM-DD-YYYY',
  valueFormat: 'DD-MM-YYYY',
  dateInput: false,
  errors: {},
  disabled: false,
  id: null,
  name: null,
  touched: null,
  invalidDateString: 'Data inválida',
  mask: null,
  autocomplete: 'on',
  dynamicAlign: false,
  initYear: new Date().getFullYear(),
  endYear: new Date().getFullYear() - 80,
  getActualDate: false
};

var _default = (0, _styledComponents.withTheme)(DatePicker);

exports.default = _default;