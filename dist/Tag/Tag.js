"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var TagStyled = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  background-color: #ffe0d2;\n  border-radius: 30px;\n  width: fit-content;\n  padding: 8px 12px;\n  position: relative;\n  &.disabled {\n    background-color: #dddddd !important;\n  }\n  & p {\n    display: inline-block;\n    margin: 0;\n    color: #444444;\n  }\n\n  & i {\n    margin-left: 8px;\n    cursor: pointer;\n    width: 1rem;\n    height: 1rem;\n    vertical-align: middle;\n  }\n"])));
/**
 * Componente: Tag <br>
 * Tag que possui texto e ícone. O ícone possui as propriedades de um botão.<br>
 * <i>Utilizado no TagSelect.</i>
 * @component
 */


function Tag(props) {
  var text = props.text,
      className = props.className,
      _onClick = props.onClick,
      disabled = props.disabled,
      tagIcon = props.tagIcon;
  return /*#__PURE__*/_react.default.createElement(TagStyled, {
    className: disabled ? "".concat(className, " disabled") : className
  }, /*#__PURE__*/_react.default.createElement("p", null, text), !disabled && /*#__PURE__*/_react.default.createElement("i", {
    className: tagIcon || "chicon-close",
    onClick: function onClick(e) {
      _onClick(e);
    }
  }));
}

Tag.propTypes = {
  /**
   * Define as classes CSS da tag
   */
  className: _propTypes.default.string,

  /**
   * Define o texto que será mostrado dentro da tag
   */
  text: _propTypes.default.string.isRequired,

  /**
   * Função que será executada ao clicar no icone
   */
  onClick: _propTypes.default.func,

  /**
   * Propriedade que define se a tag está desabilitado ou não
   */
  disabled: _propTypes.default.bool,

  /**
   * Propriedade para indicar qual ícone deverá ser utilizado
   */
  tagIcon: _propTypes.default.string
};
Tag.defaultProps = {
  className: null,
  onClick: function onClick() {},
  disabled: false,
  tagIcon: "chicon-close"
};
var _default = Tag;
exports.default = _default;