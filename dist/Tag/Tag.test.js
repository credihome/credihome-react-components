"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _Tag = _interopRequireDefault(require("./Tag"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_enzyme.default.configure({
  adapter: new _enzymeAdapterReact.default()
});
/*
  No primeiro atributo fica o nome da "esteira" de testes
  Já no segundo é a função que será executada para realizar o teste
*/


describe('Tag Testes - funcionalidades básicas', function () {
  /*
    No primeiro atributo fica o nome do teste a ser realizado
    Já no segundo é a função que será executada para realizar o teste
  */
  it('Verifica se o texto enviado é o mesmo que o exibido', function () {
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react.default.createElement(_Tag.default, {
      text: "Bot\xE3o 1"
    })); // wrapper === <div><p>Botão 1</p> <i class="chicon-close" onClick="algumaFunc" /></div>

    expect(wrapper.find("p").text()).toBe("Botão 1"); // find pode procurar por seletores CSS, ex: tags, class, ids, etc.
    // text busca pelo conteúdo que consta dentro da tag
    // toBe é o que você deseja que o valor final do expect tenha igualdade
  });
  it('O texto enviado não deve ser o mesmo do exibido', function () {
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react.default.createElement(_Tag.default, {
      text: "Bot\xE3o 2"
    }));
    expect(wrapper.find('p').text()).not.toBe("Botão 1");
  });
  /*
    fn -> método do jest para fazer um mock de uma função;
    após fazer o mock, a constante "actionButtonClick" é uma função "vazia"
    e informações sobre ela estará dentro da const tb
  */

  it('Testa se ao clicar executa alguma ação', function () {
    var actionButtonClick = jest.fn();
    var button = (0, _enzyme.shallow)( /*#__PURE__*/_react.default.createElement(_Tag.default, {
      onClick: actionButtonClick,
      text: "Bot\xE3o 3"
    }));
    button.find('i').simulate('click'); // expect(actionButtonlick.mock.calls.length).toEqual(1);

    expect(actionButtonClick).toHaveBeenCalledTimes(1);
  });
  /*
    props -> verificando propriedade disable
  */

  it('Testando se recebe a classe que desabilita um item', function () {
    var disabled = (0, _enzyme.shallow)( /*#__PURE__*/_react.default.createElement(_Tag.default, {
      text: "teste",
      disabled: true
    }));
    var findDisabled = disabled.find('i').getElements();
    expect(findDisabled).toHaveLength(0);
  });
});