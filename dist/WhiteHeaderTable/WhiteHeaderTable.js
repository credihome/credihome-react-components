"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = WhiteHeaderTable;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var WhiteHeaderTableStyled = _styledComponents.default.table(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  border-spacing: 0;\n  width: 100%;\n  color: #444444;\n  background-color: #fff;\n\n  & > thead {\n    font-size: 15px;\n  }\n\n  & > thead > tr > th {\n    border-bottom: 1px solid #F3F3F3;\n    padding: 12px;\n    vertical-align: center;\n  }\n\n  & > thead > tr > th i {\n    color: #DDDDDD;\n  }\n\n  & > thead > tr > th i:hover {\n    color: #BBBBBB;\n  }\n\n  & > tbody {\n    font-size: 14px;\n  }\n\n  & tr,\n  & td {\n    padding: 12px;\n    text-align: left;\n    border-bottom: 1px solid #F3F3F3;\n  }\n"])));
/**
 * Componente: WhiteHeaderTable
 * @component WhiteHeaderTable 
 */


function WhiteHeaderTable(props) {
  var className = props.className,
      children = props.children;
  return /*#__PURE__*/_react.default.createElement(WhiteHeaderTableStyled, {
    className: className
  }, children);
}

WhiteHeaderTable.propTypes = {
  /**
   * Recebe as classes de estilo que serão aplicadas dentro do elemento DOM.
   */
  className: _propTypes.default.string,

  /**
   * Deve receber um elemento React ou função (que devolva um DOM/React), o que ser enviado aqui, será renderizado dentro deste componente.
   */
  children: _propTypes.default.oneOfType([_propTypes.default.arrayOf(_propTypes.default.node), _propTypes.default.node]).isRequired
};
WhiteHeaderTable.defaultProps = {
  className: null
};