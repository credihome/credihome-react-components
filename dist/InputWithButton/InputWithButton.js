"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = InputWithButtonComponent;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _reactTextMask = _interopRequireDefault(require("react-text-mask"));

var _Tooltip = _interopRequireDefault(require("../Tooltip/Tooltip"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

/**
 * Component InputWithButton
 * @component
 */
var InputWithButton = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: relative;\n  min-width: fit-content;\n  z-index: 1;\n\n  & > button > i {\n    width: 1rem;\n    height: 1rem;\n    position: absolute;\n    top: calc(50% - 0.4rem);\n    left: 0;\n    color: ", ";\n    transform: ", ";\n    font-size: 0.8rem;\n  }\n\n  & button {\n    width: 1rem;\n    height: 1rem;\n    position: absolute;\n    right: 10px;\n    top: calc(50% - 0.5rem);\n    cursor: ", ";\n    background-color: transparent;\n    border: none;\n    display: ", ";\n  }\n\n  & button:focus {\n    outline: none;\n    box-shadow: none;\n  }\n\n  & input {\n    padding: ", ";\n    padding-right: calc(1rem + 10px);\n    border: 1px solid;\n    border-color: ", ";\n    border-radius: ", ";\n    margin: 0;\n    width: 100%;\n    height: ", ";\n    color: ", ";\n    box-sizing: border-box;\n    cursor: ", ";\n    transition: ", ";\n    font: ", ";\n  }\n\n  & input::placeholder {\n    color: ", ";\n  }\n\n  & input:disabled {\n    background-color: ", ";\n    color: ", ";\n    cursor: default;\n    & ~ i {\n      display: none;\n    }\n  }\n\n  & input:focus {\n    outline: none;\n    border: ", " solid\n      ", ";\n  }\n"])), function (props) {
  return props.iconColour;
}, function (props) {
  return props.iconRotate ? "rotate(".concat(props.iconRotate, ")") : "";
}, function (props) {
  return props.disabled ? "default" : "pointer";
}, function (props) {
  return props.disabled ? "none" : "default";
}, function (props) {
  var _props$theme, _props$theme$input;

  return ((_props$theme = props.theme) === null || _props$theme === void 0 ? void 0 : (_props$theme$input = _props$theme.input) === null || _props$theme$input === void 0 ? void 0 : _props$theme$input.padding) || "4px 10px";
}, function (props) {
  var _props$theme2, _props$theme2$colors, _props$theme2$colors$, _props$theme3, _props$theme3$input;

  return props.error ? ((_props$theme2 = props.theme) === null || _props$theme2 === void 0 ? void 0 : (_props$theme2$colors = _props$theme2.colors) === null || _props$theme2$colors === void 0 ? void 0 : (_props$theme2$colors$ = _props$theme2$colors.error) === null || _props$theme2$colors$ === void 0 ? void 0 : _props$theme2$colors$.backgroundColor) || "#ea2d2d" : ((_props$theme3 = props.theme) === null || _props$theme3 === void 0 ? void 0 : (_props$theme3$input = _props$theme3.input) === null || _props$theme3$input === void 0 ? void 0 : _props$theme3$input.borderColor) || "#DDDDDD";
}, function (props) {
  var _props$theme4, _props$theme4$input;

  return ((_props$theme4 = props.theme) === null || _props$theme4 === void 0 ? void 0 : (_props$theme4$input = _props$theme4.input) === null || _props$theme4$input === void 0 ? void 0 : _props$theme4$input.borderRadius) || "15px";
}, function (props) {
  var _props$theme5, _props$theme5$input;

  return ((_props$theme5 = props.theme) === null || _props$theme5 === void 0 ? void 0 : (_props$theme5$input = _props$theme5.input) === null || _props$theme5$input === void 0 ? void 0 : _props$theme5$input.height) || "100%";
}, function (props) {
  var _props$theme6, _props$theme6$input;

  return ((_props$theme6 = props.theme) === null || _props$theme6 === void 0 ? void 0 : (_props$theme6$input = _props$theme6.input) === null || _props$theme6$input === void 0 ? void 0 : _props$theme6$input.color) || "#777";
}, function (props) {
  return props.readOnly ? "pointer" : "auto";
}, function (props) {
  var _props$theme7, _props$theme7$input;

  return (_props$theme7 = props.theme) !== null && _props$theme7 !== void 0 && (_props$theme7$input = _props$theme7.input) !== null && _props$theme7$input !== void 0 && _props$theme7$input.focusBorderBottom ? "0.3s border" : "none";
}, function (props) {
  var _props$theme8, _props$theme8$input, _props$theme9, _props$theme9$input, _props$theme10, _props$theme10$input;

  return "".concat(((_props$theme8 = props.theme) === null || _props$theme8 === void 0 ? void 0 : (_props$theme8$input = _props$theme8.input) === null || _props$theme8$input === void 0 ? void 0 : _props$theme8$input.fontWeight) || "normal", " ").concat(((_props$theme9 = props.theme) === null || _props$theme9 === void 0 ? void 0 : (_props$theme9$input = _props$theme9.input) === null || _props$theme9$input === void 0 ? void 0 : _props$theme9$input.fontSize) || "inherit", " ").concat(((_props$theme10 = props.theme) === null || _props$theme10 === void 0 ? void 0 : (_props$theme10$input = _props$theme10.input) === null || _props$theme10$input === void 0 ? void 0 : _props$theme10$input.fontFamily) || "inherit");
}, function (props) {
  var _props$theme11, _props$theme11$input;

  return ((_props$theme11 = props.theme) === null || _props$theme11 === void 0 ? void 0 : (_props$theme11$input = _props$theme11.input) === null || _props$theme11$input === void 0 ? void 0 : _props$theme11$input.placeholder) || "#777";
}, function (props) {
  var _props$theme12, _props$theme12$input;

  return ((_props$theme12 = props.theme) === null || _props$theme12 === void 0 ? void 0 : (_props$theme12$input = _props$theme12.input) === null || _props$theme12$input === void 0 ? void 0 : _props$theme12$input.backgroundColorDisabled) || "rgba(255, 255, 255, 0.5)";
}, function (props) {
  var _props$theme13, _props$theme13$input;

  return ((_props$theme13 = props.theme) === null || _props$theme13 === void 0 ? void 0 : (_props$theme13$input = _props$theme13.input) === null || _props$theme13$input === void 0 ? void 0 : _props$theme13$input.fontColorDisabled) || "#545454";
}, function (props) {
  var _props$theme14, _props$theme14$input;

  return ((_props$theme14 = props.theme) === null || _props$theme14 === void 0 ? void 0 : (_props$theme14$input = _props$theme14.input) === null || _props$theme14$input === void 0 ? void 0 : _props$theme14$input.borderWeightFocus) || "1px";
}, function (props) {
  var _props$theme15, _props$theme15$input;

  return ((_props$theme15 = props.theme) === null || _props$theme15 === void 0 ? void 0 : (_props$theme15$input = _props$theme15.input) === null || _props$theme15$input === void 0 ? void 0 : _props$theme15$input.borderColorFocus) || "red";
});

function InputWithButtonComponent(props) {
  // TODO exibição de errors -> touched
  var type = props.type,
      disabled = props.disabled,
      placeholder = props.placeholder,
      handleChange = props.handleChange,
      handleBlur = props.handleBlur,
      handleFocus = props.handleFocus,
      value = props.value,
      id = props.id,
      touched = props.touched,
      errors = props.errors,
      onButtonClick = props.onButtonClick,
      buttonIcon = props.buttonIcon,
      className = props.className,
      extraAttributes = props.extraAttributes,
      iconColour = props.iconColour,
      readOnly = props.readOnly,
      iconRotate = props.iconRotate,
      onInputClick = props.onInputClick,
      mask = props.mask,
      autocomplete = props.autocomplete,
      isValidating = props.isValidating,
      hasHideTooltip = props.hasHideTooltip;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      hideTooltip = _useState2[0],
      setHideTooltip = _useState2[1];

  (0, _react.useEffect)(function () {
    if (hasHideTooltip) setHideTooltip(false);
  }, [isValidating, errors[id]]);
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, touched && errors && touched[id] && errors[id] && /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_Tooltip.default, {
    message: errors[id],
    hideTooltip: hideTooltip,
    setHideTooltip: setHideTooltip,
    hasHideTooltip: hasHideTooltip
  })), /*#__PURE__*/_react.default.createElement(InputWithButton, {
    className: className,
    disabled: disabled,
    iconColour: iconColour,
    readOnly: readOnly,
    iconRotate: iconRotate,
    error: touched && errors && touched[id] && errors[id]
  }, mask ? /*#__PURE__*/_react.default.createElement(_reactTextMask.default, _extends({
    error: errors,
    touched: touched,
    type: type,
    placeholder: placeholder,
    value: value,
    onChange: function onChange(e) {
      handleChange(e);
    },
    onBlur: handleBlur,
    onFocus: function onFocus() {
      handleFocus();
    },
    disabled: disabled,
    readOnly: readOnly,
    id: id,
    onClick: onInputClick,
    mask: mask,
    autocomplete: autocomplete
  }, extraAttributes)) : /*#__PURE__*/_react.default.createElement("input", _extends({
    type: type,
    placeholder: placeholder,
    value: value,
    onChange: handleChange,
    onBlur: handleBlur,
    onFocus: function onFocus() {
      handleFocus();
    },
    disabled: disabled,
    readOnly: readOnly,
    id: id,
    onClick: onInputClick,
    autoComplete: autocomplete
  }, extraAttributes)), /*#__PURE__*/_react.default.createElement("button", {
    type: "button",
    onClick: !disabled ? onButtonClick : function () {}
  }, /*#__PURE__*/_react.default.createElement("i", {
    className: buttonIcon
  }))));
}

InputWithButtonComponent.propTypes = {
  /**
   * Define as classes CSS do componente.
   */
  className: _propTypes.default.string,

  /**
   * Define tipo do input (conforme as propriedades do HTML).
   */
  type: _propTypes.default.string,

  /**
   * Propriedade que define se o componente está desabilitado ou não.
   */
  disabled: _propTypes.default.bool,

  /**
   * Essa propriedade deve receber uma string que será apresentada no input.
   */
  placeholder: _propTypes.default.string,

  /**
   * Deve receber o valor que o usuário digitou e guardará o valor dentro do objeto "value" [vide Formik].
   */
  handleChange: _propTypes.default.func,

  /**
   * Essa propriedade é executada quando o usuário clica fora do input.
   */
  handleBlur: _propTypes.default.func,

  /**
   * A função é chamada sempre que o componente é focado.
   */
  handleFocus: _propTypes.default.func,

  /**
   * Recebe o valor que deve ser guardado.
   */
  value: _propTypes.default.string,

  /**
   * Identificador do input.
   */
  id: _propTypes.default.string,

  /**
   * Objeto com os IDs dos campos do formulário que já foram clicados pelo usuário, essa propriedade é obrigatória para o funcionamento do formik.
   */
  touched: _propTypes.default.object,

  /**
   * A propriedade recebe um objeto que contém os erros relacionados ao seu componente.
   */
  errors: _propTypes.default.object,

  /**
   * Altera a cor do icone do botão.
   */
  iconColour: _propTypes.default.string,

  /**
   * Função disparada ao clicar no componente.
   */
  onButtonClick: _propTypes.default.func,

  /**
   * Objetos de propriedades válidas para o input do HTML.
   */
  extraAttributes: _propTypes.default.object,

  /**
   * Este atributo deve receber o nome do ícone que será exibido no componente.
   */
  buttonIcon: _propTypes.default.string.isRequired,

  /**
   *  Propriedade (boleano) que restringe a digitação dentro do componente.
   */
  readOnly: _propTypes.default.bool,

  /**
   *  Propriedade (boleano) que define se ícone irá rotacionar ou não.
   */
  iconRotate: _propTypes.default.string,

  /**
   * Recebe uma função que será disparada quando o campo for clicado.
   */
  onInputClick: _propTypes.default.func,

  /**
   * Propriedade para exibir a máscara no padrão regex.
   */
  mask: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.array]),

  /**
   * Representa o atributo HTML "autocomplete" do input (recebe "on" ou "off").
   */
  autocomplete: _propTypes.default.string,

  /**
   * Prop própria do formik, deve ser true quando o formulário estiver validando
   */
  isValidating: _propTypes.default.bool,

  /**
   * Essa propriedade define se o tooltip que mostra erro deve ter um botão para fechá-lo
   */
  hasHideTooltip: _propTypes.default.bool
};
InputWithButtonComponent.defaultProps = {
  className: null,
  onButtonClick: function onButtonClick() {},
  handleChange: function handleChange() {},
  handleBlur: function handleBlur() {},
  handleFocus: function handleFocus() {},
  value: "",
  iconColour: "#aaa",
  disabled: false,
  type: "text",
  placeholder: null,
  id: null,
  touched: null,
  errors: {},
  extraAttributes: null,
  readOnly: false,
  iconRotate: null,
  onInputClick: function onInputClick() {},
  mask: null,
  autocomplete: "on"
};