"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _InputWithButton = _interopRequireDefault(require("./InputWithButton"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_enzyme.default.configure({
  adapter: new _enzymeAdapterReact.default()
});

describe('InputWithButton - testando funcionalidades avançadas', function () {
  it('verficando se existe um ícone de exibição', function () {
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react.default.createElement(_InputWithButton.default, {
      buttonIcon: "chicon-arrow-calendar"
    }));
    expect(wrapper.find('i.chicon-arrow-calendar')).toHaveLength(1);
  });
  it('verficando se existe um imput', function () {
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react.default.createElement(_InputWithButton.default, {
      buttonIcon: "chicon-arrow-calendar"
    })); // console.log(wrapper.html())

    expect(wrapper.find('input')).toHaveLength(1);
  });
  it('verificando o placeholder do input', function () {
    var wrapper = (0, _enzyme.shallow)( /*#__PURE__*/_react.default.createElement(_InputWithButton.default, {
      buttonIcon: "chicon-arrow-calendar",
      placeholder: "batata"
    }));
    expect(wrapper.find('input[placeholder="batata"]')).toHaveLength(1);
  });
  it('verficando se função de onClick funciona', function () {
    var onActionButton = jest.fn();
    var button = (0, _enzyme.shallow)( /*#__PURE__*/_react.default.createElement(_InputWithButton.default, {
      onButtonClick: onActionButton,
      buttonIcon: "chicon-arrow-calendar"
    }));
    button.find('button').simulate('click'); // button.find('button').simulate('click');
    // expect(onActionButton.mock.calls.length).toEqual(1);

    expect(onActionButton).toHaveBeenCalledTimes(1);
  });
});