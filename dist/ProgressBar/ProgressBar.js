"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ProgressBarStyled = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: relative;\n  height: 14px;\n  box-sizing: border-box;\n\n  &::before {\n    content: '';\n    position: absolute;\n    border-radius: 5px;\n    border: 1px solid;\n    border-color: ", ";\n    box-sizing: border-box;\n    width: 100%;\n    height: 100%;\n    box-sizing: border-box;\n  }\n\n  & div {\n    border-radius: 5px;\n    position: absolute;\n    width: ", ";\n    height: 100%;\n    background-color: ", ";\n    transition: width;\n    transition-duration: ", ";\n  }\n\n  & span {\n    position: absolute;\n    height: fit-content;\n    z-index: 1;\n    color: ", ";\n    font-size: 12px;\n    text-align: center;\n    font-weight: bold;\n    top: 50%;\n    left: ", ";\n    transform: translate(-50%, -50%);\n    transition: left;\n    transition-duration: ", ";\n  }\n\n  & span::before {\n    content: '';\n    position: absolute;\n    width: 34px;\n    height: 34px;\n    background-color: ", ";\n    z-index: -1;\n    border: 1px solid;\n    border-color: ", ";\n    border-radius: 50%;\n    transform: translate(-50%, -50%);\n    top: 50%;\n    left: 50%;\n  }\n"])), function (props) {
  return props.primaryColor ? "".concat(props.primaryColor) : '#E26226';
}, function (props) {
  return props.percentage ? "".concat(props.percentage, "%") : '0%';
}, function (props) {
  return props.primaryColor ? "".concat(props.primaryColor) : '#E26226';
}, function (props) {
  return props.duration ? "".concat(props.duration, "ms") : '1000ms';
}, function (props) {
  return props.secondaryColor ? "".concat(props.secondaryColor) : '#fff';
}, function (props) {
  return props.percentage ? "".concat(props.percentage, "%") : '0%';
}, function (props) {
  return props.duration ? "".concat(props.duration, "ms") : '1000ms';
}, function (props) {
  return props.primaryColor ? "".concat(props.primaryColor) : '#E26226';
}, function (props) {
  return props.secondaryColor ? "".concat(props.secondaryColor) : '#fff';
});
/**
  * Componente: ProgressBar<br/>
  * Componente que apresenta uma porcentagem, escolhida pelo usuário, na forma de uma barra parcialmente preenchida.
  * @component
  */


function ProgressBar(props) {
  var className = props.className,
      percentage = props.percentage,
      primaryColor = props.primaryColor,
      secondaryColor = props.secondaryColor,
      duration = props.duration;

  var _useState = (0, _react.useState)(percentage),
      _useState2 = _slicedToArray(_useState, 2),
      displayedPercentage = _useState2[0],
      setDisplayedPercentage = _useState2[1];

  function animateValue(start, end, d) {
    var range = end - start;
    var current = start;
    var increment = end > start ? 1 : -1;
    var stepTime = Math.abs(Math.floor(d / range));
    var timer = setInterval(function () {
      current += increment;
      setDisplayedPercentage(current);

      if (current === end) {
        clearInterval(timer);
      }
    }, stepTime);
  }

  (0, _react.useEffect)(function () {
    if (displayedPercentage !== percentage) animateValue(displayedPercentage, percentage, duration / 2);
  }, [percentage]);
  return /*#__PURE__*/_react.default.createElement(ProgressBarStyled, {
    className: className,
    primaryColor: primaryColor,
    secondaryColor: secondaryColor,
    percentage: percentage,
    duration: duration
  }, /*#__PURE__*/_react.default.createElement("div", null), /*#__PURE__*/_react.default.createElement("span", null, "".concat(displayedPercentage || 0, "%")));
}

ProgressBar.propTypes = {
  /**
   * Define as classes CSS da progress bar.
   */
  className: _propTypes.default.string,

  /**
   * Define em qual posição da barra de progresso estará a indicação da porcentagem,
   * além de ser o valor do span que a destaca.
   */
  percentage: _propTypes.default.number.isRequired,

  /**
   * Define o cor do background do trecho preenchido da barra de progresso e do span que destaca a porcentagem.
   */
  primaryColor: _propTypes.default.string,

  /**
   * Define a cor da borda e do texto do span que destaca a porcentagem.
   */
  secondaryColor: _propTypes.default.string,

  /**
   * Duração, em ms, da animação de transição que ocorre quando a porcentagem muda.
   */
  duration: _propTypes.default.number
};
ProgressBar.defaultProps = {
  className: null,
  primaryColor: '#E26226',
  secondaryColor: '#fff',
  duration: 1000
};
var _default = ProgressBar;
exports.default = _default;