"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactTextMask = _interopRequireDefault(require("react-text-mask"));

var _createNumberMask = _interopRequireDefault(require("text-mask-addons/dist/createNumberMask"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Tooltip = _interopRequireDefault(require("../Tooltip/Tooltip"));

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var DivMultipleRange = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\nheight: ", ";\n&::after {\n  background: ", ";\n  content: '';\n  height: ", ";\n  width: ", "px;\n  margin-left: ", "px;\n  position: absolute;\n  }\n  background-color: ", ";\n  border-radius: 5px;\n  position: relative;\n  text-align: center;\n  & input {\n  position: absolute;\n  background-color: transparent;\n  z-index: 1;\n  }\n  & > output {\n    color: #AFAFAF;\n    font-size: 13px;\n    position: absolute;\n    top: ", ";\n    display: flex;\n    flex-direction: ", ";\n    transform: translateX(-50%);\n    align-items: center;\n    & > span {\n      height: 14px;\n      width: max-content;\n      &:first-child {\n        margin-right: 2.5px;\n      }\n    }\n  }\n  @-moz-document url-prefix() {\n    output {\n      top: ", ";\n    }\n  }\n  & .outputOne {\n    margin-left: ", "px;\n  }\n  & .outputTwo {\n    margin-left: calc(", "px + ", "px);\n  }\n"])), function (props) {
  return props.height || '8px';
}, function (props) {
  var _props$theme, _props$theme$colors;

  return ((_props$theme = props.theme) === null || _props$theme === void 0 ? void 0 : (_props$theme$colors = _props$theme.colors) === null || _props$theme$colors === void 0 ? void 0 : _props$theme$colors.primary) || '#000';
}, function (props) {
  return props.height || '8px';
}, function (props) {
  return props.sizeBar;
}, function (props) {
  return props.marginLeftBar;
}, function (props) {
  var _props$theme2, _props$theme2$inputRa;

  return ((_props$theme2 = props.theme) === null || _props$theme2 === void 0 ? void 0 : (_props$theme2$inputRa = _props$theme2.inputRange) === null || _props$theme2$inputRa === void 0 ? void 0 : _props$theme2$inputRa.backgroundInputRange) || '#afafaf';
}, function (props) {
  return "".concat(props.sizeBalls.replace(/\D+/g, '') / 2 + 5, "px");
}, function (props) {
  return props.sizeBar <= 80 && props.sizeBar !== 0 && 'column';
}, function (props) {
  return "".concat(props.sizeBalls.replace(/\D+/g, '') / 2 + 5, "px");
}, function (props) {
  return props.marginLeftBar;
}, function (props) {
  return props.marginLeftBar;
}, function (props) {
  return props.sizeBar;
});

var DivRangeStyled = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  width: 100%;\n  margin: 0 0 7px 0;\n  & > div {\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    & label {\n      font-family: 'Open Sans', 'Arial', sans-serif;\n      color: ", ";\n      font-size: ", ";\n      margin: 0 5px 5px 0;\n      font-weight: bold;\n      @media (max-width: 375px) {\n        width: calc(100% - 171px);\n      }\n    }\n    & > div {\n      position: relative;\n      display: flex;\n      flex-wrap: nowrap;\n      align-items: flex-end;\n      transition: all 0.1s;\n      max-width: 200px;\n      color: ", ";\n      &::before,\n      &::after {\n        font-size: ", ";\n        margin: 0 0 5px 0;\n      }\n      ", "\n      & > div {\n        & div {\n          width: max-content!important;\n        }\n        position: absolute;\n        top: 10px;\n        background-color: red;\n        right: 0;\n      }\n      & input {\n        font-family: 'Open Sans', 'Arial', sans-serif;\n        background-color: inherit;\n        border: none;\n        font-size: ", ";\n        width: calc(100% - ", ");\n        font-weight: bold;\n        text-align: right;\n        color: ", ";\n        padding: 0;\n    }\n  }\n}\n"])), function (props) {
  var _props$theme3, _props$theme3$inputRa;

  return ((_props$theme3 = props.theme) === null || _props$theme3 === void 0 ? void 0 : (_props$theme3$inputRa = _props$theme3.inputRange) === null || _props$theme3$inputRa === void 0 ? void 0 : _props$theme3$inputRa.fontColorLabel) || '#000';
}, function (props) {
  var _props$theme4, _props$theme4$inputRa;

  return ((_props$theme4 = props.theme) === null || _props$theme4 === void 0 ? void 0 : (_props$theme4$inputRa = _props$theme4.inputRange) === null || _props$theme4$inputRa === void 0 ? void 0 : _props$theme4$inputRa.fontSizeLabel) || '14px';
}, function (props) {
  var _props$theme5, _props$theme5$colors;

  return ((_props$theme5 = props.theme) === null || _props$theme5 === void 0 ? void 0 : (_props$theme5$colors = _props$theme5.colors) === null || _props$theme5$colors === void 0 ? void 0 : _props$theme5$colors.primary) || '#000';
}, function (props) {
  var _props$theme6, _props$theme6$inputRa;

  return ((_props$theme6 = props.theme) === null || _props$theme6 === void 0 ? void 0 : (_props$theme6$inputRa = _props$theme6.inputRange) === null || _props$theme6$inputRa === void 0 ? void 0 : _props$theme6$inputRa.fontSizeAfterBefore) || '12px';
}, function (props) {
  var _props$schedule$props;

  if (props.format === 'currency') {
    return "\n        &::before {\n          font-family: 'Open Sans', 'Arial', sans-serif;\n          content: \"".concat(props.schedule[props.format].prefix, "\";\n        }");
  }

  return "\n      &::after {\n        font-family: 'Open Sans', 'Arial', sans-serif;\n        content: \"".concat(((_props$schedule$props = props.schedule[props.format]) === null || _props$schedule$props === void 0 ? void 0 : _props$schedule$props.prefix) || undefined, "\";\n      }");
}, function (props) {
  var _props$theme7, _props$theme7$inputRa;

  return ((_props$theme7 = props.theme) === null || _props$theme7 === void 0 ? void 0 : (_props$theme7$inputRa = _props$theme7.inputRange) === null || _props$theme7$inputRa === void 0 ? void 0 : _props$theme7$inputRa.fontSizeInput) || '30px';
}, function (props) {
  return props.format === 'currency' ? '15px' : '40px';
}, function (props) {
  var _props$theme8, _props$theme8$colors;

  return ((_props$theme8 = props.theme) === null || _props$theme8 === void 0 ? void 0 : (_props$theme8$colors = _props$theme8.colors) === null || _props$theme8$colors === void 0 ? void 0 : _props$theme8$colors.primary) || '#000';
});

var InputRangeStyled = _styledComponents.default.input(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  pointer-events: none;\n  outline: none;\n  position: relative;\n  width: 100%;\n  appearance: none;\n  background-color: ", ";\n  height: ", ";\n  border-radius: 10px;\n  margin: 0;\n  &:focus {\n    outline: none;\n  }\n  &::-ms-track {\n    outline: none;\n    position: relative;\n    width: 100%;\n    appearance: none;\n    background-color: ", ";\n    height: 0px;\n    border-radius: 10px;\n  }\n  &::-moz-range-track {\n    outline: none;\n    position: relative;\n    width: 100%;\n    appearance: none;\n    background-color: ", ";\n    height: 0px;\n    border-radius: 10px;\n  }\n  &::-webkit-slider-runnable-track {\n    background: none;\n  }\n  &::-webkit-slider-thumb {\n    appearance: none;\n    border: none;\n    cursor: pointer;\n    width: ", ";\n    height: ", ";\n    border-radius: 100%;\n    pointer-events: auto;\n    background-color: ", ";\n    }\n  &::-moz-range-thumb {\n    appearance: none;\n    border: none;\n    cursor: pointer;\n    width: ", ";\n    height: ", ";\n    border-radius: 100%;\n    pointer-events: auto;\n    background-color: ", ";\n  }\n  &::-ms-thumb {\n    appearance: none;\n    border: none;\n    cursor: pointer;\n    width: ", ";\n    height: ", ";\n    border-radius: 100%;\n    pointer-events: auto;\n    background-color: ", ";\n  }\n  &:active, &:hover {\n    &::-webkit-slider-thumb {\n      -webkit-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);\n      -moz-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);\n      box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);\n    }\n    &::-moz-range-thumb {\n      -webkit-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);\n      -moz-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);\n      box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);\n    }\n    &::-ms-thumb {\n      -webkit-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);\n      -moz-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);\n      box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);\n    }\n  }\n"])), function (props) {
  var _props$theme9, _props$theme9$inputRa;

  return !props.multiple && (((_props$theme9 = props.theme) === null || _props$theme9 === void 0 ? void 0 : (_props$theme9$inputRa = _props$theme9.inputRange) === null || _props$theme9$inputRa === void 0 ? void 0 : _props$theme9$inputRa.backgroundInputRange) || '#B5B5B5');
}, function (props) {
  return props.height || '8px';
}, function (props) {
  var _props$theme10, _props$theme10$inputR;

  return !props.multiple && (((_props$theme10 = props.theme) === null || _props$theme10 === void 0 ? void 0 : (_props$theme10$inputR = _props$theme10.inputRange) === null || _props$theme10$inputR === void 0 ? void 0 : _props$theme10$inputR.backgroundInputRange) || '#B5B5B5');
}, function (props) {
  var _props$theme11, _props$theme11$inputR;

  return !props.multiple && (((_props$theme11 = props.theme) === null || _props$theme11 === void 0 ? void 0 : (_props$theme11$inputR = _props$theme11.inputRange) === null || _props$theme11$inputR === void 0 ? void 0 : _props$theme11$inputR.backgroundInputRange) || '#B5B5B5');
}, function (props) {
  return props.sizeBalls;
}, function (props) {
  return props.sizeBalls;
}, function (props) {
  var _props$theme12, _props$theme12$colors;

  return ((_props$theme12 = props.theme) === null || _props$theme12 === void 0 ? void 0 : (_props$theme12$colors = _props$theme12.colors) === null || _props$theme12$colors === void 0 ? void 0 : _props$theme12$colors.primary) || '#000';
}, function (props) {
  return props.sizeBalls;
}, function (props) {
  return props.sizeBalls;
}, function (props) {
  var _props$theme13, _props$theme13$colors;

  return ((_props$theme13 = props.theme) === null || _props$theme13 === void 0 ? void 0 : (_props$theme13$colors = _props$theme13.colors) === null || _props$theme13$colors === void 0 ? void 0 : _props$theme13$colors.primary) || '#000';
}, function (props) {
  return props.sizeBalls;
}, function (props) {
  return props.sizeBalls;
}, function (props) {
  var _props$theme14, _props$theme14$colors;

  return ((_props$theme14 = props.theme) === null || _props$theme14 === void 0 ? void 0 : (_props$theme14$colors = _props$theme14.colors) === null || _props$theme14$colors === void 0 ? void 0 : _props$theme14$colors.primary) || '#000';
});

var numberMask = function numberMask() {
  return (0, _createNumberMask.default)({
    prefix: '',
    thousandsSeparatorSymbol: '.',
    requireDecimal: false,
    allowDecimal: false,
    suffix: ''
  });
};

function InputRange(_ref) {
  var _divMultipleRange$cur;

  var label = _ref.label,
      placeholder = _ref.placeholder,
      name = _ref.name,
      value = _ref.value,
      error = _ref.error,
      setFieldValue = _ref.setFieldValue,
      min = _ref.min,
      max = _ref.max,
      step = _ref.step,
      format = _ref.format,
      notMask = _ref.notMask,
      multiple = _ref.multiple,
      labels = _ref.labels,
      sizeBalls = _ref.sizeBalls,
      height = _ref.height,
      handleChange = _ref.handleChange;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      monted = _useState2[0],
      setMonted = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      errorInput = _useState4[0],
      setErrorInput = _useState4[1];

  var _useState5 = (0, _react.useState)(value),
      _useState6 = _slicedToArray(_useState5, 2),
      arrItems = _useState6[0],
      setArrItems = _useState6[1];

  var _useState7 = (0, _react.useState)(0),
      _useState8 = _slicedToArray(_useState7, 2),
      sizeBar = _useState8[0],
      setSizeBar = _useState8[1];

  var _useState9 = (0, _react.useState)(0),
      _useState10 = _slicedToArray(_useState9, 2),
      marginLeftBar = _useState10[0],
      setMarginLeftBar = _useState10[1];

  (0, _react.useEffect)(function () {
    return setMonted(true);
  }, []);
  var a = (0, _react.useRef)(null);
  var b = (0, _react.useRef)(null);
  var divMultipleRange = (0, _react.useRef)(undefined);
  var divRangeWidth = (_divMultipleRange$cur = divMultipleRange.current) === null || _divMultipleRange$cur === void 0 ? void 0 : _divMultipleRange$cur.clientWidth;
  var schedule = {
    currency: {
      prefix: 'R$',
      rem: '19'
    },
    monthly: {
      prefix: 'meses',
      rem: '25'
    }
  };

  function reCalcWidth(v) {
    if (b.current) {
      var _schedule$format;

      b.current.style.width = "".concat(((v === null || v === void 0 ? void 0 : v.toString().replace(/\D+/g, '').length) + 1) * ((_schedule$format = schedule[format]) === null || _schedule$format === void 0 ? void 0 : _schedule$format.rem), "px");
    }
  }

  var calculateSize = function calculateSize() {
    // Calculos para preencher com cor o espaço entre os ranges
    var isSmallerValue = Math.min(arrItems[0], arrItems[1], max);
    var isBiggerValue = Math.max(arrItems[0], arrItems[1], min);
    var newSizeBalls = sizeBalls.replace(/\D+/g, '');

    if (isSmallerValue < min) {
      isSmallerValue = min;
    }

    if (isBiggerValue > max) {
      isBiggerValue = max;
    }

    setSizeBar((isBiggerValue - isSmallerValue) / (max - min) * (divRangeWidth - newSizeBalls));
    setMarginLeftBar(newSizeBalls / 2 + (isSmallerValue - min) / (max - min) * (divRangeWidth - newSizeBalls));
  };

  (0, _react.useEffect)(function () {
    var _a$current, _a$current$props;

    if (monted) reCalcWidth((_a$current = a.current) === null || _a$current === void 0 ? void 0 : (_a$current$props = _a$current.props) === null || _a$current$props === void 0 ? void 0 : _a$current$props.value);
    calculateSize();
    return function () {};
  });

  function testValue(key, val, comp, type) {
    var types = {
      biggerThan: function biggerThan(v, c) {
        return v >= c && key !== 8 && key !== 46 && key !== 37 && key !== 39;
      },
      lessThan: function lessThan(v, c) {
        return v < c && key !== 8 && key !== 46 && key !== 37 && key !== 39;
      }
    };
    return types[type](val, comp);
  }

  (0, _react.useEffect)(function () {
    setArrItems(value);
  }, [value]);
  (0, _react.useEffect)(function () {
    calculateSize();
  }, [arrItems]);

  function toDefineLabelName(v) {
    if (v === 1) {
      return labels.singular ? labels.singular : undefined;
    }

    if (v > 1 && v < max || v === 0) {
      return labels.plural ? labels.plural : undefined;
    }

    if (v === max) {
      return labels.last ? labels.last : labels.plural;
    }

    return v;
  }

  return /*#__PURE__*/_react.default.createElement(DivRangeStyled, {
    schedule: schedule,
    format: format
  }, multiple ? /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, error[name] && /*#__PURE__*/_react.default.createElement(_Tooltip.default, {
    message: error[name]
  }), /*#__PURE__*/_react.default.createElement(DivMultipleRange, {
    multiple: true,
    sizeBar: sizeBar,
    sizeBalls: sizeBalls,
    marginLeftBar: marginLeftBar > 0 ? marginLeftBar : marginLeftBar * -1,
    ref: divMultipleRange,
    height: height
  }, /*#__PURE__*/_react.default.createElement(InputRangeStyled, {
    sizeBalls: sizeBalls,
    name: name,
    type: "range",
    onChange: function onChange(e) {
      setArrItems([parseInt(e.target.value, 10), arrItems[1]]);
      handleChange([parseInt(e.target.value, 10), value[1]]);
    },
    value: arrItems[0],
    min: min,
    max: max,
    step: step,
    height: height
  }), /*#__PURE__*/_react.default.createElement("output", {
    className: "outputOne"
  }, /*#__PURE__*/_react.default.createElement("span", null, arrItems[0] > arrItems[1] ? arrItems[1] : arrItems[0]), /*#__PURE__*/_react.default.createElement("span", null, labels && toDefineLabelName(arrItems[0] > arrItems[1] ? arrItems[1] : arrItems[0]))), /*#__PURE__*/_react.default.createElement(InputRangeStyled, {
    sizeBalls: sizeBalls,
    name: name,
    type: "range",
    onChange: function onChange(e) {
      setArrItems([arrItems[0], parseInt(e.target.value, 10)]);
      handleChange([value[0], parseInt(e.target.value, 10)]);
    },
    value: arrItems[1],
    min: min,
    max: max,
    step: step
  }), /*#__PURE__*/_react.default.createElement("output", {
    className: "outputTwo"
  }, /*#__PURE__*/_react.default.createElement("span", null, arrItems[1] > arrItems[0] ? arrItems[1] : arrItems[0]), /*#__PURE__*/_react.default.createElement("span", null, labels && toDefineLabelName(arrItems[1] > arrItems[0] ? arrItems[1] : arrItems[0]))))) : /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("label", {
    htmlFor: "".concat(name, "_txt")
  }, label), /*#__PURE__*/_react.default.createElement("div", {
    ref: b
  }, ((value < min || value > max) && errorInput || error[name]) && /*#__PURE__*/_react.default.createElement(_Tooltip.default, {
    message: error[name] || errorInput
  }), /*#__PURE__*/_react.default.createElement(_reactTextMask.default, {
    name: name,
    id: "".concat(name, "_txt"),
    type: "text",
    ref: a,
    value: value,
    mask: !notMask && numberMask(),
    onChange: function onChange(e) {
      var _value = parseInt(e.target.value.replace(/\D+/g, ''), 10);

      if (Number.isNaN(_value)) return setFieldValue(name, 0);
      return setFieldValue(name, _value);
    },
    onKeyDown: function onKeyDown(e) {
      var _value = parseInt(e.target.value.replace(/\D+/g, ''), 10);

      if (testValue(e.keyCode, _value, max, 'biggerThan')) {
        setErrorInput("Valor m\xE1ximo permitido: ".concat(max));
        return e.preventDefault();
      }

      if (testValue(e.keyCode, _value, min, 'lessThan')) {
        setErrorInput("Valor m\xEDnimo permitido: ".concat(min));
      }

      if (Number.isNaN(_value)) return setFieldValue(name, 0);
      if (errorInput) setErrorInput(false);
      return true;
    },
    min: min,
    max: max,
    step: step,
    placeholder: placeholder
  }))), /*#__PURE__*/_react.default.createElement(InputRangeStyled, {
    name: name,
    type: "range",
    value: value,
    onChange: function onChange(e) {
      reCalcWidth(e.target.value);
      return setFieldValue(name, parseInt(e.target.value, 10));
    },
    min: min,
    max: max,
    step: step,
    height: height,
    sizeBalls: sizeBalls
  })));
}

InputRange.propTypes = {
  /**
   * Este atributo será o rótulo do input de valor único
   */
  label: _propTypes.default.string.isRequired,

  /**
   * Rever a existência deste atributo
   */
  placeholder: _propTypes.default.string,

  /**
   * Atributo utilizado para identificação
   */
  name: _propTypes.default.string.isRequired,

  /**
   * Valor que a input deve assumir
   */
  value: _propTypes.default.any.isRequired,

  /** 
   * Objeto com os IDs dos campos do formulário setados como error true, essa propriedade é obrigatória para o funcionamento do formik
   */
  error: _propTypes.default.object.isRequired,

  /**
   * Atributo utilizado no Formik para atribuir valores dentro do Formik
   */
  setFieldValue: _propTypes.default.func.isRequired,

  /**
  * Atributo responsável por definir qual será o tipo de máscara <b>monthly (meses)</b> ou <b>currency (valores em dinheiro)</b>
  */
  format: _propTypes.default.string.isRequired,

  /**
   * Valor mínimo do range
   */
  min: _propTypes.default.number.isRequired,

  /**
   * Valor máximo do range
   */
  max: _propTypes.default.number.isRequired,

  /**
   * Intervalo de valores da bolinha
   */
  step: _propTypes.default.number,

  /**
   * Um Objeto de rótulos para multiplas bolinhas com chaves definidas para <b>singular</b>, <b>plural</b> e <b>last</b>
   * Onde: </br> - singular é para caso de valor único [opcional] </br> - plural para caso de valor acima de 1 [opcional]
   * </br> - last para último valor do range [opcional]
   */
  labels: _propTypes.default.object,

  /**
  * Atributo responsável por <b>NÃO</b> atribuir máscara de valores, <b>obs:</b> o tipo de máscara é definido pela propriedade <b>format</b> 
  */
  notMask: _propTypes.default.bool,

  /**
   * Define se o range é de uma bolinha ou duas.
   */
  multiple: _propTypes.default.bool,

  /**
   * Propriedade que define a altura da barra.
   */
  height: _propTypes.default.string,

  /**
   * Definição de tamanho dos dots (bolinhas).
   */
  sizeBalls: _propTypes.default.string,

  /**
   * Função disparada no evento de mudança no valor do input
   */
  handleChange: _propTypes.default.func
};
InputRange.defaultProps = {
  label: null,
  step: 100,
  placeholder: null,
  labels: {},
  notMask: false,
  multiple: false,
  height: '8px',
  sizeBalls: '21px',
  handleChange: function handleChange() {}
};

var _default = /*#__PURE__*/(0, _react.memo)(InputRange);

exports.default = _default;