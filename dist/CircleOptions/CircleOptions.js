"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _style = require("./style");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var generateInitials = function generateInitials(value) {
  var regExp = new RegExp(/(\w{0,1})[^\s]+ ?(\w{0,1})[^\s]+/);
  var regExp2 = new RegExp(/(\w{0,1})[^\s]/);

  if ((value === null || value === void 0 ? void 0 : value.indexOf(' ')) >= 1) {
    return regExp.exec(value).splice(1, 2).join('');
  }

  return regExp2.exec(value)[0];
};

var CircleOptions = function CircleOptions(props) {
  var onAddAction = props.onAddAction,
      items = props.items,
      onDeleteAction = props.onDeleteAction,
      maxItems = props.maxItems,
      deleteAll = props.deleteAll,
      confirmDelete = props.confirmDelete,
      activeItem = props.activeItem,
      itemLabel = props.itemLabel,
      onClickAction = props.onClickAction;

  var _items = (0, _react.useMemo)(function () {
    return items;
  }, [items]);

  var _useState = (0, _react.useState)(activeItem),
      _useState2 = _slicedToArray(_useState, 2),
      localActiveItem = _useState2[0],
      setLocalActiveItem = _useState2[1];

  var _generateInitials = (0, _react.useCallback)(function (name) {
    return generateInitials(name);
  }, []);

  var editOptions = function editOptions(idx, confirm) {
    var array = _toConsumableArray(_items);

    array.splice(idx, 1);
    if (confirm) return confirmDelete({
      index: idx,
      items: _toConsumableArray(array)
    });
    return onDeleteAction({
      index: idx,
      items: _toConsumableArray(array)
    });
  };

  return /*#__PURE__*/_react.default.createElement(_style.CircleOptionsStyled, {
    maxItems: maxItems
  }, (_items === null || _items === void 0 ? void 0 : _items.length) && _items.map(function (item, idx) {
    return /*#__PURE__*/_react.default.createElement("button", {
      key: "".concat(item[itemLabel], "_").concat(idx),
      className: localActiveItem === idx ? 'selected' : '',
      onClick: function onClick() {
        return setLocalActiveItem(idx) && onClickAction && onClickAction(idx);
      }
    }, _generateInitials(item[itemLabel]), (deleteAll || idx > 0) && /*#__PURE__*/_react.default.createElement("span", {
      onClick: function onClick() {
        return confirmDelete ? editOptions(idx, true) : editOptions(idx);
      }
    }, /*#__PURE__*/_react.default.createElement("i", {
      className: 'chicon chicon-close'
    })));
  }) || /*#__PURE__*/_react.default.createElement("span", null, "Nenhum item", /*#__PURE__*/_react.default.createElement("br", null), "para ser exibido."), (_items === null || _items === void 0 ? void 0 : _items.length) < maxItems && /*#__PURE__*/_react.default.createElement("button", {
    id: "CircleOptions",
    onClick: function onClick() {
      return onAddAction(_items);
    }
  }, /*#__PURE__*/_react.default.createElement("i", {
    className: 'chicon chicon-plus'
  })));
};

var _default = /*#__PURE__*/(0, _react.memo)(CircleOptions);

exports.default = _default;
CircleOptions.propTypes = {
  onAddAction: _propTypes.default.func,
  items: _propTypes.default.arrayOf(_propTypes.default.object.isRequired).isRequired,
  onDeleteAction: _propTypes.default.func,
  maxItems: _propTypes.default.number,
  deleteAll: _propTypes.default.bool,
  confirmDelete: _propTypes.default.func,
  activeItem: _propTypes.default.number,
  itemLabel: _propTypes.default.string,
  onClickAction: _propTypes.default.func
};
CircleOptions.defaultProps = {
  onAddAction: function onAddAction() {
    return alert('Envie uma função via a propriedade "onAddAction" para adicionar itens ao seu array (items).');
  },
  onDeleteAction: function onDeleteAction() {
    return alert('Envie uma função via a propriedade "onDeleteAction" para excluir itens do seu array (items).');
  },
  maxItems: 4,
  deleteAll: false,
  confirmDelete: null,
  activeItem: null,
  itemLabel: 'label',
  onClickAction: null
};