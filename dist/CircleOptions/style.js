"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CircleOptionsStyled = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var CircleOptionsStyled = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  height: auto;\n  padding: 5px;\n  width: min-content;\n  background-color: #ffcdb6;\n  border-radius: 50px;\n  position: relative;\n  transition: 0.5s;\n  &:hover {\n    & button:not(:last-child) {\n    margin-right: 13px;\n    }\n    & button:not(#CircleOptions) {\n      outline: none;  \n      & span {\n      position: absolute;\n      display: flex;\n      right: -4px;\n      border-radius: 37px;\n      background-color: #E26226;\n      width: 16px;\n      height: 16px;\n      bottom: 0px;\n        &:hover {\n        background-color: #b73d03;\n        }\n        .chicon-close {\n        font-size: 11px;\n        color: white;\n        margin: 0 auto;\n        margin-top: 3px;\n        margin-left: 2.7px;\n      }\n    }\n  }\n}\n\n  & > span {\n    color: #000;\n    white-space: nowrap;\n    padding: 0 10px 0 10px;\n    font-size: 13px;\n    text-align: center;\n    font-weight: bold;\n  }\n\n  & button {\n    outline: none;\n    cursor: pointer;\n    width: 42px;\n    height: 42px;\n    border-radius: 50px;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    color: #FFFFFF;\n    text-transform: uppercase;\n    font-size: 18px;\n    position: relative;\n    transition: 0.5s;\n    box-shadow: 0px 0px 12px 0.001px #8888885e;\n    background-color: #CDD5F5;\n    color: #3B5BDB;\n    border: none;\n    &:hover {\n      background-color: #afbef7;\n    }\n    span {\n      display: none;\n    }\n    & button:not(:last-child) {\n      transition: 0.5s;\n    }\n    &:not(:first-child) {\n      margin-left: -7px;\n    } \n  }\n\n  ", "\n\n  & button#CircleOptions {\n    background-color: #0C173B;\n    color: #ffffff;\n    &:hover{\n      background-color: #162D7B;\n    }\n    .chicon-plus {\n      font-size: 27px;\n      margin: 0 auto;\n      line-height: 0;\n    }\n  }\n\n  & button:not(#CircleOptions).selected {\n    border: 3px solid #FFFFFF;\n    box-sizing: border-box;\n    font-weight: bold;\n    transition: none;\n    & span {\n      /* bottom: -3.4px; */\n      bottom: -3px;\n      right: -7px;\n      &:before, &:after {\n      bottom: 2.7px;\n      /* bottom: 2px; */\n      }\n    }     \n  }  \n"])), function (props) {
  var text;

  for (var a = 0; a < props.maxItems; a++) {
    text = text + "\n        & button:nth-child(".concat(a, "){\n          z-index: ").concat(props.maxItems - a, ";\n        }\n      ");
  }

  return text;
});

exports.CircleOptionsStyled = CircleOptionsStyled;