"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _dayjs = _interopRequireDefault(require("dayjs"));

var _styles = require("./styles");

var _clipboard = _interopRequireDefault(require("./icons/clipboard.svg"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/**
  * Componente: Timeline
  * @component
  */
function Timeline(_ref) {
  var events = _ref.events,
      text = _ref.text,
      user = _ref.user,
      date = _ref.date,
      format = _ref.format;

  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      formatedEvents = _useState2[0],
      setFormatedEvents = _useState2[1];

  (0, _react.useEffect)(function () {
    var getLast = function getLast(event, i) {
      if (i === events.length - 1) {
        return _objectSpread(_objectSpread({}, event), {}, {
          last: true
        });
      }

      return _objectSpread(_objectSpread({}, event), {}, {
        last: false
      });
    };

    var compare = function compare(a, b) {
      return a[date] < b[date];
    };

    var newEvents = events.map(function (event, i) {
      return getLast(event, i);
    });
    setFormatedEvents(newEvents.sort(compare));
  }, [events]);
  return /*#__PURE__*/_react.default.createElement(_styles.Container, null, /*#__PURE__*/_react.default.createElement("div", {
    className: "timeline"
  }, formatedEvents.map(function (event) {
    return /*#__PURE__*/_react.default.createElement(_styles.Event, {
      last: event.last
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "clipboard"
    }, /*#__PURE__*/_react.default.createElement("img", {
      src: _clipboard.default,
      alt: ""
    })), /*#__PURE__*/_react.default.createElement("p", {
      className: "event-description"
    }, user && /*#__PURE__*/_react.default.createElement("span", {
      className: "user"
    }, "".concat(event[user], " - ")), event[text], " ", /*#__PURE__*/_react.default.createElement("br", null), date && /*#__PURE__*/_react.default.createElement("span", {
      className: "date"
    }, (0, _dayjs.default)(event[date]).format(format))));
  })));
}

Timeline.propTypes = {
  /**
   * Este atributo deve receber um array de objetos contendo os eventos a serem exibidos.
   */
  events: _propTypes.default.array.isRequired,

  /**
   * Este atributo deve ser o atributo do objeto que representa um evento.
   */
  text: _propTypes.default.string.isRequired,

  /**
   * Este atributo deve ser o atributo do objeto que representa o responsável pelo evento.
  */
  user: _propTypes.default.string,

  /**
   * Este atributo deve ser o atributo do objeto que representa a data do evento.
  */
  date: _propTypes.default.string,

  /**
   * Este atributo deve ser o formato de saída da data do evento.
  */
  format: _propTypes.default.string
};
Timeline.defaultProps = {
  user: '',
  date: '',
  format: 'DD/MM/YYYY - HH:mm:ss'
};
var _default = Timeline;
exports.default = _default;