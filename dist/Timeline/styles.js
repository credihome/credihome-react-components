"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Event = exports.Container = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n\n  .user {\n    font-weight: bold;\n  }\n\n  .date {\n    font-size: 14px;\n    color: #707070;\n  }\n\n  .clipboard {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    background-color: #e26226;\n    width: 25px;\n    height: 25px;\n    border-radius: 50%;\n    margin-left: -12px;\n    img {\n      width: 11px;\n    }\n  }\n\n  .event-description {\n    margin: 0px;\n    margin-left: 10px;\n    margin-bottom: 1rem;\n  }\n"])));

exports.Container = Container;

var Event = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  display: flex;\n  \n  border-left: ", ";\n"])), function (props) {
  return props.last ? 'none' : 'solid 1px black';
});

exports.Event = Event;