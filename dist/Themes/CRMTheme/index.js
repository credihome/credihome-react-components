"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = CRMTheme;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = require("styled-components");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var size = {
  mobileM: '320px',
  tablet: '768px',
  laptop: '1024px',
  laptopL: '1440px'
};

function CRMTheme(props) {
  var children = props.children;
  var theme = {
    colors: {
      primary: '#E26226',
      secondary: '#0C173B',
      error: {
        backgroundColor: '#ea2d2d',
        color: '#fff'
      },
      warning: {
        backgroundColor: '#efa500',
        color: '#000'
      }
    },
    input: {
      height: '100%',
      borderRadius: '15px',
      borderColor: '#DDDDDD',
      color: '#777',
      padding: '3px 10px 2px 10px',
      backgroundColorDisabled: '#f3f3f3',
      fontWeight: 'normal',
      fontSize: '13px',
      fontFamily: 'Arial'
    },
    tooltip: {
      padding: '6px 12px',
      fontSize: '14px',
      fontWeight: 'normal',
      arrowPosition: "\n        right: 15%;\n      "
    },
    inputRange: {
      fontSizeAfterBefore: '12px',
      fontSizeInput: '30px',
      fontSizeLabel: '14px',
      fontColorLabel: '#B5B5B5',
      backgroundInputRange: '#B5B5B5'
    },
    breakpoints: {
      mobileM: "(min-width: ".concat(size.mobileM, ")"),
      tablet: "(min-width: ".concat(size.tablet, ")"),
      laptop: "(min-width: ".concat(size.laptop, ")"),
      laptopL: "(min-width: ".concat(size.laptopL, ")")
    },
    select: {
      optionsWidth: '100%',
      optionsMargin: '0 auto',
      minHeight: 'auto'
    }
  };
  return /*#__PURE__*/_react.default.createElement(_styledComponents.ThemeProvider, {
    theme: theme
  }, children);
}