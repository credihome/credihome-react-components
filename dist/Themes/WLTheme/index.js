"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = WLTheme;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = require("styled-components");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function WLTheme(props) {
  var style = props.style,
      device = props.device,
      children = props.children;
  var theme = {
    colors: {
      primary: style.primaryBackgroundColorButton,
      secondary: style.borderBottomColorInput,
      error: {
        backgroundColor: style.validation.error.backgroundColor,
        color: style.validation.error.color
      },
      info: {
        backgroundColor: style.validation.info.backgroundColor,
        color: style.validation.info.color
      },
      warning: {
        backgroundColor: style.validation.warning.backgroundColor,
        color: style.validation.warning.color
      }
    },
    input: {
      height: '35px',
      borderRadius: '5px',
      borderColor: '#707070',
      color: '#000',
      focusBorderBottom: '3px solid',
      padding: '0px 0px 0px 10px',
      margin: '0px 0px 10px 0px',
      backgroundColorDisabled: style.backgroundColorInputDisabled,
      fontColorDisabled: style.fontColorInputDisabled,
      borderColorFocus: style.primaryBackgroundColorButton,
      borderWeightFocus: '2px'
    },
    tooltip: {
      padding: '8px 10px',
      fontSize: '12px',
      fontWeight: '600',
      arrowPosition: "\n        right: 0;\n        left: 0;\n        margin: auto;\n      ",
      lineHeight: {
        mobile: "16px"
      }
    },
    datePicker: {
      calendarPadding: '10px 6px',
      weekFontSize: '13px',
      daysFontSize: '12px',
      headerFontSize: '14px',
      calendarSquare: '29px'
    },
    select: {
      optionsMargin: '0',
      optionsWidth: '100%',
      optionsColor: 'black',
      optionsBorder: '1px solid'
    },
    inputRange: {
      /*
      fontSizeAfterBefore: '14px',
      fontColorLabel: 'red',
      */
    },
    breakpoints: device
  };
  return /*#__PURE__*/_react.default.createElement(_styledComponents.ThemeProvider, {
    theme: theme
  }, children);
}