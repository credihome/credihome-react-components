"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.CloseButton = exports.Info = exports.Arrow = exports.ContentTooltip = exports.WrapperTooltip = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

/**
 * Componente: Tooltip
 * @component
 */
function Tooltip(props) {
  var message = props.message,
      type = props.type,
      backgroundColor = props.backgroundColor,
      textColor = props.textColor,
      theme = props.theme,
      hasHideTooltip = props.hasHideTooltip,
      hideIcon = props.hideIcon,
      hideTooltip = props.hideTooltip,
      setHideTooltip = props.setHideTooltip;

  var validateBackground = function validateBackground() {
    if (backgroundColor) {
      return backgroundColor;
    }

    if (!backgroundColor && type === "error" || !type) {
      var _theme$colors, _theme$colors$error;

      return (theme === null || theme === void 0 ? void 0 : (_theme$colors = theme.colors) === null || _theme$colors === void 0 ? void 0 : (_theme$colors$error = _theme$colors.error) === null || _theme$colors$error === void 0 ? void 0 : _theme$colors$error.backgroundColor) || "#ea2d2d";
    }

    if (!backgroundColor && type === "warning") {
      var _theme$colors2, _theme$colors2$warnin;

      return (theme === null || theme === void 0 ? void 0 : (_theme$colors2 = theme.colors) === null || _theme$colors2 === void 0 ? void 0 : (_theme$colors2$warnin = _theme$colors2.warning) === null || _theme$colors2$warnin === void 0 ? void 0 : _theme$colors2$warnin.backgroundColor) || "#efa500";
    }

    return "#dddddd";
  };

  var setTooltip = function setTooltip() {
    setHideTooltip(true);
  };

  var validateTextColor = function validateTextColor() {
    if (textColor) {
      return textColor;
    }

    if (!textColor && type === "error" || !type) {
      var _theme$colors3, _theme$colors3$error;

      return (theme === null || theme === void 0 ? void 0 : (_theme$colors3 = theme.colors) === null || _theme$colors3 === void 0 ? void 0 : (_theme$colors3$error = _theme$colors3.error) === null || _theme$colors3$error === void 0 ? void 0 : _theme$colors3$error.color) || "#fff";
    }

    if (!textColor && type === "warning" || !type) {
      var _theme$colors4, _theme$colors4$warnin;

      return (theme === null || theme === void 0 ? void 0 : (_theme$colors4 = theme.colors) === null || _theme$colors4 === void 0 ? void 0 : (_theme$colors4$warnin = _theme$colors4.warning) === null || _theme$colors4$warnin === void 0 ? void 0 : _theme$colors4$warnin.color) || "#000";
    }

    return "black";
  };

  return hideTooltip === false && /*#__PURE__*/_react.default.createElement(WrapperTooltip, null, /*#__PURE__*/_react.default.createElement(ContentTooltip, {
    backgroundColor: validateBackground,
    textColor: validateTextColor
  }, /*#__PURE__*/_react.default.createElement(Arrow, {
    backgroundColor: validateBackground
  }), /*#__PURE__*/_react.default.createElement(Info, null, message), hasHideTooltip && /*#__PURE__*/_react.default.createElement(CloseButton, {
    textColor: validateTextColor,
    onClick: function onClick() {
      return setHideTooltip(!hideTooltip);
    },
    type: "button"
  }, /*#__PURE__*/_react.default.createElement("i", {
    className: hideIcon || "chicon-close"
  }))));
}

var WrapperTooltip = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: relative;\n"])));

exports.WrapperTooltip = WrapperTooltip;

var ContentTooltip = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n  text-align: center;\n  border-radius: 5px;\n  padding: ", ";\n  width: auto !important;\n  right: 0;\n  -webkit-box-shadow: 0px 1px 3px 0px rgba(58, 58, 58, 1);\n  -moz-box-shadow: 0px 1px 3px 0px rgba(58, 58, 58, 1);\n  box-shadow: 0px 1px 3px 0px rgba(58, 58, 58, 1);\n  margin-top: -40px !important;\n  z-index: 2;\n  visibility: visible;\n  background-color: ", ";\n  color: ", ";\n  position: absolute;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n"])), function (props) {
  var _props$theme, _props$theme$tooltip;

  return ((_props$theme = props.theme) === null || _props$theme === void 0 ? void 0 : (_props$theme$tooltip = _props$theme.tooltip) === null || _props$theme$tooltip === void 0 ? void 0 : _props$theme$tooltip.padding) || "8px 12px";
}, function (props) {
  return props.backgroundColor;
}, function (props) {
  return props.textColor;
});

exports.ContentTooltip = ContentTooltip;

var Arrow = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n  content: \"\";\n  position: absolute;\n  top: 100%;\n  width: 1px !important;\n  border-width: 10px;\n  border-style: solid;\n  border-color: ", " transparent transparent\n    transparent;\n  ", "\n"])), function (props) {
  return props.backgroundColor;
}, function (props) {
  var _props$theme2, _props$theme2$tooltip;

  return (_props$theme2 = props.theme) !== null && _props$theme2 !== void 0 && (_props$theme2$tooltip = _props$theme2.tooltip) !== null && _props$theme2$tooltip !== void 0 && _props$theme2$tooltip.arrowPosition ? props.theme.tooltip.arrowPosition : "right: 15%;";
});

exports.Arrow = Arrow;

var Info = _styledComponents.default.span(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n  font-family: \"Open Sans\", \"Arial\", sans-serif;\n  line-height: 16px;\n  font-size: ", ";\n  font-weight: ", ";\n  @media (max-width: 320px) {\n    line-height: 16px;\n  }\n  @media (max-width: 768px) {\n    line-height: ", ";\n  }\n"])), function (props) {
  var _props$theme3, _props$theme3$tooltip;

  return ((_props$theme3 = props.theme) === null || _props$theme3 === void 0 ? void 0 : (_props$theme3$tooltip = _props$theme3.tooltip) === null || _props$theme3$tooltip === void 0 ? void 0 : _props$theme3$tooltip.fontSize) || "14px";
}, function (props) {
  var _props$theme4, _props$theme4$tooltip;

  return ((_props$theme4 = props.theme) === null || _props$theme4 === void 0 ? void 0 : (_props$theme4$tooltip = _props$theme4.tooltip) === null || _props$theme4$tooltip === void 0 ? void 0 : _props$theme4$tooltip.fontWeight) || "normal";
}, function (props) {
  var _props$theme5, _props$theme5$tooltip, _props$theme5$tooltip2;

  return ((_props$theme5 = props.theme) === null || _props$theme5 === void 0 ? void 0 : (_props$theme5$tooltip = _props$theme5.tooltip) === null || _props$theme5$tooltip === void 0 ? void 0 : (_props$theme5$tooltip2 = _props$theme5$tooltip.lineHeight) === null || _props$theme5$tooltip2 === void 0 ? void 0 : _props$theme5$tooltip2.mobile) || "26px";
});

exports.Info = Info;

var CloseButton = _styledComponents.default.button(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n  margin-left: 5px;\n  background-color: transparent;\n  border: none;\n  cursor: pointer;\n  padding: 0;\n  flex-align: flex-start;\n  & > i {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    color: ", ";\n  }\n  &:hover {\n    transform: scale(1.2);\n  }\n"])), function (props) {
  return props.textColor;
});

exports.CloseButton = CloseButton;
Tooltip.propTypes = {
  /**
   * Objeto de nome <b>tooltip</b> que pode receber propriedades css como padding, arrowPosition (right, left, top, bottom), fontSize e fontWeight para estilização.
   */
  theme: _propTypes.default.object.isRequired,

  /**
   * Texto a ser exibido dentro do tooltip.
   */
  message: _propTypes.default.string.isRequired,

  /**
   * O tipo recebido nessa mensagem só pode ser uma string. Tipos disponíveis: error ou warning.
   */
  type: _propTypes.default.string,

  /**
   * Essa propriedade define a cor de fundo do tooltip.
   */
  backgroundColor: _propTypes.default.string,

  /**
   * Essa propriedade define a cor de texto do tooltip.
   */
  textColor: _propTypes.default.string,

  /**
   * Essa propriedade é a propriedade responsável por esconder o tooltip
   */
  hideTooltip: _propTypes.default.bool,

  /**
   * Essa propriedade deve ser a função que altera o valor de hideTooltip
   */
  setHideTooltip: _propTypes.default.func,

  /**
   * Essa propriedade define se o tooltip deve ter um botão para fechá-lo
   */
  hasHideTooltip: _propTypes.default.bool,

  /**
   * Essa propriedade define qual ícone deve aparecer no lugar do botão que fecha o Tooltip.
   * Ela deve ser o nome de um chicon. O valor default é "chicon-close"
   */
  hideIcon: _propTypes.default.string
};
Tooltip.defaultProps = {
  type: "error",
  backgroundColor: undefined,
  hasHideTooltip: false,
  hideTooltip: false,
  setHideTooltip: function setHideTooltip() {}
};

var _default = (0, _styledComponents.withTheme)(Tooltip);

exports.default = _default;