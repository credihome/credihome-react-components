"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ModalConfirmation;

var _react = _interopRequireWildcard(require("react"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ModalConfirmation(props) {
  var onConfirm = props.onConfirm,
      onCancel = props.onCancel;
  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("div", {
    style: {
      backgroundColor: 'white',
      color: 'black',
      width: '300px',
      height: 'auto',
      padding: '45px 10px 45px 10px',
      position: 'absolute',
      left: '38%',
      fontSize: '18px',
      textAlign: 'center',
      borderRadius: '7px',
      zIndex: '100'
    }
  }, "Deseja excluir este item?", /*#__PURE__*/_react.default.createElement("br", null), /*#__PURE__*/_react.default.createElement("button", {
    type: "button",
    onClick: function onClick() {
      onConfirm();
    },
    style: {
      marginRight: '7px',
      borderRadius: '11px',
      width: '88px',
      backgroundColor: '#E26226',
      border: 'none',
      color: 'white',
      padding: '3px',
      marginTop: '2rem'
    }
  }, "Sim"), /*#__PURE__*/_react.default.createElement("button", {
    type: "button",
    onClick: function onClick() {
      onCancel();
    },
    style: {
      borderRadius: '11px',
      width: '88px',
      backgroundColor: '#E26226',
      border: 'none',
      color: 'white',
      padding: '3px',
      marginTop: '2rem'
    }
  }, "N\xE3o")), /*#__PURE__*/_react.default.createElement("div", {
    style: {
      position: 'absolute',
      width: '100%',
      height: '1000%',
      bottom: '-672%',
      left: '0',
      backgroundColor: 'black',
      opacity: '0.7',
      zIndex: '99'
    }
  }));
}