"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _Select = _interopRequireDefault(require("./Select"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_enzyme.default.configure({
  adapter: new _enzymeAdapterReact.default()
});

describe('Testes básicos de funcionalidade do Select', function () {
  // O teste abaixo na verdade deveria ter sido feito dentro do próprio componente da input, visando a escrita de teste
  // apenas do component principal (sem os seus filhos)
  it.skip('Teste do placeholder do input utilizado dentro do Select', function () {
    var HtmlSelect = (0, _enzyme.mount)( /*#__PURE__*/_react.default.createElement(_Select.default, {
      placeholder: "Frutas",
      handleChange: function handleChange(v) {
        return console.log(v);
      },
      options: []
    }));
    var placeholder = HtmlSelect.find('input').at(0).props().placeholder;
    expect(placeholder).toBe('Frutas');
    expect(placeholder).not.toBe('Frutaz');
  });
  it('Teste para exibição das opções enviadas ao select', function () {
    var myOptions = [{
      key: 'one',
      label: 'Um'
    }, {
      key: 'two',
      label: 'Dois'
    }, {
      key: 'three',
      label: 'Três'
    }];
    var optionsText = ['Um', 'Dois', 'Três'];
    var HtmlSelect = (0, _enzyme.mount)( /*#__PURE__*/_react.default.createElement(_Select.default, {
      handleChange: function handleChange(v) {
        return console.log(v);
      },
      options: myOptions
    }));
    var options = HtmlSelect.find('.choice-div button');
    options.forEach(function (button) {
      return expect(optionsText.includes(button.text())).toBe(true);
    });
  });
  it('Teste para exibir a mensagem de nenhuma opção encontrada', function () {
    var HtmlSelect = (0, _enzyme.mount)( /*#__PURE__*/_react.default.createElement(_Select.default, {
      noOptionString: "Nenhuma op\xE7\xE3o encontrada",
      handleChange: function handleChange(v) {
        return console.log(v);
      },
      options: []
    }));
    var options = HtmlSelect.find('.choice-div button');
    console.log(options.html());
    expect(options.at(0).text()).toBe('Nenhuma opção encontrada');
  });
  it.only('Teste para ver o que acontece quando uma opção é selecionada', function () {
    var fnMock = jest.fn();
    var optionsText = [{
      key: 'one',
      label: 'Um'
    }, {
      key: 'two',
      label: 'Dois'
    }];
    var HtmlSelect = (0, _enzyme.mount)( /*#__PURE__*/_react.default.createElement(_Select.default, {
      handleChange: function handleChange() {},
      onButtonClick: fnMock,
      options: optionsText
    }));
    var options = HtmlSelect.find('.choice-div button');
    var button = options.at(1);
    button.simulate('mousedown');
    expect(fnMock).toHaveBeenCalledTimes(1);
    button.simulate('mousedown');
    expect(fnMock).toHaveBeenCalledTimes(2);
  });
});