"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _InputWithButton = _interopRequireDefault(require("../InputWithButton/InputWithButton"));

var _OutsideAlerter = _interopRequireDefault(require("../OutsideAlerter/OutsideAlerter"));

var _templateObject;

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var SelectStyled = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  position: relative;\n  margin: ", ";\n  & .tag-row {\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-start;\n    flex-wrap: wrap;\n    align-items: center;\n    margin-left: -2px;\n    margin-top: calc(0.5rem - 2px);\n  }\n  & .choice-div {\n    display: none;\n    flex-direction: column;\n    position: ", ";\n    z-index: 9999;\n    border-radius: 0px 0px 5px 5px;\n    box-shadow: ", ";\n    margin: ", ";\n    padding-top: 0.5rem;\n    width: ", ";\n    max-width: ", ";\n    background-color: #fff;\n    top: ", ";\n    bottom: ", ";\n    height: auto;\n    max-height: ", ";\n    overflow: auto;\n    box-sizing: border-box;\n    border: ", ";\n    border-color: ", ";\n    border-top: none;\n    &.active {\n      display: flex;\n    }\n    & > button {\n      text-align: left;\n      background-color: ", ";\n      border: none;\n      box-sizing: content-box;\n      cursor: pointer;\n      padding: 0.33rem 0.7rem;\n      word-wrap: break-word;\n      color: ", ";\n      user-select: none;\n      min-height: ", ";\n      &:hover {\n        background-color: #eee;\n      }\n      &:last-child {\n        border-radius: 0px 0px 5px 5px;\n      }\n    }\n  }\n"])), function (props) {
  var _props$theme, _props$theme$input;

  return ((_props$theme = props.theme) === null || _props$theme === void 0 ? void 0 : (_props$theme$input = _props$theme.input) === null || _props$theme$input === void 0 ? void 0 : _props$theme$input.margin) || "0";
}, function (props) {
  return props.dynamicAlign ? "fixed" : "absolute";
}, function (props) {
  return props.toDefineShadowProperty ? "2px -4px 2px #00000029" : "2px 4px 2px #00000029";
}, function (props) {
  var _props$theme2, _props$theme2$select;

  return ((_props$theme2 = props.theme) === null || _props$theme2 === void 0 ? void 0 : (_props$theme2$select = _props$theme2.select) === null || _props$theme2$select === void 0 ? void 0 : _props$theme2$select.optionsMargin) || "0 0 0 20px";
}, function (props) {
  var _props$theme3, _props$theme3$select;

  return ((_props$theme3 = props.theme) === null || _props$theme3 === void 0 ? void 0 : (_props$theme3$select = _props$theme3.select) === null || _props$theme3$select === void 0 ? void 0 : _props$theme3$select.optionsWidth) || props.allowOverflow && "inherit" || "calc(100% - 30px)";
}, function (props) {
  return props.allowOverflow;
}, function (props) {
  return props.toAlignTopProperty;
}, function (props) {
  return props.alignBottom ? "25px" : "auto";
}, function (props) {
  return props.maxHeight ? props.maxHeight : "none";
}, function (props) {
  var _props$theme4, _props$theme4$select;

  return ((_props$theme4 = props.theme) === null || _props$theme4 === void 0 ? void 0 : (_props$theme4$select = _props$theme4.select) === null || _props$theme4$select === void 0 ? void 0 : _props$theme4$select.optionsBorder) || "none";
}, function (props) {
  var _props$theme5, _props$theme5$input;

  return ((_props$theme5 = props.theme) === null || _props$theme5 === void 0 ? void 0 : (_props$theme5$input = _props$theme5.input) === null || _props$theme5$input === void 0 ? void 0 : _props$theme5$input.borderColor) || "transparent";
}, function (props) {
  var _props$theme6, _props$theme6$select;

  return ((_props$theme6 = props.theme) === null || _props$theme6 === void 0 ? void 0 : (_props$theme6$select = _props$theme6.select) === null || _props$theme6$select === void 0 ? void 0 : _props$theme6$select.backgroundColorOptions) || "#fff";
}, function (props) {
  var _props$theme7, _props$theme7$select;

  return ((_props$theme7 = props.theme) === null || _props$theme7 === void 0 ? void 0 : (_props$theme7$select = _props$theme7.select) === null || _props$theme7$select === void 0 ? void 0 : _props$theme7$select.optionsColor) || "#E26226";
}, function (props) {
  var _props$theme8, _props$theme8$select;

  return ((_props$theme8 = props.theme) === null || _props$theme8 === void 0 ? void 0 : (_props$theme8$select = _props$theme8.select) === null || _props$theme8$select === void 0 ? void 0 : _props$theme8$select.minHeight) || "17px";
});
/**
 * Componente: Select
 * @component
 */


function Select(props) {
  var buttonIcon = props.buttonIcon,
      className = props.className,
      errors = props.errors,
      value = props.value,
      options = props.options,
      placeholder = props.placeholder,
      handleChange = props.handleChange,
      handleBlur = props.handleBlur,
      handleFocus = props.handleFocus,
      disabled = props.disabled,
      id = props.id,
      theme = props.theme,
      name = props.name,
      onButtonClick = props.onButtonClick,
      readOnly = props.readOnly,
      touched = props.touched,
      noOptionString = props.noOptionString,
      maxHeight = props.maxHeight,
      optionsLabel = props.optionsLabel,
      optionsKey = props.optionsKey,
      handleFilter = props.handleFilter,
      asyncHandleFilter = props.asyncHandleFilter,
      handleInfiniteScroll = props.handleInfiniteScroll,
      keyPrefix = props.keyPrefix,
      allowOverflow = props.allowOverflow,
      autocomplete = props.autocomplete,
      dynamicAlign = props.dynamicAlign,
      noUp = props.noUp,
      isValidating = props.isValidating,
      hasHideTooltip = props.hasHideTooltip,
      hasMore = props.hasMore;
  var valueObj = options.find(function (element) {
    return element[optionsKey] === value;
  });

  var _useState = (0, _react.useState)(options),
      _useState2 = _slicedToArray(_useState, 2),
      filtered = _useState2[0],
      setFiltered = _useState2[1];

  var _useState3 = (0, _react.useState)(valueObj && valueObj[optionsLabel] || ""),
      _useState4 = _slicedToArray(_useState3, 2),
      display = _useState4[0],
      setDisplay = _useState4[1];

  var _useState5 = (0, _react.useState)(valueObj && valueObj[optionsKey]),
      _useState6 = _slicedToArray(_useState5, 2),
      selected = _useState6[0],
      setSelected = _useState6[1];

  var _useState7 = (0, _react.useState)(false),
      _useState8 = _slicedToArray(_useState7, 2),
      openOptions = _useState8[0],
      setOpenOptions = _useState8[1];

  var _useState9 = (0, _react.useState)(false),
      _useState10 = _slicedToArray(_useState9, 2),
      toAlignTop = _useState10[0],
      setToAlignTop = _useState10[1];

  var _useState11 = (0, _react.useState)(0),
      _useState12 = _slicedToArray(_useState11, 2),
      heightDropOptions = _useState12[0],
      setHeightDropOptions = _useState12[1];

  var _useState13 = (0, _react.useState)(0),
      _useState14 = _slicedToArray(_useState13, 2),
      toAlignBottom = _useState14[0],
      setToAlignBottom = _useState14[1];

  var _useState15 = (0, _react.useState)(false),
      _useState16 = _slicedToArray(_useState15, 2),
      AlignShadowTop = _useState16[0],
      setAlignShadowTop = _useState16[1];

  var optionsRef = (0, _react.useRef)(undefined);
  var lastOptionRef = (0, _react.useRef)(undefined);
  var outsideRef = (0, _react.useRef)(null);
  var didMountRef = (0, _react.useRef)(false);
  var lastOptionCallback = (0, _react.useCallback)(function (option) {
    if (lastOptionRef.current) lastOptionRef.current.disconnect();
    lastOptionRef.current = new IntersectionObserver(function (entries) {
      handleInfiniteScroll(false);

      if (entries[0].isIntersecting && hasMore) {
        handleInfiniteScroll(true);
      }
    });
    if (option) lastOptionRef.current.observe(option);
  }, [lastOptionRef]);

  function resetDisplay() {
    if (selected === undefined) {
      setDisplay("");
    } else {
      setDisplay(options.find(function (v) {
        return v[optionsKey] === selected;
      })[optionsLabel]);
    }
  }

  (0, _OutsideAlerter.default)(outsideRef, function () {
    resetDisplay();
    setOpenOptions(false);
  });

  function filterChoices(labelFilter) {
    if (handleFilter) {
      setFiltered(handleFilter(labelFilter) || []);
    } else if (asyncHandleFilter) {
      asyncHandleFilter(labelFilter).then(function (v) {
        setFiltered(v);
      });
    } else {
      setFiltered(options.filter(function (option) {
        return option[optionsLabel].toLowerCase().search(labelFilter.toLowerCase()) !== -1;
      }));
    }
  }

  (0, _react.useEffect)(function () {
    if (didMountRef.current && value !== selected) {
      var newValueObj = options.find(function (element) {
        return element[optionsKey] === value;
      });
      setDisplay(newValueObj && newValueObj[optionsLabel] || "");
      setSelected(newValueObj && newValueObj[optionsKey]);
    }
  }, [value]);
  (0, _react.useEffect)(function () {
    setFiltered(options);
  }, [options]);
  (0, _react.useEffect)(function () {
    if (!disabled && !readOnly) {
      filterChoices(display.replace(/[\\()[\]]/g, ""));
    }
  }, [display]);
  (0, _react.useEffect)(function () {
    if (!disabled && didMountRef.current) {
      handleChange(selected);
      setFiltered(options);
      if (asyncHandleFilter) asyncHandleFilter("").then(function (v) {
        return setFiltered(v);
      });
    } else if (!didMountRef.current) {
      didMountRef.current = true;
    }
  }, [selected]);
  (0, _react.useEffect)(function () {
    var v = options.find(function (element) {
      return element[optionsKey] === value;
    });

    if (!hasMore) {
      setDisplay(v && v[optionsLabel] || "");
    }

    setSelected(v && v[optionsKey]);
  }, [options]);

  function newInput(e) {
    setOpenOptions(true);
    setDisplay(e.target.value);
  }

  function defineAlignTop() {
    if (dynamicAlign) {
      if (toAlignBottom > 0) {
        return "".concat(toAlignBottom, "px");
      }

      return "initial";
    }

    if (!dynamicAlign) {
      if (toAlignTop) {
        return "initial";
      }

      return "calc(100% - 4px)";
    }

    return false;
  }

  var toDynamicallyAlign = function toDynamicallyAlign() {
    var detailsParentElement = outsideRef.current.getBoundingClientRect();

    if (window.screen.availHeight - detailsParentElement.y - heightDropOptions - detailsParentElement.height > heightDropOptions) {
      setAlignShadowTop(false);
      setToAlignBottom(detailsParentElement.bottom);
    } else {
      setAlignShadowTop(true);
      setToAlignBottom(detailsParentElement.top - heightDropOptions);
    }
  };

  var toDefineDropAlign = function toDefineDropAlign() {
    var detailsParentElement = outsideRef.current.getBoundingClientRect();

    if (window.screen.availHeight - detailsParentElement.y - heightDropOptions - detailsParentElement.height > heightDropOptions || noUp) {
      setToAlignTop(false);
      setAlignShadowTop(false);
    } else {
      setToAlignTop(true);
      setAlignShadowTop(true);
    }
  }; // A função abaixo e suas respectivas funções incluidas são fruto da necessidade de uma
  // verificação dinâmica sobre a expansão das opções do select.
  // O select não foi feito com sua tag correta por limitações de estilização e, a melhor forma
  // encontrada para contornar a situação foi utilizar div e buttons na construção do mesmo.
  // Alguns dos efeitos colaterais dessa decisão é a necessidade de criar funções que seriam do
  // comportamento nativo do select, nas divs/buttons correspondentes.
  // POR DEFAULT, o select averigua e alinha as opções abaixo ou acima do input/button pai.
  // PORÉM, em modais com scroll habilitado, o select expande esse scroll e fica dentro do modal.
  // Quebrando o layout e atrapalhando ver todas as opções. POR ISSO, foi criada a condição com um
  // position:absolute e com todos os calculos baseado na dimensão da tela e posicionamento
  // do input/button pai para alinhar a div de opções. OU SEJA, ao utilizar o select dentro de
  // um modal, é importante definir como true a propriedade de dynamicAlign.


  (0, _react.useEffect)(function () {
    setHeightDropOptions(optionsRef.current.clientHeight);

    if (openOptions === true && !dynamicAlign) {
      toDefineDropAlign();
    }

    if (openOptions === true && dynamicAlign) {
      toDynamicallyAlign();
    }
  }, [openOptions, heightDropOptions, display]);
  return /*#__PURE__*/_react.default.createElement(SelectStyled, {
    className: className,
    dynamicAlign: dynamicAlign,
    ref: outsideRef,
    maxHeight: maxHeight,
    allowOverflow: allowOverflow,
    theme: theme,
    alignBottom: toAlignTop,
    toAlignBottomProperty: toAlignBottom,
    toAlignTopProperty: defineAlignTop(),
    toDefineShadowProperty: AlignShadowTop
  }, /*#__PURE__*/_react.default.createElement(_InputWithButton.default, {
    buttonIcon: buttonIcon,
    iconColour: "#444444",
    iconRotate: "90deg",
    value: display,
    handleChange: function handleChange(e) {
      return newInput(e);
    },
    placeholder: placeholder,
    onButtonClick: function onButtonClick() {
      return setOpenOptions(!openOptions);
    },
    handleBlur: handleBlur,
    handleFocus: handleFocus,
    disabled: disabled,
    id: id,
    touched: touched,
    errors: errors,
    isValidating: isValidating,
    readOnly: readOnly,
    onInputClick: function onInputClick() {
      return setOpenOptions(!openOptions);
    },
    autocomplete: autocomplete,
    hasHideTooltip: hasHideTooltip,
    extraAttributes: {
      name: name
    }
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: openOptions ? "choice-div active" : "choice-div",
    ref: optionsRef
  }, filtered && filtered.length === 0 ? /*#__PURE__*/_react.default.createElement("button", {
    type: "button"
  }, noOptionString) : filtered && filtered.map(function (filteredItem, idx) {
    return /*#__PURE__*/_react.default.createElement("button", {
      type: "button",
      ref: filtered.length === idx + 1 && hasMore ? lastOptionCallback : undefined,
      key: "".concat(keyPrefix, "_").concat(id || name, "_").concat(filteredItem[optionsKey], "_").concat(idx),
      onMouseDown: function onMouseDown() {
        setDisplay(filteredItem[optionsLabel]);
        setSelected(filteredItem[optionsKey]);
        setOpenOptions(false);

        if (filteredItem !== null && filteredItem !== void 0 && filteredItem.onOptionButtonClick) {
          filteredItem.onOptionButtonClick(filteredItem);
        } else {
          onButtonClick(filteredItem);
        }
      }
    }, filteredItem[optionsLabel]);
  })));
}

var _default = Select;
exports.default = _default;
Select.propTypes = {
  /**
   * Este atributo deve receber o nome do ícone que será exibido no select.
   */
  buttonIcon: _propTypes.default.string,

  /**
   * Define as classes CSS do select
   */
  className: _propTypes.default.string,

  /**
   * Define o valor que o select deve assumir contanto que esse valor seja uma key no array de options
   */
  value: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.number]),

  /**
   * Este atributo é um array de objetos, que deve possuir as seguintes propriedades:<br /> <b>label: string|required</b><br /><b>key: string|required</b>.
   */
  options: _propTypes.default.arrayOf(_propTypes.default.shape({
    label: _propTypes.default.string,
    key: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.number]),
    name: _propTypes.default.string,
    value: _propTypes.default.string
  })).isRequired,

  /**
   * Este atributo deve receber uma string para exibir no placeholder do componente.
   */
  placeholder: _propTypes.default.string,

  /**
   * A função é chamada sempre que um evento de clique (seleção da opção) acontece.
   */
  handleChange: _propTypes.default.func.isRequired,

  /**
   * A função é chamada sempre que o select é desfocado.
   */
  handleBlur: _propTypes.default.func,

  /**
   * A função é chamada sempre que o select é focado.
   */
  handleFocus: _propTypes.default.func,

  /**
   * Propriedade que define se o select está desabilitado ou não
   */
  disabled: _propTypes.default.bool,

  /**
   * Identificador do select
   */
  id: _propTypes.default.string.isRequired,

  /**
   * Identificador/nome do select
   */
  name: _propTypes.default.string,

  /**
   * Função disparada ao clicar em alguma opção do select, o mesmo não substitui a função handleChange.
   * <b>Necessário refatorar essa função pois ela é aplicada em todas as opções (mesma função que o handleChange), talvez o correto seria que cada opção tivesse sua ação única.</b>
   */
  onButtonClick: _propTypes.default.func,

  /**
   *  Propriedade que restringe a digitação dentro do select.
   */
  readOnly: _propTypes.default.bool,

  /**
   * Objeto com os IDs dos campos do formulário que já foram clicados pelo usuário, essa propriedade é obrigatória para o funcionamento do formik
   */
  touched: _propTypes.default.object,

  /**
   * Objeto com os IDs dos campos do formulário setados como error true, essa propriedade é obrigatória para o funcionamento do formik
   */
  errors: _propTypes.default.object,

  /**
   * Texto que será exibido caso a opção buscada não seja encontrada.
   */
  noOptionString: _propTypes.default.string,

  /**
   * Define a altura máxima de expansão da caixa de opções do select.
   */
  maxHeight: _propTypes.default.string,

  /**
   * Caso o objeto options não siga o padrão [{key, label}], settar o optionsLabel irá substituir o valor de label<br>
   * <i>Ex: [{key: "true", nome: "Sim"}, {key: "false", nome: "Não"}] funciona se optionsLabel="nome"</i>
   */
  optionsLabel: _propTypes.default.string,

  /**
   * Caso o objeto options não siga o padrão [{key, label}], settar o optionsKey irá substituir o valor de key<br>
   * <i>Ex: [{valor: "true", label: "Sim"}, {valor: "false", label: "Não"}] funciona se optionsKey="valor"</i>
   */
  optionsKey: _propTypes.default.string,

  /**
   * Função de filtragem a ser utilizada caso não queira usar a função default. Comumente utilizada para adicionar passos extras na filtragem
   */
  handleFilter: _propTypes.default.func,

  /**
   * Função de filtragem a ser utilizada caso seja necessário que a função tenha uma promessa dentro dela. Comumente utilizada para adicionar alguma chamada de API na filtragem
   */
  asyncHandleFilter: _propTypes.default.func,

  /**
   * Função de para lidar com o infinite scroll
   */
  handleInfiniteScroll: _propTypes.default.func,

  /**
   * Informe o máximo de largura que a caixa de opções deve ter, é permitido qualquer unidade de medida (CSS) nesta propriedade.
   */
  allowOverflow: _propTypes.default.string,

  /**
   * Adiciona um prefixo não existente para a key do botão, caso tenha por exemplo 2 selects com as mesmas opções
   */
  keyPrefix: _propTypes.default.string,

  /**
   * Representa o atributo HTML "autocomplete" do input utilizado no select, recebe "on" ou "off"
   */
  autocomplete: _propTypes.default.string,

  /**
   * Propriedade para definir o tipo de posicionamento da caixa de opções, sendo as variações internas: 'fixed' ou 'absolute'
   * que foram implementadas para situações de uso em modais, por exemplo pois por default quando a caixa de opções abre pode gerar scroll.
   */
  dynamicAlign: _propTypes.default.bool,

  /**
   * Propriedade que, se for True, define que as opções do select não devem abrir para cima
   */
  noUp: _propTypes.default.bool,

  /**
   * Prop própria do formik, deve ser true quando o formulário estiver validando
   */
  isValidating: _propTypes.default.bool,

  /**
   * Essa propriedade define se o tooltip que mostra erro deve ter um botão para fechá-lo
   */
  hasHideTooltip: _propTypes.default.bool,

  /**
   * Essa propriedade que ativa o infinite scroll e define se há mais items para serem exibidos
   */
  hasMore: _propTypes.default.bool
};
Select.defaultProps = {
  buttonIcon: "chicon-play",
  className: null,
  value: null,
  placeholder: "",
  allowOverflow: "inherit",
  handleBlur: function handleBlur() {},
  handleFocus: function handleFocus() {},
  onButtonClick: function onButtonClick() {},
  disabled: false,
  readOnly: false,
  id: null,
  name: null,
  touched: null,
  errors: {},
  noOptionString: "Nenhuma opção encontrada",
  maxHeight: null,
  optionsLabel: "label",
  keyPrefix: "",
  optionsKey: "key",
  handleFilter: null,
  asyncHandleFilter: null,
  handleInfiniteScroll: null,
  autocomplete: "off",
  dynamicAlign: false,
  noUp: false,
  hasMore: false
};