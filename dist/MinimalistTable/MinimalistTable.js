"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var MinimalistTableStyled = _styledComponents.default.table(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  width: 100%;\n  border-spacing: 0px;\n  border-collapse: collapse;\n\n  & thead tr th {\n    text-align: center;\n    border: 0px;\n    padding: 14px 2px;\n    font-weight: ", ";\n    font-size: ", ";\n    color: ", ";\n    &:first-child {\n      text-align: start;\n    }\n  }\n\n  & tbody:before {\n    height: 3px;\n    display: table-row;\n    content: \"\";\n  }\n\n  & tbody tr {\n    border-bottom: 1px solid\n      ", ";\n  }\n\n  & tbody tr:last-of-type {\n    border-bottom: none;\n  }\n\n  & tbody tr td {\n    padding: 14px 2px;\n    text-align: center;\n    color: ", ";\n    font-size: ", ";\n    &:first-child {\n      text-align: start;\n    }\n  }\n"])), function (props) {
  return props.boldHeadTitle ? "bold" : "inherit";
}, function (props) {
  return props.fontSizeHead && props.fontSizeHead || "12px";
}, function (props) {
  return props.fontColorHead && props.fontColorHead || "#e26126";
}, function (props) {
  return props.dividerColor && props.dividerColor || "#0000001a";
}, function (props) {
  return props.fontColorCell && props.fontColorCell || "#444444";
}, function (props) {
  return props.fontSizeCell && props.fontSizeCell || "14px";
});
/**
  * Component: MinimalistTable
  * @component
  */


function MinimalistTable(props) {
  var className = props.className,
      children = props.children,
      boldHeadTitle = props.boldHeadTitle,
      fontColorHead = props.fontColorHead,
      fontSizeHead = props.fontSizeHead,
      fontSizeCell = props.fontSizeCell,
      fontColorCell = props.fontColorCell,
      dividerColor = props.dividerColor;
  return /*#__PURE__*/_react.default.createElement(MinimalistTableStyled, {
    className: className,
    boldHeadTitle: boldHeadTitle,
    fontSizeHead: fontSizeHead,
    fontSizeCell: fontSizeCell,
    dividerColor: dividerColor,
    fontColorHead: fontColorHead,
    fontColorCell: fontColorCell
  }, children);
}

MinimalistTable.propTypes = {
  /**
   * Define as classes CSS da MinimalistTable
   */
  className: _propTypes.default.string,

  /**
   * Deve receber um elemento React ou função (que devolva um DOM/React), essa propriedade define o que será renderizado dentro deste componente.
   */
  children: _propTypes.default.oneOfType([_propTypes.default.arrayOf(_propTypes.default.node), _propTypes.default.node]).isRequired,

  /**
   * Define se os títulos do cabeçalho será exibido no estilo 'bold' ou não
   */
  boldHeadTitle: _propTypes.default.bool,

  /**
   * Passa uma nova cor para os títulos do cabeçalho
   */
  fontColorHead: _propTypes.default.string,

  /**
  * Passa um novo tamanho de fonte aos títulos do cabeçalho
  */
  fontSizeHead: _propTypes.default.string,

  /**
  * Passa um novo tamanho de fonte as células do corpo da tabela
  */
  fontSizeCell: _propTypes.default.string,

  /**
  * Passa uma nova cor de fonte as células do corpo da tabela
  */
  fontColorCell: _propTypes.default.string,

  /**
  * Altera a cor da divisória entre as linhas da tabela
  */
  dividerColor: _propTypes.default.string
};
MinimalistTable.defaultProps = {
  className: null,
  boldHeadTitle: false,
  fontColorHead: '#e26126',
  fontSizeHead: '12px',
  fontSizeCell: '14px',
  fontColorCell: '#444444',
  dividerColor: '#0000001a'
};
var _default = MinimalistTable;
exports.default = _default;