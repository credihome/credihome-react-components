
# ColoredCircle
ColoredCircle é uma div redonda que pode ter qualquer cor.

## Props
| Prop | Tipo | Default | Descrição |
|---|---|---|---|
| className | string | `null` | Classes a serem adicionadas no componente, separadas por `' '` (espaço) |
| color | string | `#DDDDDD` | Cor do círculo |
| size | string | `1rem` | Tanto a altura e a espessura que o círculo vai ter |
