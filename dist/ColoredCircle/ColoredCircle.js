"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ColoredCircle;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ColoredCircleStyled = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  width: ", ";;\n  background-color: ", ";\n  border-radius: 50%;\n  &:after {\n    content: \"\";\n    display: block;\n    padding-bottom: 100%;\n  }\n"])), function (props) {
  return props.size ? props.size : '1rem';
}, function (props) {
  return props.color ? props.color : '#DDDDDD';
});

function ColoredCircle(props) {
  var className = props.className,
      color = props.color,
      size = props.size;
  return /*#__PURE__*/_react.default.createElement(ColoredCircleStyled, {
    className: className,
    color: color,
    size: size
  });
}

ColoredCircle.propTypes = {
  className: _propTypes.default.string,
  color: _propTypes.default.string,
  size: _propTypes.default.string
};
ColoredCircle.defaultProps = {
  className: null,
  color: '#DDDDDD',
  size: '1rem'
};