"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = RoundBorderBox;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var RoundBorderBoxStyled = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n  border-radius: 5px;\n  background-color: #fff;\n  box-shadow: 0px 3px 6px #0000001A;\n  padding: ", ";\n  position: ", ";\n"])), function (props) {
  return props.padding ? props.padding : '1rem';
}, function (props) {
  return props.position ? props.position : 'static';
});

function RoundBorderBox(props) {
  var children = props.children,
      className = props.className,
      padding = props.padding,
      position = props.position;
  return /*#__PURE__*/_react.default.createElement(RoundBorderBoxStyled, {
    className: className,
    padding: padding,
    position: position
  }, children);
}

RoundBorderBox.propTypes = {
  className: _propTypes.default.string,
  children: _propTypes.default.oneOfType([_propTypes.default.arrayOf(_propTypes.default.node), _propTypes.default.node]).isRequired,
  padding: _propTypes.default.string,
  position: _propTypes.default.string
};
RoundBorderBox.defaultProps = {
  className: null,
  padding: '1rem',
  position: 'static'
};