"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = require("react");

/**
 * Função: useOutsideAlerter<br>
 * Função que chama uma função callback sempre que um clique fora do componente ref for executado
 * 
 * @function
 * @param {node} ref Referência para o componente que não deve disparar a função de callback
 * @param {function} callback Função que é chamada quando ocorre um evento de mouseDown fora de ref
 */
function useOutsideAlerter(ref, callback) {
  /**
   * Alert if clicked on outside of element
   */
  function handleClickOutside(event) {
    if (ref.current && !ref.current.contains(event.target)) {
      callback();
    }
  }

  (0, _react.useEffect)(function () {
    // Bind the event listener
    document.addEventListener('mousedown', handleClickOutside);
    return function () {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside);
    };
  });
}

var _default = useOutsideAlerter;
exports.default = _default;