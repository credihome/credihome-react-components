"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = FilteringHeader;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var FilteringHeaderStyle = _styledComponents.default.th(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n\n  & > div {\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n    justify-content: center;\n    height: fit-content;\n    width: fit-content;\n  }\n\n  & > div > div {\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: space-between;\n    margin-left: 0.5rem;\n  }\n\n  & i {\n    width: 1em;\n    height: 1em;\n    font-size: smaller;\n    position: relative;\n    cursor: pointer;\n    opacity: 1;\n  }\n\n  & i.non-active {\n    opacity: 0.3;\n  }\n\n  & i.non-active:hover {\n    opacity: 1;\n  }\n\n  & i:before {\n    top: 0;\n    left: 0;\n    bottom: 0;\n    position: absolute;\n  }\n"])));

function FilteringHeader(props) {
  var children = props.children,
      className = props.className,
      onClickUp = props.onClickUp,
      onClickDown = props.onClickDown,
      onReset = props.onReset,
      text = props.text,
      activeFilter = props.activeFilter;

  var _React$useState = _react.default.useState(activeFilter || 'both'),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      active = _React$useState2[0],
      setActive = _React$useState2[1];

  var didMountRef = (0, _react.useRef)(false);

  function activate(kind) {
    if (active === kind) {
      setActive('both');
      onReset();
    } else {
      setActive(kind);

      if (kind === 'up') {
        onClickUp();
      } else {
        onClickDown();
      }
    }
  }

  (0, _react.useEffect)(function () {
    if (didMountRef.current) {
      activate(activeFilter);
    } else if (!didMountRef.current) {
      didMountRef.current = true;
    }
  }, [activeFilter]);
  return /*#__PURE__*/_react.default.createElement(FilteringHeaderStyle, {
    className: className
  }, /*#__PURE__*/_react.default.createElement("div", null, children || text, /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("i", {
    className: "chicon-arrow-top ".concat(active === 'down' && 'non-active'),
    onClick: function onClick() {
      activate('up');
    }
  }), /*#__PURE__*/_react.default.createElement("i", {
    className: "chicon-arrow-bottom ".concat(active === 'up' && 'non-active'),
    onClick: function onClick() {
      activate('down');
    }
  }))));
}

FilteringHeader.propTypes = {
  className: _propTypes.default.string,
  children: _propTypes.default.oneOfType([_propTypes.default.arrayOf(_propTypes.default.node), _propTypes.default.node]),
  onClickUp: _propTypes.default.func.isRequired,
  onClickDown: _propTypes.default.func.isRequired,
  onReset: _propTypes.default.func.isRequired,
  text: _propTypes.default.string,
  activeFilter: _propTypes.default.string
};
FilteringHeader.defaultProps = {
  className: null,
  children: null,
  text: null,
  activeFilter: null
};