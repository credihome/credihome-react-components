"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _enzyme = _interopRequireWildcard(require("enzyme"));

var _enzymeAdapterReact = _interopRequireDefault(require("enzyme-adapter-react-16"));

var _FilteringHeader = _interopRequireDefault(require("./FilteringHeader"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_enzyme.default.configure({
  adapter: new _enzymeAdapterReact.default()
});

var setup = function setup(defaultProps) {
  var _defaultProps$text = defaultProps.text,
      text = _defaultProps$text === void 0 ? 'Test' : _defaultProps$text,
      _defaultProps$onClick = defaultProps.onClickUp,
      onClickUp = _defaultProps$onClick === void 0 ? function () {} : _defaultProps$onClick,
      _defaultProps$onClick2 = defaultProps.onClickDown,
      onClickDown = _defaultProps$onClick2 === void 0 ? function () {} : _defaultProps$onClick2,
      _defaultProps$onReset = defaultProps.onReset,
      onReset = _defaultProps$onReset === void 0 ? function () {} : _defaultProps$onReset,
      _defaultProps$childre = defaultProps.children,
      children = _defaultProps$childre === void 0 ? null : _defaultProps$childre,
      _defaultProps$activeF = defaultProps.activeFilter,
      activeFilter = _defaultProps$activeF === void 0 ? null : _defaultProps$activeF,
      _defaultProps$classNa = defaultProps.className,
      className = _defaultProps$classNa === void 0 ? '' : _defaultProps$classNa;
  var props = {
    text: text,
    onClickUp: onClickUp,
    onClickDown: onClickDown,
    onReset: onReset,
    children: children,
    activeFilter: activeFilter,
    className: className
  };
  return {
    props: props,
    wrapper: (0, _enzyme.shallow)( /*#__PURE__*/_react.default.createElement(_FilteringHeader.default, props, children))
  };
};

describe('Funcionalidades básicas', function () {
  it('O texto passado deve ser o mesmo renderizado', function () {
    var _setup = setup({
      text: 'Testando'
    }),
        wrapper = _setup.wrapper;

    var element = wrapper.find('div').at(0);
    expect(element.text()).toEqual('Testando');
  });
  it('Função deve ser chamada apenas uma vez quando o botão é clicado', function () {
    var clickDown = jest.fn();

    var _setup2 = setup({
      onClickDown: clickDown
    }),
        wrapper = _setup2.wrapper;

    wrapper.find('.chicon-arrow-bottom').simulate('click');
    expect(clickDown).toHaveBeenCalledTimes(1);
  });
  it('A classe non-active deve ser atribuida na arrow Bottom quando arrow Up é clicado', function () {
    var _setup3 = setup({}),
        wrapper = _setup3.wrapper;

    wrapper.find('.chicon-arrow-top').simulate('click');
    var toTest = wrapper.find('.chicon-arrow-bottom');
    expect(toTest.hasClass('non-active')).toBeTruthy();
  });
  it('Fidelidade do filho renderizado', function () {
    var _setup4 = setup({
      children: /*#__PURE__*/_react.default.createElement("p", null, "ol\xE1")
    }),
        wrapper = _setup4.wrapper;

    var array = wrapper.find('p').getElements();
    expect(array[0].type).toBe('p');
  });
  it('Deve chamar a função onReset 1 vez quando o mesmo botão é clicado duas vezes', function () {
    var reset = jest.fn();

    var _setup5 = setup({
      onReset: reset
    }),
        wrapper = _setup5.wrapper;

    wrapper.find('.chicon-arrow-bottom').simulate('click');
    wrapper.find('.chicon-arrow-bottom').simulate('click');
    expect(reset).toHaveBeenCalledTimes(1);
  });
  it('Deve adicionar os tributos da propriedade _className_ quando for passado', function () {
    var _setup6 = setup({
      className: 'teste'
    }),
        wrapper = _setup6.wrapper;

    expect(wrapper.hasClass('teste')).toBeTruthy();
  });
  it('Define o filtro passado como inicial', function () {
    var useStateSpy = jest.spyOn(_react.default, 'useState');

    var _setup7 = setup({
      activeFilter: 'up'
    }),
        wrapper = _setup7.wrapper;

    expect(useStateSpy).toHaveBeenCalledWith('up');
  });
  it('Deve iniciar o filtro com both como valor inicial', function () {
    var useStateSpy = jest.spyOn(_react.default, 'useState');
    var wrapper = setup({});
    expect(useStateSpy).toHaveBeenCalledWith('both');
  });
});