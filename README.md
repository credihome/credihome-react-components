# Credihome-react-components

## Lista de Componentes

[BlueHeaderTable](/src/lib/BlueHeaderTable/BlueHeaderTable.md)

[ColoredCircle](/src/lib/ColoredCircle/ColoredCircle.md)

[DatePicker](/src/lib/DatePicker/DatePicker.md)

[FilteringHeader](/src/lib/FilteringHeader/FilteringHeader.md)

[InputWithButton](/src/lib/InputWithButton/InputWithButton.md)

[OutsideAlerter](/src/lib/OutsideAlerter/OutsideAlerter.md)

[RoundBorderBox](/src/lib/RoundBorderBox/RoundBorderBox.md)

[Select](/src/lib/Select/Select.md)

[Tag](/src/lib/Tag/Tag.md)

[TagSelect](/src/lib/TagSelect/TagSelect.md)

[WhiteHeaderTable](/src/lib/WhiteHeaderTable/WhiteHeaderTable.md)

## Para usuários

### Instalando a biblioteca
Para utilizar a biblioteca credihome-react-components primeiramente precisamos adicionar ela como dependência no seu projeto, para isso basta rodar:

```yarn add https://[user]@bitbucket.org/credihome/credihome-react-components.git```

Onde [user] é seu usuário no BitBucket.
> É a mesma URL que você utilizaria para fazer clone HTTPS

Rodando isso, a biblioteca deve aparecer como qualquer outra dependência.

### Atualizando a biblioteca
Atualizar a biblioteca é super simples, basta rodar `yarn upgrade`

### Utilizando a biblioteca
Para utilizar os componentes disponíveis na biblioteca só é necessário importa-la, para isso, se estiver usando ES6  você pode incluir junto de todas suas importações:

```javascript
import { [component] } from 'credihome-react-library';
```

Onde [component] deve ser substituído por um ou mais componentes da [lista de componentes](#markdown-header-lista-de-componentes) acima.

Para usar os temas específicos você pode envolver a raíz de seu app com a tag de tema, assim como você faria caso fosse usar `<ConnectedRouter>` do react-router ou `<Provider>` do react-redux:

```javascript
import { [PROJECT]Theme } from 'credihome-react-library';

...

<[PROJECT]Theme>
  {App}
</[PROJECT]Theme>
```

## Para desenvolvedores
### Desenvolvimento
Para facilitar o desenvolvimento do componente se você rodar `yarn start` um projeto React bem básico vai rodar na porta padrão (usualmente é a 3000). Nele você pode importar o componente que estiver desenvolvendo e testar à vontade.

Não esqueça de rodar `yarn install` se for a sua primeira vez mexendo com o projeto.

### Antes de pushar
Antes de puxar é necessário fazer 2 coisas:

 1. Aumentar a versão no package.json
 2. Rodar `yarn build`
