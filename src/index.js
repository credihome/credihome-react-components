import React from 'react';
import { render } from 'react-dom';
import DatePickerExample from './examples/DatePickerExample';
import TagSelectExample from './examples/TagSelectExample';
import TooltipExample from './examples/TooltipExample';
import RoundBorderBoxExample from './examples/RoundBorderBoxExample';
import BlueHeaderTableExample from './examples/BlueHeaderTableExample';
import WhiteHeaderTableExample from './examples/WhiteHeaderTableExample';
import ProgressBarExample from './examples/ProgressBarExample';
import InputRangeExample from './examples/InputRange';
import TimelineExample from './examples/TimelineExample';
import MinimalistTableExample from './examples/MinimalistTableExample';
import { CRMTheme } from './lib/index';
import CircleOptionsExample from './examples/CircleOptionsExample';

import './index.css';

const App = () => (
  <CRMTheme>
    <header>
      <h2>CREDIHOME - React Components</h2>
    </header>
    <main>
      <nav>
        <ul>
          <li><a href="#DatePickerExample">DatePicker</a></li>
          <li><a href="#TagSelectExample">TagSelect</a></li>
          <li><a href="#TooltipExample">Tooltip</a></li>
          <li><a href="#RoundBorderBoxExample">RoundBorderBox</a></li>
          <li><a href="#BlueHeaderTableExample">BlueHeaderTable</a></li>
          <li><a href="#WhiteHeaderTableExample">WhiteHeaderTable</a></li>
          <li><a href="#ProgressBarExample">ProgressBar</a></li>
          <li><a href="#InputRange">InputRange</a></li>
          <li><a href="#MinimalistTableExample">MinimalistTable</a></li>
          <li><a href="#TimelineExample">Timeline</a></li>
        </ul>
      </nav>
      <div id="DatePickerExample" className="wrapper">
        <DatePickerExample />
      </div>
      <div id="DatePickerExample" className="wrapper">
        <TagSelectExample />
      </div>
      <div id="TooltipExample" className="wrapper">
        <TooltipExample />
      </div>
      <div id="RoundBorderBoxExample" className="wrapper">
        <RoundBorderBoxExample />
      </div>
      <div id="BlueHeaderTableExample" className="wrapper">
        <BlueHeaderTableExample />
      </div>
      <div id="WhiteHeaderTableExample" className="wrapper">
        <WhiteHeaderTableExample />
      </div>
      <div id="ProgressBarExample" className="wrapper">
        <ProgressBarExample />
      </div>
      <div id="InputRange" className="wrapper">
        <InputRangeExample />
      </div>
      <div id="MinimalistTableExample" className="wrapper">
        <MinimalistTableExample />
      </div>
      <div id="TimelineExample" className="wrapper">
        <TimelineExample />
      </div>
      <div id="CircleOptionsExample" className="wrapper">
        <CircleOptionsExample />
      </div>
    </main>
  </CRMTheme>
);
render(<App />, document.getElementById('root'));
