import Tooltip, { ContentTooltip } from './Tooltip';
import React from 'react';
import Enzyme, { mount, shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new EnzymeAdapter() });

it('Mudança de background color vermelho', () => {
    const wrapper = mount(<Tooltip type="error" />);
    expect(getComputedStyle(wrapper.find(ContentTooltip).getDOMNode())['background-color']).toBe('rgb(234, 45, 45)');
})
