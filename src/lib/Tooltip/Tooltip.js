/* eslint-disable react/prop-types */
import React, { useState } from "react";
import PropTypes from "prop-types";
import styled, { withTheme } from "styled-components";

/**
 * Componente: Tooltip
 * @component
 */
function Tooltip(props) {
  const {
    message,
    type,
    backgroundColor,
    textColor,
    theme,
    hasHideTooltip,
    hideIcon,
    hideTooltip,
    setHideTooltip,
  } = props;

  const validateBackground = () => {
    if (backgroundColor) {
      return backgroundColor;
    }
    if ((!backgroundColor && type === "error") || !type) {
      return theme?.colors?.error?.backgroundColor || "#ea2d2d";
    }
    if (!backgroundColor && type === "warning") {
      return theme?.colors?.warning?.backgroundColor || "#efa500";
    }
    return "#dddddd";
  };

  const setTooltip = () => {
    setHideTooltip(true);
  };

  const validateTextColor = () => {
    if (textColor) {
      return textColor;
    }
    if ((!textColor && type === "error") || !type) {
      return theme?.colors?.error?.color || "#fff";
    }
    if ((!textColor && type === "warning") || !type) {
      return theme?.colors?.warning?.color || "#000";
    }
    return "black";
  };

  return (
    hideTooltip === false && (
      <WrapperTooltip>
        <ContentTooltip
          backgroundColor={validateBackground}
          textColor={validateTextColor}
        >
          <Arrow backgroundColor={validateBackground} />
          <Info>{message}</Info>
          {hasHideTooltip && (
            <CloseButton
            textColor={validateTextColor}
            onClick={() => setHideTooltip(!hideTooltip)}
            type="button"
            >
              <i
                className={hideIcon || "chicon-close"}
              />
            </CloseButton>
          )}
        </ContentTooltip>
      </WrapperTooltip>
    )
  );
}

export const WrapperTooltip = styled.div`
  position: relative;
`;

export const ContentTooltip = styled.div`
  text-align: center;
  border-radius: 5px;
  padding: ${(props) => props.theme?.tooltip?.padding || "8px 12px"};
  width: auto !important;
  right: 0;
  -webkit-box-shadow: 0px 1px 3px 0px rgba(58, 58, 58, 1);
  -moz-box-shadow: 0px 1px 3px 0px rgba(58, 58, 58, 1);
  box-shadow: 0px 1px 3px 0px rgba(58, 58, 58, 1);
  margin-top: -40px !important;
  z-index: 2;
  visibility: visible;
  background-color: ${(props) => props.backgroundColor};
  color: ${(props) => props.textColor};
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Arrow = styled.div`
  content: "";
  position: absolute;
  top: 100%;
  width: 1px !important;
  border-width: 10px;
  border-style: solid;
  border-color: ${(props) => props.backgroundColor} transparent transparent
    transparent;
  ${(props) =>
    props.theme?.tooltip?.arrowPosition
      ? props.theme.tooltip.arrowPosition
      : "right: 15%;"}
`;

export const Info = styled.span`
  font-family: "Open Sans", "Arial", sans-serif;
  line-height: 16px;
  font-size: ${(props) => props.theme?.tooltip?.fontSize || "14px"};
  font-weight: ${(props) => props.theme?.tooltip?.fontWeight || "normal"};
  @media (max-width: 320px) {
    line-height: 16px;
  }
  @media (max-width: 768px) {
    line-height: ${(props) => props.theme?.tooltip?.lineHeight?.mobile || "26px"};
  }
`;

export const CloseButton = styled.button`
  margin-left: 5px;
  background-color: transparent;
  border: none;
  cursor: pointer;
  padding: 0;
  flex-align: flex-start;
  & > i {
    display: flex;
    align-items: center;
    justify-content: center;
    color: ${(props) => props.textColor};
  }
  &:hover {
    transform: scale(1.2);
  }
`;

Tooltip.propTypes = {
  /**
   * Objeto de nome <b>tooltip</b> que pode receber propriedades css como padding, arrowPosition (right, left, top, bottom), fontSize e fontWeight para estilização.
   */
  theme: PropTypes.object.isRequired,
  /**
   * Texto a ser exibido dentro do tooltip.
   */
  message: PropTypes.string.isRequired,
  /**
   * O tipo recebido nessa mensagem só pode ser uma string. Tipos disponíveis: error ou warning.
   */
  type: PropTypes.string,
  /**
   * Essa propriedade define a cor de fundo do tooltip.
   */
  backgroundColor: PropTypes.string,
  /**
   * Essa propriedade define a cor de texto do tooltip.
   */
  textColor: PropTypes.string,
  /**
   * Essa propriedade é a propriedade responsável por esconder o tooltip
   */
  hideTooltip: PropTypes.bool,
  /**
   * Essa propriedade deve ser a função que altera o valor de hideTooltip
   */
  setHideTooltip: PropTypes.func,
  /**
   * Essa propriedade define se o tooltip deve ter um botão para fechá-lo
   */
  hasHideTooltip: PropTypes.bool,
  /**
   * Essa propriedade define qual ícone deve aparecer no lugar do botão que fecha o Tooltip.
   * Ela deve ser o nome de um chicon. O valor default é "chicon-close"
   */
  hideIcon: PropTypes.string,
};

Tooltip.defaultProps = {
  type: "error",
  backgroundColor: undefined,
  hasHideTooltip: false,
  hideTooltip: false,
  setHideTooltip: () => {},
};

export default withTheme(Tooltip);
