/* eslint react/no-array-index-key: 0 */

import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const TagStyled = styled.div`
  background-color: #ffe0d2;
  border-radius: 30px;
  width: fit-content;
  padding: 8px 12px;
  position: relative;
  &.disabled {
    background-color: #dddddd !important;
  }
  & p {
    display: inline-block;
    margin: 0;
    color: #444444;
  }

  & i {
    margin-left: 8px;
    cursor: pointer;
    width: 1rem;
    height: 1rem;
    vertical-align: middle;
  }
`;

/**
 * Componente: Tag <br>
 * Tag que possui texto e ícone. O ícone possui as propriedades de um botão.<br>
 * <i>Utilizado no TagSelect.</i>
 * @component
 */
function Tag(props) {
  const { text, className, onClick, disabled, tagIcon } = props;

  return (
    <TagStyled className={disabled ? `${className} disabled` : className}>
      <p>{text}</p>
      {!disabled && (
        <i
          className={tagIcon || "chicon-close"}
          onClick={(e) => {
            onClick(e);
          }}
        />
      )}
    </TagStyled>
  );
}

Tag.propTypes = {
  /**
   * Define as classes CSS da tag
   */
  className: PropTypes.string,
  /**
   * Define o texto que será mostrado dentro da tag
   */
  text: PropTypes.string.isRequired,
  /**
   * Função que será executada ao clicar no icone
   */
  onClick: PropTypes.func,
  /**
   * Propriedade que define se a tag está desabilitado ou não
   */
  disabled: PropTypes.bool,
  /**
   * Propriedade para indicar qual ícone deverá ser utilizado
   */
  tagIcon: PropTypes.string,
};

Tag.defaultProps = {
  className: null,
  onClick: () => { },
  disabled: false,
  tagIcon: "chicon-close",
};

export default Tag;
