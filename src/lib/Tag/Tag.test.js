import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Tag from './Tag';

Enzyme.configure({ adapter: new EnzymeAdapter() });

/*
  No primeiro atributo fica o nome da "esteira" de testes
  Já no segundo é a função que será executada para realizar o teste
*/
describe('Tag Testes - funcionalidades básicas', () => {
  /*
    No primeiro atributo fica o nome do teste a ser realizado
    Já no segundo é a função que será executada para realizar o teste
  */

  it('Verifica se o texto enviado é o mesmo que o exibido', () => {
    const wrapper = shallow(<Tag text="Botão 1" />);
    // wrapper === <div><p>Botão 1</p> <i class="chicon-close" onClick="algumaFunc" /></div>
    expect(wrapper.find("p").text()).toBe("Botão 1");
    // find pode procurar por seletores CSS, ex: tags, class, ids, etc.
    // text busca pelo conteúdo que consta dentro da tag
    // toBe é o que você deseja que o valor final do expect tenha igualdade
  });

  it('O texto enviado não deve ser o mesmo do exibido', () => {
    const wrapper = shallow(<Tag text="Botão 2" />);
    expect(wrapper.find('p').text()).not.toBe("Botão 1");
  });

  /*
    fn -> método do jest para fazer um mock de uma função;
    após fazer o mock, a constante "actionButtonClick" é uma função "vazia"
    e informações sobre ela estará dentro da const tb
  */

  it('Testa se ao clicar executa alguma ação', () => {
    const actionButtonClick = jest.fn();
    const button = shallow(<Tag onClick={actionButtonClick} text="Botão 3" />);
    button.find('i').simulate('click');
    // expect(actionButtonlick.mock.calls.length).toEqual(1);
    expect(actionButtonClick).toHaveBeenCalledTimes(1);
  });

  /*
    props -> verificando propriedade disable
  */

  it('Testando se recebe a classe que desabilita um item', () => {
    const disabled = shallow(<Tag text="teste" disabled />);
    const findDisabled = disabled.find('i').getElements();
    expect(findDisabled).toHaveLength(0);
  });




})