import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const WhiteHeaderTableStyled = styled.table`
  border-spacing: 0;
  width: 100%;
  color: #444444;
  background-color: #fff;

  & > thead {
    font-size: 15px;
  }

  & > thead > tr > th {
    border-bottom: 1px solid #F3F3F3;
    padding: 12px;
    vertical-align: center;
  }

  & > thead > tr > th i {
    color: #DDDDDD;
  }

  & > thead > tr > th i:hover {
    color: #BBBBBB;
  }

  & > tbody {
    font-size: 14px;
  }

  & tr,
  & td {
    padding: 12px;
    text-align: left;
    border-bottom: 1px solid #F3F3F3;
  }
`;

/**
 * Componente: WhiteHeaderTable
 * @component WhiteHeaderTable 
 */

export default function WhiteHeaderTable(props) {
  const {
    className, children,
  } = props;

  return (
    <WhiteHeaderTableStyled className={className}>
      {children}
    </WhiteHeaderTableStyled>
  );
}

WhiteHeaderTable.propTypes = {
  /**
   * Recebe as classes de estilo que serão aplicadas dentro do elemento DOM.
   */
  className: PropTypes.string,
  /**
   * Deve receber um elemento React ou função (que devolva um DOM/React), o que ser enviado aqui, será renderizado dentro deste componente.
   */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

WhiteHeaderTable.defaultProps = {
  className: null,
};
