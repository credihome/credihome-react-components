/* eslint react/no-array-index-key: 0 */

import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styled, { withTheme } from 'styled-components';
import InputWithButton from '../InputWithButton/InputWithButton';
import momentPTBR from './Utils/pt-br';
import outsideAlerter from '../OutsideAlerter/OutsideAlerter';
import Select from '../Select/Select';


moment.locale('pt-br', momentPTBR);

const Overlay = styled.div`
display: ${(props) => (props.active ? 'block' : 'none')};
position: fixed;
top: 0;
left: 0;
z-index: 2;
opacity: 0.6;
background-color: #707070;
height: 100%;
width: 100%;

@media (min-width: 768px) {
  display: none;
}
`;
const DatePickerStyled = styled.div`
  position: relative;
  margin: ${(props) => props.theme?.input?.margin || '0'};

  & input[type=date]::-webkit-clear-button, /* blue cross */
  & input[type=date]::-webkit-inner-spin-button, /* up */
  & input[type=date]::-webkit-outer-spin-button, /* down */
  & input[type=date]::-webkit-calendar-picker-indicator /* datepicker*/ {
    display: none;
  }

  & input {
    &:focus {
      border: ${(props) => props.theme?.input?.borderWeightFocus || '1px'} solid
        ${(props) => props.theme?.input?.borderColorFocus || 'red'};
    }
    transition: border 0.5s;
  }

  & .datetime-reset-button {
    display: none;
  }

  & .calendar-div {
    position: fixed;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    width: fit-content;
    height: fit-content;
    background-color: #FFFFFF;
    box-shadow: ${(props) => (props.toDefineShadowPropertie ? ' 0px -3px 6px #00000029' : ' 0px 3px 6px #00000029')};
    border-radius: 7px;
    padding: ${(props) => props.theme?.datePicker?.calendarPadding || '1rem'};
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    display: none;
    z-index: 3;
  }

  & .calendar-div.active {
    display: block;
  }

  & .header {
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    margin-bottom: .5rem;

    & .month-select {
      width: 140px;
      padding-right: 4px;
    }

    & .year-select {
      width: 80px;
    }

    & input {
      color: #0C173B;
      font-weight: 500;
      font-family: 'Roboto', sans-serif;
      font-size: 14px;
    }

    & .chicon-arrow-bottom {
      transform: none;
      opacity: 0.25;
      background: ${(props) => props.theme?.colors?.primary || '#E26226'};
      border-radius: 10px;
      color: white;
      height: 18px;
      width: 18px;
    }

    & button > i {
      top: calc(50% - 0.55rem);
      left: 3px;
    }

    & .chicon-arrow-bottom::before {
      line-height: 1.55;
      margin-left: 1.15px;
    }

    & .choice-div {
      height: 200px;
      margin: 4px 0 0 0;

      & button {
        color: #707070;
      }
    }

    & ::-webkit-scrollbar {
      width: 6px;
    }

    & ::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px white;
      border-radius: 10px;
    }

    & ::-webkit-scrollbar-thumb {
      background: ${(props) => props.theme?.colors?.primary || '#e26339'};
      border-radius: 10px;
    }
  }

  & .icon {
    cursor: pointer;
    color: #707070;
  }

  & .table-days {
    width: fit-content;
  }

  & .weekdays {
    color: ${(props) => props.theme?.colors?.primary || '#E26226'};
    font-weight: 700;
    font-size: ${(props) => props.theme?.datePicker?.weekFontSize || '1rem'};
  }

  & th {
    height: ${(props) => props.theme?.datePicker?.calendarSquare || '32px'};
    min-width: ${(props) => props.theme?.datePicker?.calendarSquare || '32px'};
    padding: 0;
    text-align: center;
  }

  & td {
    cursor: pointer;
    text-align: center;
    vertical-align: middle;
    color: #707070;
    height: ${(props) => props.theme?.datePicker?.calendarSquare || '32px'};
    min-width: ${(props) => props.theme?.datePicker?.calendarSquare || '32px'};
    padding: 0;
    font-size: ${(props) => props.theme?.datePicker?.daysFontSize || '0.9rem'};
    border-radius: 50%;
  }

  & td.month-day:hover {
    background-color: #eee;
  }

  & td.month-day.active {
    color: #FFF;
    background-color: ${(props) => props.theme?.colors?.primary || '#E26226'};
  }

  & td.non-month-day {
    cursor: default;
    opacity: 0.3;
  }

  @media (min-width: 768px) {
    & .calendar-div {
     top: ${(props) => (props.dynamicAlign ? `${props.toAlignBottomPropertie}px` : 'auto')};
     right: 0;
     transform: translate(0, 0);
     bottom: ${(props) => (props.toAlignTopPropertie)};
     position: ${(props) => (props.dynamicAlign ? 'fixed' : 'absolute')};
     left: ${(props) => (props.dynamicAlign ? `${props.toAlignLeftPropertie}px` : 'auto')};
    }
  }
`;

/**
  * Component: Date Picker
  * @component
  */
function DatePicker(props) {
  const {
    value,
    handleChange,
    handleBlur,
    className,
    weekdaysLabel,
    monthsLabel,
    padZeros,
    placeholder,
    displayFormat,
    valueFormat,
    dateInput,
    disabled,
    id,
    name,
    touched,
    errors,
    theme,
    invalidDateString,
    mask,
    autocomplete,
    dynamicAlign,
    initYear,
    endYear,
    setFieldError,
    isValidating,
    hasHideTooltip,
    getActualDate
  } = props;

  function parseSelectedValue() {
    if (value && typeof value === 'string') {
      const parsedString = moment(value, valueFormat);
      if (!parsedString.isValid()) {
        // eslint-disable-next-line no-console
        console.warn(`DATE PICKER WARNING: Invalid Date. ${value} is not a valid date or is not in the specified format.`);
        return { displayString: invalidDateString };
      }
      return {
        day: parseInt(parsedString.format('D'), 10),
        month: parseInt(parsedString.format('M'), 10) - 1,
        year: parseInt(parsedString.format('YYYY'), 10),
        displayString: parsedString.format(displayFormat),
      };
    } if (value && value instanceof Date) {
      return {
        day: value.getDate(),
        month: value.getMonth(),
        year: value.getFullYear(),
        displayString: moment().year(value.getFullYear()).month(value.getMonth()).date(value.getDate())
          .format(displayFormat),
      };
    }
    return null;
  }

  const selected = parseSelectedValue();
  const [calendarOpen, setCalendarOpen] = useState(false);
  const [selectedDay, setSelectedDay] = useState((selected?.day) ? selected.day : null);
  const [selectedMonth, setSelectedMonth] = useState((selected?.month) ? selected.month : null);
  const [selectedYear, setSelectedYear] = useState((selected?.year) ? selected.year : null);
  const [displayedMonth, setDisplayedMonth] = useState((selected?.month) ? selected.month : new Date().getMonth());
  const [displayedYear, setDisplayedYear] = useState((selected?.year) ? selected.year : new Date().getFullYear());
  const [calendar, setCalendar] = useState([]);
  const [textDisplay, setTextDisplay] = useState(!dateInput && ((selected && selected.displayString) || ''));
  const [lastWritten, setLastWritten] = useState(false);

  const [heightOfCalendar, setHeightOfCalendar] = useState(0);
  const [toAlignBottom, setToAlignBottom] = useState(0);
  const [toAlignTop, setToAlignTop] = useState(false);
  const [toAlignLeft, setToAlignLeft] = useState(0);
  const [alignShadowTop, setAlignShadowTop] = useState(false);

  const outsideRef = useRef(null);
  const didMountRef = useRef(false);
  const calendarRef = useRef(null);
  outsideAlerter(outsideRef, () => setCalendarOpen(false));
  const [yearArray, renderYearArray] = useState([]);

  useEffect(() => {
    if (didMountRef.current && !lastWritten) {
      const newSelected = parseSelectedValue();
      setSelectedDay(newSelected?.day);
      setSelectedMonth(newSelected?.month);
      setSelectedYear(newSelected?.year);
      setDisplayedMonth(newSelected?.month >= 0 ? newSelected?.month : new Date().getMonth());
      setDisplayedYear(newSelected?.year || new Date().getFullYear());
      setTextDisplay(newSelected?.displayString);
    }
    setLastWritten(false);
  }, [value]);

  useEffect(() => {
    if (!disabled && didMountRef.current && selectedDay && (selectedMonth || (selectedMonth === 0)) && selectedYear) {
      handleChange(moment().year(selectedYear).month(selectedMonth).date(selectedDay)
        .format(valueFormat));
    } else if (!didMountRef.current) {
      didMountRef.current = true;
    } else if (!selectedDay || (!selectedMonth && (selectedMonth !== 0)) || !selectedYear) {
      handleChange(undefined);
    }
  }, [selectedDay, selectedMonth, selectedYear]);

  function monthData(month, year) {
    const firstDay = new Date(year, month, 1).getDay();
    const daysInMonth = new Date(year, month + 1, 0).getDate();
    const daysInPrevMonth = new Date(year, month, 0).getDate();
    return ({
      firstDay,
      daysInMonth,
      daysInPrevMonth,
      hasSixWeeks: (firstDay + daysInMonth > 35),
    });
  }

  function calculateDays(month, year) {
    const days = [];
    const data = monthData(month, year);
    let count = 0;
    for (let i = data.firstDay - 1; i >= 0; i -= 1) {
      days[count] = { day: data.daysInPrevMonth - i, monthDay: false };
      count += 1;
    }
    for (let i = 0; i < data.daysInMonth; i += 1) {
      days[count] = { day: i + 1, monthDay: true };
      count += 1;
    }
    let i = 1;
    for (count; count < (data.hasSixWeeks ? 42 : 35); count += 1) {
      days[count] = { day: i, monthDay: false };
      i += 1;
    }
    const calendarDays = [];
    for (i = 0; i < (data.hasSixWeeks ? 6 : 5); i += 1) {
      calendarDays[i] = days.slice(i * 7, (i + 1) * 7);
    }
    setCalendar(calendarDays);
  }

  useEffect(() => {
    calculateDays(displayedMonth, displayedYear);
  }, [displayedMonth, displayedYear]);

  function renderSelectOptions(inityear, endyear) {
    const options = [];
    // eslint-disable-next-line no-plusplus
    for (let i = inityear; i >= endyear; i--) {
      options.push({
        label: `${i}`,
        key: i,
      });
    }
    renderYearArray(options);
  }

  useEffect(() => {
    renderSelectOptions(initYear, endYear);
  }, [initYear, endYear]);

  // Used when onBlur text input and after selected
  function textFormatter(newDay) {
    if (displayedYear && (displayedMonth || (displayedMonth === 0)) && (newDay || selectedDay)) {
      setTextDisplay(moment().year(displayedYear).month(displayedMonth)
        .date(newDay || selectedDay)
        .format(displayFormat));
    } else {
      setTextDisplay('');
    }
  }

  function changeSelected(newDay) {
    setSelectedDay(newDay);
    setSelectedMonth(displayedMonth);
    setSelectedYear(displayedYear);

    if (!dateInput) {
      textFormatter(newDay);
    }
  }

  function changeSelectedMonth(newMonth) {
    setSelectedMonth(newMonth);
    setSelectedYear(displayedYear);
  }

  function changeSelectedYear(newYear) {
    if (newYear >= endYear) {
      setDisplayedYear(newYear);
      setSelectedYear(newYear);
      setSelectedMonth(displayedMonth);
    }
  }

  function dateWritten(e) {
    if (e.target.valueAsDate) {
      setSelectedDay(e.target.valueAsDate.getUTCDate());
      setSelectedMonth(e.target.valueAsDate.getUTCMonth());
      setSelectedYear(e.target.valueAsDate.getUTCFullYear());
      setDisplayedMonth(e.target.valueAsDate.getUTCMonth());
      setDisplayedYear(e.target.valueAsDate.getUTCFullYear());
    }
  }

  function dateFormatter() {
    if (selectedDay && (selectedMonth || (selectedMonth === 0)) && selectedYear) {
      return `${selectedYear.toString().padStart(4, '0')}-${(selectedMonth + 1).toString().padStart(2, '0')}-${selectedDay.toString().padStart(2, '0')}`;
    }
    return undefined;
  }

  function textWritten(e) {
    const date = moment(e.target.value, displayFormat);
    setLastWritten(true);
    if (date.isValid()) {
      setSelectedDay(parseInt(date.format('D'), 10));
      setSelectedMonth(parseInt(date.format('M'), 10) - 1);
      setSelectedYear(parseInt(date.format('YYYY'), 10));
      setDisplayedMonth(parseInt(date.format('M'), 10) - 1);
      setDisplayedYear(parseInt(date.format('YYYY'), 10));
      setTextDisplay(e.target.value);
    } else {
      setSelectedDay(null);
      setSelectedMonth(null);
      setSelectedYear(null);
      setTextDisplay(e.target.value);
    }
  }

  function renderDay(day, index) {
    const display = (padZeros ? day.day.toString().padStart(2, '0') : day.day);
    if (day.monthDay) {
      if (day.day === selectedDay
        && selectedMonth === displayedMonth
        && selectedYear === displayedYear) {
        return (
          // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
          <td key={`day-${index}`} className="month-day active" onClick={() => changeSelected(day.day)}>{display}</td>
        );
      }
      return (
        // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
        <td key={`day-${index}`} className="month-day" onClick={() => changeSelected(day.day)}>{display}</td>
      );
    }
    return (
      <td key={`non-day-${index}`} className="non-month-day">{display}</td>
    );
  }

  const toDynamicallyAlign = () => {
    const detailsParentElement = outsideRef.current.getBoundingClientRect();
    const calendarProperties = calendarRef.current.getBoundingClientRect();

    if (window.screen.availHeight - detailsParentElement.bottom - detailsParentElement.height > heightOfCalendar) {
      setAlignShadowTop(false);
      setToAlignBottom(detailsParentElement.bottom);
      setToAlignLeft(detailsParentElement.right - calendarProperties.width);
    } else {
      setAlignShadowTop(true);
      setToAlignBottom((detailsParentElement.bottom - heightOfCalendar - detailsParentElement.height));
    }
  };

  const toDefineDropAlign = () => {
    const detailsParentElement = outsideRef.current.getBoundingClientRect();
    if (window.screen.availHeight - detailsParentElement.bottom - detailsParentElement.height > heightOfCalendar) {
      setToAlignTop(false);
      setAlignShadowTop(false);
    } else {
      setToAlignTop(true);
      setAlignShadowTop(true);
    }
  };

  // eslint-disable-next-line consistent-return
  function defineAlignTop() {
    if (dynamicAlign) {
      if (toAlignBottom > 0) {
        return `${toAlignBottom}px`;
      }
      return 'initial';
    }
    if (!dynamicAlign) {
      if (toAlignTop) {
        return '25px';
      }
      return 'initial';
    }
  }

  function validateYear(){
    if(setFieldError){
      const year = selectedYear;
      if( year && year < endYear){
        setFieldError(id,`Ano não pode ser inferior a ${endYear}`)
      }
      if( year && year > initYear){
        setFieldError(id,`Ano não pode ser superior a ${initYear}`)
      }
    }
  }

  useEffect(() => {
    setHeightOfCalendar(calendarRef.current.clientHeight);
    if (calendarOpen === true && !dynamicAlign) {
      toDefineDropAlign();
    }
    if (calendarOpen === true && dynamicAlign) {
      toDynamicallyAlign();
    }
  }, [calendarOpen, heightOfCalendar]);

  function handleClickCalendar() {
    setCalendarOpen(!calendarOpen)

    if (!selectedDay && getActualDate) {
      setSelectedDay(new Date().getDate());
      setSelectedMonth(new Date().getMonth());
      setSelectedYear(new Date().getFullYear());
    }
  }

  return (
    <div>
      <Overlay active={calendarOpen && !disabled} />
      <DatePickerStyled
        className={className}
        ref={outsideRef}
        alignBottom={toAlignTop}
        dynamicAlign={dynamicAlign}
        toAlignBottomPropertie={toAlignBottom}
        toAlignLeftPropertie={toAlignLeft}
        toAlignTopPropertie={defineAlignTop()}
        toDefineShadowPropertie={alignShadowTop}

      >
        <InputWithButton
          name={name}
          buttonIcon="chicon-calendar"
          iconColour={theme?.colors?.primary || '#E26226'}
          alt="Open calendar"
          onButtonClick={handleClickCalendar}
          type={dateInput ? 'date' : 'text'}
          id={id}
          value={dateInput ? dateFormatter() : textDisplay}
          handleChange={dateInput ? (e) => dateWritten(e) : (e) => textWritten(e)}
          handleBlur={dateInput ? handleBlur : () => { textFormatter(); handleBlur(); validateYear(); }}
          placeholder={placeholder}
          disabled={disabled}
          touched={touched}
          extraAttributes={{ name }}
          errors={errors}
          error={(touched && errors) && touched[id] && errors[id]}
          mask={dateInput ? null : mask}
          autocomplete={autocomplete}
          isValidating={isValidating}
          hasHideTooltip={hasHideTooltip}
        />
        <div ref={calendarRef} className={`calendar-div ${calendarOpen && !disabled ? 'active' : ''}`}>
          {(calendarOpen && !disabled) && (
            <>
              <div className="header">
                <Select
                  buttonIcon="chicon-arrow-bottom"
                  className="month-select"
                  handleChange={(newMonth) => changeSelectedMonth(newMonth)}
                  value={selectedMonth}
                  options={monthsLabel}
                  readOnly
                />

                <Select
                  buttonIcon="chicon-arrow-bottom"
                  className="year-select"
                  handleChange={(newYear) => changeSelectedYear(newYear)}
                  value={selectedYear}
                  options={yearArray}
                  readOnly
                />
              </div>
              <table className="table-days">
                <thead className="weekdays">
                  <tr>
                    <th>{weekdaysLabel[0]}</th>
                    <th>{weekdaysLabel[1]}</th>
                    <th>{weekdaysLabel[2]}</th>
                    <th>{weekdaysLabel[3]}</th>
                    <th>{weekdaysLabel[4]}</th>
                    <th>{weekdaysLabel[5]}</th>
                    <th>{weekdaysLabel[6]}</th>
                  </tr>
                </thead>
                <tbody className="days">
                  {calendar.map((week, i) => (
                    <tr key={`week-${i}`}>
                      {week.map((day, index) => (
                        renderDay(day, index)
                      ))}
                    </tr>
                  ))}
                </tbody>
              </table>
            </>
          )}
        </div>
      </DatePickerStyled>
    </div>

  );
}

DatePicker.propTypes = {
  /**
    * Deve receber o estado/variável onde o valor setado pelo componente é guardado.
    */
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date),
  ]),
  /**
   * Deve receber uma função que pega o valor que o usuário escolheu e que guardará o valor dentro da propriedade "value".
   */
  handleChange: PropTypes.func.isRequired,
  /**
   * Essa propriedade é executada quando o usuário clica fora do input.
   */
  handleBlur: PropTypes.func,
  /**
   * Define as classes CSS do DatePicker.
   */
  className: PropTypes.string,
  /**
   * Essa propriedade deve receber um array onde cada item é o nome do dia da semana que será exibido.
   */
  weekdaysLabel: PropTypes.arrayOf(PropTypes.string),
  /**
   * Essa propriedade deve receber um array onde cada item é o nome de cada mês que será exibido.
   */
  monthsLabel: PropTypes.arrayOf(PropTypes.string),
  /**
   * Essa propriedade remove o número 0 quando setado como false, da frente dos números de 1-9, dentro do seletor de datas do calendário.
   */
  padZeros: PropTypes.bool,
  /**
   * Essa propriedade deve receber uma string que será apresentado no input.
   */
  placeholder: PropTypes.string,
  /**
   * Deve receber uma string que informa como será o formato do valor exibido ao usuário, exemplo: <b>'YYYY-MM-DD'</b>.
   */
  displayFormat: PropTypes.string,
  /**
   * Deve receber uma string que informa como será o formato do valor que será guardado, exemplo: <b>'DD-MM-YYYY'</b>.
   */
  valueFormat: PropTypes.string,
  /**
   * Essa propriedade permite que o usuário modifique a data sem que haja uma quebra de layout.
   * E também simula alguns tipos de seletores de data que estão dentro de alguns sistemas operacionais.
   */
  dateInput: PropTypes.bool,
  /**
   * A propriedade deve receber um objeto onde contém os erros relacionados ao seu componente.
   */
  errors: PropTypes.object,
  /**
   * Propriedade que define se o DatePicker está desabilitado ou não.
   */
  disabled: PropTypes.bool,
  /**
   * Identificador do select.
   */
  id: PropTypes.string,
  /**
   * Identificador/nome do select.
   */
  name: PropTypes.string,
  /**
   * Objeto com os IDs dos campos do formulário que já foram clicados pelo usuário, essa propriedade é obrigatória para o funcionamento do formik.
   */
  touched: PropTypes.object,
  /**
   * Recebe um objeto com propriedades que se referem a algum elemento CSS, o conteúdo final da propriedade deve ser CSS.
   */
  theme: PropTypes.object.isRequired,
  /**
   * Quando uma data for inválida, o texto contido nessa propriedade será exibido.
   */
  invalidDateString: PropTypes.string,
  /**
   * Propriedade para exibir a máscara de data no padrão regex.
   */
  mask: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  /**
   * Representa o atributo HTML "autocomplete" do input utilizado no DatePicker, recebe "on" ou "off".
   */
  autocomplete: PropTypes.string,
  /**
   * Propriedade para definir o tipo de posicionamento da caixa de opções, sendo as variações internas: 'fixed' ou 'absolute'
   * que foram implementadas para situações de uso em modais, por exemplo pois por default quando a caixa de opções abre pode gerar scroll.
   */
  dynamicAlign: PropTypes.bool,
  /**
   * Essa propriedade determina à partir de que ano o calendário se inicia.
   */
  initYear: PropTypes.number,
  /**
   * Essa propriedade determina até que ano o calendário será vigente.
   */
  endYear: PropTypes.number,
  /**
   * Prop própria do formik, deve ser true quando o formulário estiver validando
   */
  isValidating: PropTypes.bool,
  /**
   * Essa propriedade define se o tooltip que mostra erro deve ter um botão para fechá-lo
   */
  hasHideTooltip: PropTypes.bool,
  /**
   * Define se o datePicker deve abrir o calendário com o dia atual já selecionado, caso não haja valor nenhum selecionado.
   */
  getActualDate: PropTypes.bool
};

DatePicker.defaultProps = {
  value: null,
  handleBlur: () => {},
  className: null,
  weekdaysLabel: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
  monthsLabel: [{ label: 'Janeiro', key: 0 }, { label: 'Fevereiro', key: 1 }, { label: 'Março', key: 2 }, { label: 'Abril', key: 3 }, { label: 'Maio', key: 4 }, { label: 'Junho', key: 5 }, { label: 'Julho', key: 6 }, { label: 'Agosto', key: 7 }, { label: 'Setembro', key: 8 }, { label: 'Outubro', key: 9 }, { label: 'Novembro', key: 10 }, { label: 'Dezembro', key: 11 }],
  padZeros: true,
  placeholder: 'MM-DD-YYYY',
  displayFormat: 'MM-DD-YYYY',
  valueFormat: 'DD-MM-YYYY',
  dateInput: false,
  errors: {},
  disabled: false,
  id: null,
  name: null,
  touched: null,
  invalidDateString: 'Data inválida',
  mask: null,
  autocomplete: 'on',
  dynamicAlign: false,
  initYear: new Date().getFullYear(),
  endYear: new Date().getFullYear() - 80,
  getActualDate: false,
};

export default withTheme(DatePicker);
