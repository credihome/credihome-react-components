import React from 'react';
import Enzyme, { mount } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import DatePicker from './DatePicker';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('Testes básicos de funcionalidade do DatePicker', () => {

  it('Teste data máxima aceita', () => {
    const testYear = 2010;
    const HtmlDate = mount(<DatePicker initYear={testYear} handleChange={() => {}} theme={{}} />);

    const iconCalendar = HtmlDate.find('.chicon-calendar').at(0);
    iconCalendar.simulate('click');

    const yearsSelect = HtmlDate.find('Select.year-select');

    const firstYear = yearsSelect.find('.choice-div button').at(0);
    
    expect(Number(firstYear.text())).toBe(testYear);
  });

  it.skip('Teste data mínima aceita', () => {});

});
