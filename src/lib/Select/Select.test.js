import React from 'react';
import Enzyme, { mount, shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import Select from './Select';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('Testes básicos de funcionalidade do Select', () => {
  // O teste abaixo na verdade deveria ter sido feito dentro do próprio componente da input, visando a escrita de teste
  // apenas do component principal (sem os seus filhos)
  it.skip('Teste do placeholder do input utilizado dentro do Select', () => {
    const HtmlSelect = mount(<Select placeholder="Frutas" handleChange={(v) => console.log(v)} options={[]} />);
    const placeholder = HtmlSelect.find('input').at(0).props().placeholder;

    expect(placeholder).toBe('Frutas');
    expect(placeholder).not.toBe('Frutaz');
  });

  it('Teste para exibição das opções enviadas ao select', () => {
    const myOptions = [{ key: 'one', label: 'Um' }, { key: 'two', label: 'Dois' }, { key: 'three', label: 'Três' }];
    const optionsText = ['Um', 'Dois', 'Três'];

    const HtmlSelect = mount(<Select handleChange={(v) => console.log(v)} options={myOptions} />);
    const options =  HtmlSelect.find('.choice-div button');
  
    options.forEach((button) => expect(optionsText.includes(button.text())).toBe(true));
  });

  it('Teste para exibir a mensagem de nenhuma opção encontrada', () => {
    const HtmlSelect = mount(<Select noOptionString="Nenhuma opção encontrada" handleChange={(v) => console.log(v)} options={[]} />);
    const options = HtmlSelect.find('.choice-div button');

    console.log(options.html())
    expect(options.at(0).text()).toBe('Nenhuma opção encontrada');
  });

  it.only('Teste para ver o que acontece quando uma opção é selecionada', () => {
    const fnMock = jest.fn();
    const optionsText = [{ key: 'one', label: 'Um'}, {key: 'two', label: 'Dois'}];
    const HtmlSelect = mount(<Select handleChange={() => {}} onButtonClick={fnMock} options={optionsText}/>);
    const options = HtmlSelect.find('.choice-div button');
    const button = options.at(1);

    button.simulate('mousedown');
    expect(fnMock).toHaveBeenCalledTimes(1);
    
    button.simulate('mousedown');
    expect(fnMock).toHaveBeenCalledTimes(2);
  });
});
