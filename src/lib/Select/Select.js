import PropTypes from "prop-types";
import React, { useCallback, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import InputWithButton from "../InputWithButton/InputWithButton";
import outsideAlerter from "../OutsideAlerter/OutsideAlerter";

const SelectStyled = styled.div`
  position: relative;
  margin: ${(props) => props.theme?.input?.margin || "0"};
  & .tag-row {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    flex-wrap: wrap;
    align-items: center;
    margin-left: -2px;
    margin-top: calc(0.5rem - 2px);
  }
  & .choice-div {
    display: none;
    flex-direction: column;
    position: ${(props) => (props.dynamicAlign ? "fixed" : "absolute")};
    z-index: 9999;
    border-radius: 0px 0px 5px 5px;
    box-shadow: ${(props) =>
      props.toDefineShadowProperty
        ? "2px -4px 2px #00000029"
        : "2px 4px 2px #00000029"};
    margin: ${(props) => props.theme?.select?.optionsMargin || "0 0 0 20px"};
    padding-top: 0.5rem;
    width: ${(props) =>
      props.theme?.select?.optionsWidth ||
      (props.allowOverflow && "inherit") ||
      "calc(100% - 30px)"};
    max-width: ${(props) => props.allowOverflow};
    background-color: #fff;
    top: ${(props) => props.toAlignTopProperty};
    bottom: ${(props) => (props.alignBottom ? "25px" : "auto")};
    height: auto;
    max-height: ${(props) => (props.maxHeight ? props.maxHeight : "none")};
    overflow: auto;
    box-sizing: border-box;
    border: ${(props) => props.theme?.select?.optionsBorder || "none"};
    border-color: ${(props) =>
      props.theme?.input?.borderColor || "transparent"};
    border-top: none;
    &.active {
      display: flex;
    }
    & > button {
      text-align: left;
      background-color: ${(props) =>
        props.theme?.select?.backgroundColorOptions || "#fff"};
      border: none;
      box-sizing: content-box;
      cursor: pointer;
      padding: 0.33rem 0.7rem;
      word-wrap: break-word;
      color: ${(props) => props.theme?.select?.optionsColor || "#E26226"};
      user-select: none;
      min-height: ${(props) => props.theme?.select?.minHeight || "17px"};
      &:hover {
        background-color: #eee;
      }
      &:last-child {
        border-radius: 0px 0px 5px 5px;
      }
    }
  }
`;

/**
 * Componente: Select
 * @component
 */

function Select(props) {
  const {
    buttonIcon,
    className,
    errors,
    value,
    options,
    placeholder,
    handleChange,
    handleBlur,
    handleFocus,
    disabled,
    id,
    theme,
    name,
    onButtonClick,
    readOnly,
    touched,
    noOptionString,
    maxHeight,
    optionsLabel,
    optionsKey,
    handleFilter,
    asyncHandleFilter,
    handleInfiniteScroll,
    keyPrefix,
    allowOverflow,
    autocomplete,
    dynamicAlign,
    noUp,
    isValidating,
    hasHideTooltip,
    hasMore,
  } = props;

  const valueObj = options.find((element) => element[optionsKey] === value);
  const [filtered, setFiltered] = useState(options);
  const [display, setDisplay] = useState(
    (valueObj && valueObj[optionsLabel]) || ""
  );
  const [selected, setSelected] = useState(valueObj && valueObj[optionsKey]);
  const [openOptions, setOpenOptions] = useState(false);
  const [toAlignTop, setToAlignTop] = useState(false);
  const [heightDropOptions, setHeightDropOptions] = useState(0);
  const [toAlignBottom, setToAlignBottom] = useState(0);
  const [AlignShadowTop, setAlignShadowTop] = useState(false);

  const optionsRef = useRef(undefined);
  const lastOptionRef = useRef(undefined);

  const outsideRef = useRef(null);
  const didMountRef = useRef(false);

  const lastOptionCallback = useCallback(
    (option) => {
      if (lastOptionRef.current) lastOptionRef.current.disconnect();

      lastOptionRef.current = new IntersectionObserver((entries) => {
        handleInfiniteScroll(false);

        if (entries[0].isIntersecting && hasMore) {
          handleInfiniteScroll(true);
        }
      });

      if (option) lastOptionRef.current.observe(option);
    },
    [lastOptionRef]
  );

  function resetDisplay() {
    if (selected === undefined) {
      setDisplay("");
    } else {
      setDisplay(options.find((v) => v[optionsKey] === selected)[optionsLabel]);
    }
  }

  outsideAlerter(outsideRef, () => {
    resetDisplay();
    setOpenOptions(false);
  });

  function filterChoices(labelFilter) {
    if (handleFilter) {
      setFiltered(handleFilter(labelFilter) || []);
    } else if (asyncHandleFilter) {
      asyncHandleFilter(labelFilter).then((v) => {
        setFiltered(v);
      });
    } else {
      setFiltered(
        options.filter(
          (option) =>
            option[optionsLabel]
              .toLowerCase()
              .search(labelFilter.toLowerCase()) !== -1
        )
      );
    }
  }

  useEffect(() => {
    if (didMountRef.current && value !== selected) {
      const newValueObj = options.find(
        (element) => element[optionsKey] === value
      );
      setDisplay((newValueObj && newValueObj[optionsLabel]) || "");
      setSelected(newValueObj && newValueObj[optionsKey]);
    }
  }, [value]);

  useEffect(() => {
    setFiltered(options);
  }, [options]);

  useEffect(() => {
    if (!disabled && !readOnly) {
      filterChoices(display.replace(/[\\()[\]]/g, ""));
    }
  }, [display]);

  useEffect(() => {
    if (!disabled && didMountRef.current) {
      handleChange(selected);
      setFiltered(options);
      if (asyncHandleFilter) asyncHandleFilter("").then((v) => setFiltered(v));
    } else if (!didMountRef.current) {
      didMountRef.current = true;
    }
  }, [selected]);

  useEffect(() => {
    const v = options.find((element) => element[optionsKey] === value);

    if (!hasMore) {
      setDisplay((v && v[optionsLabel]) || "");
    }
    setSelected(v && v[optionsKey]);
  }, [options]);

  function newInput(e) {
    setOpenOptions(true);
    setDisplay(e.target.value);
  }

  function defineAlignTop() {
    if (dynamicAlign) {
      if (toAlignBottom > 0) {
        return `${toAlignBottom}px`;
      }
      return "initial";
    }
    if (!dynamicAlign) {
      if (toAlignTop) {
        return "initial";
      }
      return "calc(100% - 4px)";
    }
    return false;
  }

  const toDynamicallyAlign = () => {
    const detailsParentElement = outsideRef.current.getBoundingClientRect();
    if (
      window.screen.availHeight -
        detailsParentElement.y -
        heightDropOptions -
        detailsParentElement.height >
      heightDropOptions
    ) {
      setAlignShadowTop(false);
      setToAlignBottom(detailsParentElement.bottom);
    } else {
      setAlignShadowTop(true);
      setToAlignBottom(detailsParentElement.top - heightDropOptions);
    }
  };

  const toDefineDropAlign = () => {
    const detailsParentElement = outsideRef.current.getBoundingClientRect();
    if (
      window.screen.availHeight -
        detailsParentElement.y -
        heightDropOptions -
        detailsParentElement.height >
        heightDropOptions ||
      noUp
    ) {
      setToAlignTop(false);
      setAlignShadowTop(false);
    } else {
      setToAlignTop(true);
      setAlignShadowTop(true);
    }
  };

  // A função abaixo e suas respectivas funções incluidas são fruto da necessidade de uma
  // verificação dinâmica sobre a expansão das opções do select.
  // O select não foi feito com sua tag correta por limitações de estilização e, a melhor forma
  // encontrada para contornar a situação foi utilizar div e buttons na construção do mesmo.
  // Alguns dos efeitos colaterais dessa decisão é a necessidade de criar funções que seriam do
  // comportamento nativo do select, nas divs/buttons correspondentes.
  // POR DEFAULT, o select averigua e alinha as opções abaixo ou acima do input/button pai.
  // PORÉM, em modais com scroll habilitado, o select expande esse scroll e fica dentro do modal.
  // Quebrando o layout e atrapalhando ver todas as opções. POR ISSO, foi criada a condição com um
  // position:absolute e com todos os calculos baseado na dimensão da tela e posicionamento
  // do input/button pai para alinhar a div de opções. OU SEJA, ao utilizar o select dentro de
  // um modal, é importante definir como true a propriedade de dynamicAlign.

  useEffect(() => {
    setHeightDropOptions(optionsRef.current.clientHeight);
    if (openOptions === true && !dynamicAlign) {
      toDefineDropAlign();
    }
    if (openOptions === true && dynamicAlign) {
      toDynamicallyAlign();
    }
  }, [openOptions, heightDropOptions, display]);

  return (
    <SelectStyled
      className={className}
      dynamicAlign={dynamicAlign}
      ref={outsideRef}
      maxHeight={maxHeight}
      allowOverflow={allowOverflow}
      theme={theme}
      alignBottom={toAlignTop}
      toAlignBottomProperty={toAlignBottom}
      toAlignTopProperty={defineAlignTop()}
      toDefineShadowProperty={AlignShadowTop}
    >
      <InputWithButton
        buttonIcon={buttonIcon}
        iconColour="#444444"
        iconRotate="90deg"
        value={display}
        handleChange={(e) => newInput(e)}
        placeholder={placeholder}
        onButtonClick={() => setOpenOptions(!openOptions)}
        handleBlur={handleBlur}
        handleFocus={handleFocus}
        disabled={disabled}
        id={id}
        touched={touched}
        errors={errors}
        isValidating={isValidating}
        readOnly={readOnly}
        onInputClick={() => setOpenOptions(!openOptions)}
        autocomplete={autocomplete}
        hasHideTooltip={hasHideTooltip}
        extraAttributes={{
          name,
        }}
      />
      <div
        className={openOptions ? "choice-div active" : "choice-div"}
        ref={optionsRef}
      >
        {filtered && filtered.length === 0 ? (
          <button type="button">{noOptionString}</button>
        ) : (
          filtered &&
          filtered.map((filteredItem, idx) => (
            <button
              type="button"
              ref={
                filtered.length === idx + 1 && hasMore
                  ? lastOptionCallback
                  : undefined
              }
              key={`${keyPrefix}_${id || name}_${
                filteredItem[optionsKey]
              }_${idx}`}
              onMouseDown={() => {
                setDisplay(filteredItem[optionsLabel]);
                setSelected(filteredItem[optionsKey]);
                setOpenOptions(false);
                if (filteredItem?.onOptionButtonClick) {
                  filteredItem.onOptionButtonClick(filteredItem);
                } else {
                  onButtonClick(filteredItem);
                }
              }}
            >
              {filteredItem[optionsLabel]}
            </button>
          ))
        )}
      </div>
    </SelectStyled>
  );
}

export default Select;

Select.propTypes = {
  /**
   * Este atributo deve receber o nome do ícone que será exibido no select.
   */
  buttonIcon: PropTypes.string,
  /**
   * Define as classes CSS do select
   */
  className: PropTypes.string,
  /**
   * Define o valor que o select deve assumir contanto que esse valor seja uma key no array de options
   */
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  /**
   * Este atributo é um array de objetos, que deve possuir as seguintes propriedades:<br /> <b>label: string|required</b><br /><b>key: string|required</b>.
   */
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      key: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      name: PropTypes.string,
      value: PropTypes.string,
    })
  ).isRequired,
  /**
   * Este atributo deve receber uma string para exibir no placeholder do componente.
   */
  placeholder: PropTypes.string,
  /**
   * A função é chamada sempre que um evento de clique (seleção da opção) acontece.
   */
  handleChange: PropTypes.func.isRequired,
  /**
   * A função é chamada sempre que o select é desfocado.
   */
  handleBlur: PropTypes.func,
  /**
   * A função é chamada sempre que o select é focado.
   */
  handleFocus: PropTypes.func,
  /**
   * Propriedade que define se o select está desabilitado ou não
   */
  disabled: PropTypes.bool,
  /**
   * Identificador do select
   */
  id: PropTypes.string.isRequired,
  /**
   * Identificador/nome do select
   */
  name: PropTypes.string,
  /**
   * Função disparada ao clicar em alguma opção do select, o mesmo não substitui a função handleChange.
   * <b>Necessário refatorar essa função pois ela é aplicada em todas as opções (mesma função que o handleChange), talvez o correto seria que cada opção tivesse sua ação única.</b>
   */
  onButtonClick: PropTypes.func,
  /**
   *  Propriedade que restringe a digitação dentro do select.
   */
  readOnly: PropTypes.bool,
  /**
   * Objeto com os IDs dos campos do formulário que já foram clicados pelo usuário, essa propriedade é obrigatória para o funcionamento do formik
   */
  touched: PropTypes.object,
  /**
   * Objeto com os IDs dos campos do formulário setados como error true, essa propriedade é obrigatória para o funcionamento do formik
   */
  errors: PropTypes.object,
  /**
   * Texto que será exibido caso a opção buscada não seja encontrada.
   */
  noOptionString: PropTypes.string,
  /**
   * Define a altura máxima de expansão da caixa de opções do select.
   */
  maxHeight: PropTypes.string,
  /**
   * Caso o objeto options não siga o padrão [{key, label}], settar o optionsLabel irá substituir o valor de label<br>
   * <i>Ex: [{key: "true", nome: "Sim"}, {key: "false", nome: "Não"}] funciona se optionsLabel="nome"</i>
   */
  optionsLabel: PropTypes.string,
  /**
   * Caso o objeto options não siga o padrão [{key, label}], settar o optionsKey irá substituir o valor de key<br>
   * <i>Ex: [{valor: "true", label: "Sim"}, {valor: "false", label: "Não"}] funciona se optionsKey="valor"</i>
   */
  optionsKey: PropTypes.string,
  /**
   * Função de filtragem a ser utilizada caso não queira usar a função default. Comumente utilizada para adicionar passos extras na filtragem
   */
  handleFilter: PropTypes.func,
  /**
   * Função de filtragem a ser utilizada caso seja necessário que a função tenha uma promessa dentro dela. Comumente utilizada para adicionar alguma chamada de API na filtragem
   */
  asyncHandleFilter: PropTypes.func,
  /**
   * Função de para lidar com o infinite scroll
   */
  handleInfiniteScroll: PropTypes.func,
  /**
   * Informe o máximo de largura que a caixa de opções deve ter, é permitido qualquer unidade de medida (CSS) nesta propriedade.
   */
  allowOverflow: PropTypes.string,
  /**
   * Adiciona um prefixo não existente para a key do botão, caso tenha por exemplo 2 selects com as mesmas opções
   */
  keyPrefix: PropTypes.string,
  /**
   * Representa o atributo HTML "autocomplete" do input utilizado no select, recebe "on" ou "off"
   */
  autocomplete: PropTypes.string,
  /**
   * Propriedade para definir o tipo de posicionamento da caixa de opções, sendo as variações internas: 'fixed' ou 'absolute'
   * que foram implementadas para situações de uso em modais, por exemplo pois por default quando a caixa de opções abre pode gerar scroll.
   */
  dynamicAlign: PropTypes.bool,
  /**
   * Propriedade que, se for True, define que as opções do select não devem abrir para cima
   */
  noUp: PropTypes.bool,
  /**
   * Prop própria do formik, deve ser true quando o formulário estiver validando
   */
  isValidating: PropTypes.bool,
  /**
   * Essa propriedade define se o tooltip que mostra erro deve ter um botão para fechá-lo
   */
  hasHideTooltip: PropTypes.bool,
  /**
   * Essa propriedade que ativa o infinite scroll e define se há mais items para serem exibidos
   */
  hasMore: PropTypes.bool,
};

Select.defaultProps = {
  buttonIcon: "chicon-play",
  className: null,
  value: null,
  placeholder: "",
  allowOverflow: "inherit",
  handleBlur: () => {},
  handleFocus: () => {},
  onButtonClick: () => {},
  disabled: false,
  readOnly: false,
  id: null,
  name: null,
  touched: null,
  errors: {},
  noOptionString: "Nenhuma opção encontrada",
  maxHeight: null,
  optionsLabel: "label",
  keyPrefix: "",
  optionsKey: "key",
  handleFilter: null,
  asyncHandleFilter: null,
  handleInfiniteScroll: null,
  autocomplete: "off",
  dynamicAlign: false,
  noUp: false,
  hasMore: false,
};
