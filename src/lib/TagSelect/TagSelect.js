/* eslint react/no-array-index-key: 0 */

import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Tag from "../Tag/Tag";
import Select from "../Select/Select";

const TagSelectStyled = styled.div`
  position: relative;

  & .tag-row {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    flex-wrap: wrap;
    align-items: center;
    margin-left: -2px;
    margin-top: calc(0.5rem - 2px);
  }

  & .tag-row > div {
    margin: 8px 24px 0px 0px;
  }
`;

/**
 * Componente: TagSelect<br>
 * Select onde é possível escolher multiplas opções.<br>
 * Tais opções aparecem abaixo do componente na forma de tag e podem ser deletadas.
 * @component
 */
function TagSelect(props) {
  const {
    className,
    value,
    options,
    placeholder,
    handleChange,
    handleBlur,
    handleFocus,
    disabled,
    name,
    id,
    touched,
    noOptionString,
    maxHeight,
    optionsLabel,
    optionsKey,
    errors,
    autocomplete,
    isValidating,
    hasHideTooltip,
  } = props;

  function separateValues() {
    const filterSelected = [];
    const filterNotSelected = [];
    options.forEach((option) => {
      (value.some((v) => v === option[optionsKey])
        ? filterSelected
        : filterNotSelected
      ).push(option);
    });
    return { filterSelected, filterNotSelected };
  }

  const initialValues = separateValues();
  const [selected, setSelected] = useState(value);
  const [tags, setTags] = useState(initialValues.filterSelected);
  const [notSelected, setNotSelected] = useState(
    initialValues.filterNotSelected
  );
  const didMountRef = useRef(false);

  useEffect(() => {
    if (didMountRef.current && value !== selected) {
      const newValues = separateValues();
      setSelected(value);
      setTags(newValues.filterSelected);
      setNotSelected(newValues.filterNotSelected);
    }
  }, [value]);

  useEffect(() => {
    if (!disabled && didMountRef.current) {
      handleChange(selected);
    } else if (!didMountRef.current) {
      didMountRef.current = true;
    }
  }, [selected]);

  function addTag(newTag) {
    const tag = notSelected.find((option) => option[optionsKey] === newTag);
    if (!disabled && tag !== undefined) {
      setSelected([...selected, newTag]);
      setTags([...tags, tag]);
      setNotSelected(
        notSelected.filter((option) => option[optionsKey] !== newTag)
      );
    }
  }

  function removeTag(tag, index) {
    setTags(tags.filter((_, i) => i !== index));
    setSelected(selected.filter((v) => v !== tag[optionsKey]));
    setNotSelected([...notSelected, tag]);
  }

  return (
    <TagSelectStyled className={className}>
      <Select
        options={notSelected}
        placeholder={placeholder}
        handleChange={(v) => addTag(v)}
        handleBlur={handleBlur}
        handleFocus={handleFocus}
        disabled={disabled}
        isValidating={isValidating}
        name={name}
        id={id}
        touched={touched}
        noOptionString={noOptionString}
        maxHeight={maxHeight}
        optionsLabel={optionsLabel}
        optionsKey={optionsKey}
        errors={errors}
        autocomplete={autocomplete}
        hasHideTooltip={hasHideTooltip}
      />
      <div className="tag-row">
        {tags.map((tag, i) => (
          <Tag
            text={tag[optionsLabel]}
            disabled={disabled}
            key={tag[optionsKey]}
            onClick={() => removeTag(tag, i)}
          />
        ))}
      </div>
    </TagSelectStyled>
  );
}

TagSelect.propTypes = {
  /**
   * Define as classes CSS do tag select
   */
  className: PropTypes.string,
  /**
   * Define o valor que o tag select deve assumir contanto que esse valor seja uma key no array de options
   */
  value: PropTypes.arrayOf(
    PropTypes.shape({ label: PropTypes.string, key: PropTypes.string })
  ),
  /**
   * Este atributo é um array de objetos, que deve possuir as seguintes propriedades:<br /> <b>label: string|required</b><br /><b>key: string|required</b>.
   */
  options: PropTypes.arrayOf(
    PropTypes.shape({ label: PropTypes.string, key: PropTypes.string })
  ).isRequired,
  /**
   * Este atributo deve receber uma string para exibir no placeholder do componente.
   */
  placeholder: PropTypes.string,
  /**
   * A função é chamada sempre que um evento de clique (seleção da opção) acontece.
   */
  handleChange: PropTypes.func,
  /**
   * A função é chamada sempre que o tag select é desfocado.
   */
  handleBlur: PropTypes.func,
  /**
   * A função é chamada sempre que o tag select é focado.
   */
  handleFocus: PropTypes.func,
  /**
   * Propriedade que define se o tag select está desabilitado ou não
   */
  disabled: PropTypes.bool,
  /**
   * Identificador do tag select
   */
  id: PropTypes.string,
  /**
   * Identificador/nome do tag select
   */
  name: PropTypes.string,
  /**
   * Objeto com os IDs dos campos do formulário que já foram clicados pelo usuário, essa propriedade é obrigatória para o funcionamento do formik
   */
  touched: PropTypes.object,
  /**
   * Texto que será exibido caso a opção buscada não seja encontrada.
   */
  noOptionString: PropTypes.string,
  /**
   * Define a altura máxima de expansão da caixa de opções do tag select.
   */
  maxHeight: PropTypes.string,
  /**
   * Caso o objeto options não siga o padrão [{key, label}], settar o optionsLabel irá substituir o valor de label<br>
   * <i>Ex: [{key: "true", nome: "Sim"}, {key: "false", nome: "Não"}] funciona se optionsLabel="nome"</i>
   */
  optionsLabel: PropTypes.string,
  /**
   * Caso o objeto options não siga o padrão [{key, label}], settar o optionsKey irá substituir o valor de key<br>
   * <i>Ex: [{valor: "true", label: "Sim"}, {valor: "false", label: "Não"}] funciona se optionsKey="valor"</i>
   */
  optionsKey: PropTypes.string,
  /**
   * Objeto com os IDs dos campos do formulário setados como error true, essa propriedade é obrigatória para o funcionamento do formik
   */
  errors: PropTypes.object,
  /**
   * Representa o atributo HTML "autocomplete" do input utilizado no tag select, recebe "on" ou "off"
   */
  autocomplete: PropTypes.string,
  /**
   * Prop própria do formik, deve ser true quando o formulário estiver validando
   */
  isValidating: PropTypes.bool,
  /**
   * Essa propriedade define se o tooltip que mostra erro deve ter um botão para fechá-lo
   */
  hasHideTooltip: PropTypes.bool,
};

TagSelect.defaultProps = {
  className: null,
  value: [],
  placeholder: "",
  handleChange: () => [],
  handleBlur: () => [],
  handleFocus: () => [],
  disabled: false,
  id: null,
  name: null,
  touched: null,
  noOptionString: "Nenhuma opção encontrada",
  maxHeight: null,
  optionsLabel: "label",
  optionsKey: "key",
  errors: {},
  autocomplete: "off",
};

export default TagSelect;
