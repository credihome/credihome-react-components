import React from 'react';
import { ThemeProvider } from 'styled-components';

export default function WLTheme(props) {
  const {
    style, device, children,
  } = props;

  const theme = {
    colors: {
      primary: style.primaryBackgroundColorButton,
      secondary: style.borderBottomColorInput,
      error: {
        backgroundColor: style.validation.error.backgroundColor,
        color: style.validation.error.color,
      },
      info: {
        backgroundColor: style.validation.info.backgroundColor,
        color: style.validation.info.color,
      },
      warning: {
        backgroundColor: style.validation.warning.backgroundColor,
        color: style.validation.warning.color,
      },
    },
    input: {
      height: '35px',
      borderRadius: '5px',
      borderColor: '#707070',
      color: '#000',
      focusBorderBottom: '3px solid',
      padding: '0px 0px 0px 10px',
      margin: '0px 0px 10px 0px',
      backgroundColorDisabled: style.backgroundColorInputDisabled,
      fontColorDisabled: style.fontColorInputDisabled,
      borderColorFocus: style.primaryBackgroundColorButton,
      borderWeightFocus: '2px',
    },
    tooltip: {
      padding: '8px 10px',
      fontSize: '12px',
      fontWeight: '600',
      arrowPosition: `
        right: 0;
        left: 0;
        margin: auto;
      `,
      lineHeight: {
        mobile: "16px",
      }
    },
    datePicker: {
      calendarPadding: '10px 6px',
      weekFontSize: '13px',
      daysFontSize: '12px',
      headerFontSize: '14px',
      calendarSquare: '29px',
    },
    select: {
      optionsMargin: '0',
      optionsWidth: '100%',
      optionsColor: 'black',
      optionsBorder: '1px solid',
    },
    inputRange: {
      /*
      fontSizeAfterBefore: '14px',
      fontColorLabel: 'red',
      */
    },
    breakpoints: device,
  };

  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
}
