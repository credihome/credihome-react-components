import React from 'react';
import { ThemeProvider } from 'styled-components';

const size = {
  mobileM: '320px',
  tablet: '768px',
  laptop: '1024px',
  laptopL: '1440px',
};

export default function CRMTheme(props) {
  const { children } = props;

  const theme = {
    colors: {
      primary: '#E26226',
      secondary: '#0C173B',
      error: {
        backgroundColor: '#ea2d2d',
        color: '#fff',
      },
      warning: {
        backgroundColor: '#efa500',
        color: '#000',
      },
    },
    input: {
      height: '100%',
      borderRadius: '15px',
      borderColor: '#DDDDDD',
      color: '#777',
      padding: '3px 10px 2px 10px',
      backgroundColorDisabled: '#f3f3f3',
      fontWeight: 'normal',
      fontSize: '13px',
      fontFamily: 'Arial',
    },
    tooltip: {
      padding: '6px 12px',
      fontSize: '14px',
      fontWeight: 'normal',
      arrowPosition: `
        right: 15%;
      `,
    },
    inputRange: {
      fontSizeAfterBefore: '12px',
      fontSizeInput: '30px',
      fontSizeLabel: '14px',
      fontColorLabel: '#B5B5B5',
      backgroundInputRange: '#B5B5B5',
    },
    breakpoints: {
      mobileM: `(min-width: ${size.mobileM})`,
      tablet: `(min-width: ${size.tablet})`,
      laptop: `(min-width: ${size.laptop})`,
      laptopL: `(min-width: ${size.laptopL})`,
    },
    select: {
      optionsWidth: '100%',
      optionsMargin: '0 auto',
      minHeight: 'auto'
    }
  };

  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
}
