
# RoundBorderBox
RoundBorderBox é uma div estilizada para ter uma sombra, bordas redondas e fundo branco.

## Props
| Prop | Tipo | Default | Descrição |
|---|---|---|---|
| className | string | `null` | Classes a serem adicionadas no componente, separadas por `' '` (espaço) |
| children | node | `null` | HTML que ficará dentro do componente. Utilizar como se fosse uma tag normal, no exemplo: `<RoundBorderBox><p>Children</p></RoundBorderBox>`, `<p>Children</p>` vai ser o valor dessa prop |
| padding | string | `'1rem'` | Propriedade do CSS padding, para ser aplicado na caixa |
| position | string | `'static'` | Propriedade do CSS position, para ser aplicado na caixa |
