import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const RoundBorderBoxStyled = styled.div`
  border-radius: 5px;
  background-color: #fff;
  box-shadow: 0px 3px 6px #0000001A;
  padding: ${(props) => (props.padding ? props.padding : '1rem')};
  position: ${(props) => (props.position ? props.position : 'static')};
`;

export default function RoundBorderBox(props) {
  const {
    children, className, padding, position,
  } = props;

  return (
    <RoundBorderBoxStyled className={className} padding={padding} position={position}>
      {children}
    </RoundBorderBoxStyled>
  );
}

RoundBorderBox.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  padding: PropTypes.string,
  position: PropTypes.string,
};

RoundBorderBox.defaultProps = {
  className: null,
  padding: '1rem',
  position: 'static',
};
