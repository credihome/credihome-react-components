import React, { useState, useMemo, useCallback, memo } from 'react';
import PropTypes from 'prop-types';
import { CircleOptionsStyled } from './style'
import styled from 'styled-components';

const generateInitials = (value) => {
  const regExp = new RegExp(/(\w{0,1})[^\s]+ ?(\w{0,1})[^\s]+/);
  const regExp2 = new RegExp(/(\w{0,1})[^\s]/);

  if (value?.indexOf(' ') >= 1) {
    return regExp.exec(value).splice(1, 2).join('');
  }

  return regExp2.exec(value)[0];
}

const CircleOptions = (props) => {
  const {
    onAddAction, items, onDeleteAction, maxItems, deleteAll, confirmDelete, activeItem, itemLabel, onClickAction
  } = props;

  const _items = useMemo(() => items, [items]);
  const [localActiveItem, setLocalActiveItem] = useState(activeItem);
  const _generateInitials = useCallback((name) => generateInitials(name), []);

  const editOptions = (idx, confirm) => {
    const array = [..._items];
    array.splice(idx, 1);

    if (confirm) return confirmDelete({ index: idx, items: [...array] });
    return onDeleteAction({ index: idx, items: [...array] });
  }

  return (
    <CircleOptionsStyled maxItems={maxItems}>
      {_items?.length && _items.map((item, idx) => (
        <button key={`${item[itemLabel]}_${idx}`} className={localActiveItem === idx ? 'selected' : ''} onClick={() => setLocalActiveItem(idx) && onClickAction && onClickAction(idx)}>{_generateInitials(item[itemLabel])}
          {(deleteAll || idx > 0) && (<span onClick={() => confirmDelete ? editOptions(idx, true) : editOptions(idx)}><i className={'chicon chicon-close'}></i></span>)}
        </button>
      )) || <span>Nenhum item<br />para ser exibido.</span>
      }

      {
        _items?.length < maxItems && (
          <button id="CircleOptions" onClick={() => onAddAction(_items)}><i className={'chicon chicon-plus'}></i></button>
        )
      }
    </CircleOptionsStyled >
  );
}

export default memo(CircleOptions);

CircleOptions.propTypes = {
  onAddAction: PropTypes.func,
  items: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
  onDeleteAction: PropTypes.func,
  maxItems: PropTypes.number,
  deleteAll: PropTypes.bool,
  confirmDelete: PropTypes.func,
  activeItem: PropTypes.number,
  itemLabel: PropTypes.string,
  onClickAction: PropTypes.func,
}

CircleOptions.defaultProps = {
  onAddAction: () => alert('Envie uma função via a propriedade "onAddAction" para adicionar itens ao seu array (items).'),
  onDeleteAction: () => alert('Envie uma função via a propriedade "onDeleteAction" para excluir itens do seu array (items).'),
  maxItems: 4,
  deleteAll: false,
  confirmDelete: null,
  activeItem: null,
  itemLabel: 'label',
  onClickAction: null,
}
