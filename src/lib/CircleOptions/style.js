import styled from 'styled-components';

export const CircleOptionsStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: auto;
  padding: 5px;
  width: min-content;
  background-color: #ffcdb6;
  border-radius: 50px;
  position: relative;
  transition: 0.5s;
  &:hover {
    & button:not(:last-child) {
    margin-right: 13px;
    }
    & button:not(#CircleOptions) {
      outline: none;  
      & span {
      position: absolute;
      display: flex;
      right: -4px;
      border-radius: 37px;
      background-color: #E26226;
      width: 16px;
      height: 16px;
      bottom: 0px;
        &:hover {
        background-color: #b73d03;
        }
        .chicon-close {
        font-size: 11px;
        color: white;
        margin: 0 auto;
        margin-top: 3px;
        margin-left: 2.7px;
      }
    }
  }
}

  & > span {
    color: #000;
    white-space: nowrap;
    padding: 0 10px 0 10px;
    font-size: 13px;
    text-align: center;
    font-weight: bold;
  }

  & button {
    outline: none;
    cursor: pointer;
    width: 42px;
    height: 42px;
    border-radius: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #FFFFFF;
    text-transform: uppercase;
    font-size: 18px;
    position: relative;
    transition: 0.5s;
    box-shadow: 0px 0px 12px 0.001px #8888885e;
    background-color: #CDD5F5;
    color: #3B5BDB;
    border: none;
    &:hover {
      background-color: #afbef7;
    }
    span {
      display: none;
    }
    & button:not(:last-child) {
      transition: 0.5s;
    }
    &:not(:first-child) {
      margin-left: -7px;
    } 
  }

  ${props => {
    let text;
    for (let a = 0; a < props.maxItems; a++) {
      text = text + `
        & button:nth-child(${a}){
          z-index: ${props.maxItems - a};
        }
      `
    }

    return text;
  }}

  & button#CircleOptions {
    background-color: #0C173B;
    color: #ffffff;
    &:hover{
      background-color: #162D7B;
    }
    .chicon-plus {
      font-size: 27px;
      margin: 0 auto;
      line-height: 0;
    }
  }

  & button:not(#CircleOptions).selected {
    border: 3px solid #FFFFFF;
    box-sizing: border-box;
    font-weight: bold;
    transition: none;
    & span {
      /* bottom: -3.4px; */
      bottom: -3px;
      right: -7px;
      &:before, &:after {
      bottom: 2.7px;
      /* bottom: 2px; */
      }
    }     
  }  
`;
