import React, { useState, useMemo, memo } from 'react';

export default function ModalConfirmation(props) {
  const { onConfirm, onCancel } = props;

  return (
    <>
      <div style={{ backgroundColor: 'white', color: 'black', width: '300px', height: 'auto', padding: '45px 10px 45px 10px', position: 'absolute', left: '38%', fontSize: '18px', textAlign: 'center', borderRadius: '7px', zIndex: '100' }}>Deseja excluir este item?
          <br />
        <button type="button" onClick={() => { onConfirm() }} style={{ marginRight: '7px', borderRadius: '11px', width: '88px', backgroundColor: '#E26226', border: 'none', color: 'white', padding: '3px', marginTop: '2rem' }}>Sim</button>
        <button type="button" onClick={() => { onCancel() }} style={{ borderRadius: '11px', width: '88px', backgroundColor: '#E26226', border: 'none', color: 'white', padding: '3px', marginTop: '2rem' }}>Não</button>
      </div>
      <div style={{ position: 'absolute', width: '100%', height: '1000%', bottom: '-672%', left: '0', backgroundColor: 'black', opacity: '0.7', zIndex: '99' }}>
      </div>
    </>
  );

}