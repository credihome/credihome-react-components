import styled from 'styled-components';

export const Container = styled.div`

  .user {
    font-weight: bold;
  }

  .date {
    font-size: 14px;
    color: #707070;
  }

  .clipboard {
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #e26226;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    margin-left: -12px;
    img {
      width: 11px;
    }
  }

  .event-description {
    margin: 0px;
    margin-left: 10px;
    margin-bottom: 1rem;
  }
`;

export const Event = styled.div`
  display: flex;
  
  border-left: ${props => (props.last ? 'none' : 'solid 1px black')};
`;
