import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import { Container, Event } from './styles';
import ClipBoard from './icons/clipboard.svg';


/**
  * Componente: Timeline
  * @component
  */

function Timeline({ events, text, user, date, format }) {
  const [formatedEvents, setFormatedEvents] = useState([]);

  useEffect(() => {
    const getLast = (event, i) => {
      if (i === events.length - 1) {
        return { ...event, last: true };
      }
      return { ...event, last: false };
    };
    const compare = (a, b) => a[date] < b[date];
    const newEvents = events.map((event, i) => getLast(event, i));
    setFormatedEvents(newEvents.sort(compare));
  }, [events]);

  return (
    <Container>
      <div className="timeline">
        {formatedEvents.map(event => (
          <Event last={event.last}>
            <div className="clipboard">
              <img src={ClipBoard} alt="" />
            </div>
            <p className="event-description">
              {user && <span className="user">{`${event[user]} - `}</span>}
              {event[text]} <br />
              {date && (
                <span className="date">
                  {dayjs(event[date]).format(format)}
                </span>
              )}
            </p>
          </Event>
        ))}
      </div>
    </Container>
  );
}

Timeline.propTypes = {
  /**
   * Este atributo deve receber um array de objetos contendo os eventos a serem exibidos.
   */
  events: PropTypes.array.isRequired,
  /**
   * Este atributo deve ser o atributo do objeto que representa um evento.
   */
  text: PropTypes.string.isRequired,
  /**
   * Este atributo deve ser o atributo do objeto que representa o responsável pelo evento.
  */
  user: PropTypes.string,
  /**
   * Este atributo deve ser o atributo do objeto que representa a data do evento.
  */
  date: PropTypes.string,
  /**
   * Este atributo deve ser o formato de saída da data do evento.
  */
  format: PropTypes.string,
};

Timeline.defaultProps = {
  user: '',
  date: '',
  format: 'DD/MM/YYYY - HH:mm:ss',
};

export default Timeline;
