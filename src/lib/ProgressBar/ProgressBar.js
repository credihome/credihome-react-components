import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ProgressBarStyled = styled.div`
  position: relative;
  height: 14px;
  box-sizing: border-box;

  &::before {
    content: '';
    position: absolute;
    border-radius: 5px;
    border: 1px solid;
    border-color: ${(props) => (props.primaryColor ? `${props.primaryColor}` : '#E26226')};
    box-sizing: border-box;
    width: 100%;
    height: 100%;
    box-sizing: border-box;
  }

  & div {
    border-radius: 5px;
    position: absolute;
    width: ${(props) => (props.percentage ? `${props.percentage}%` : '0%')};
    height: 100%;
    background-color: ${(props) => (props.primaryColor ? `${props.primaryColor}` : '#E26226')};
    transition: width;
    transition-duration: ${(props) => (props.duration ? `${props.duration}ms` : '1000ms')};
  }

  & span {
    position: absolute;
    height: fit-content;
    z-index: 1;
    color: ${(props) => (props.secondaryColor ? `${props.secondaryColor}` : '#fff')};
    font-size: 12px;
    text-align: center;
    font-weight: bold;
    top: 50%;
    left: ${(props) => (props.percentage ? `${props.percentage}%` : '0%')};
    transform: translate(-50%, -50%);
    transition: left;
    transition-duration: ${(props) => (props.duration ? `${props.duration}ms` : '1000ms')};
  }

  & span::before {
    content: '';
    position: absolute;
    width: 34px;
    height: 34px;
    background-color: ${(props) => (props.primaryColor ? `${props.primaryColor}` : '#E26226')};
    z-index: -1;
    border: 1px solid;
    border-color: ${(props) => (props.secondaryColor ? `${props.secondaryColor}` : '#fff')};
    border-radius: 50%;
    transform: translate(-50%, -50%);
    top: 50%;
    left: 50%;
  }
`;

/**
  * Componente: ProgressBar<br/>
  * Componente que apresenta uma porcentagem, escolhida pelo usuário, na forma de uma barra parcialmente preenchida.
  * @component
  */
function ProgressBar(props) {
  const {
    className, percentage, primaryColor, secondaryColor, duration,
  } = props;

  const [displayedPercentage, setDisplayedPercentage] = useState(percentage);

  function animateValue(start, end, d) {
    const range = end - start;
    let current = start;
    const increment = end > start ? 1 : -1;
    const stepTime = Math.abs(Math.floor(d / range));
    const timer = setInterval(() => {
      current += increment;
      setDisplayedPercentage(current);
      if (current === end) {
        clearInterval(timer);
      }
    }, stepTime);
  }

  useEffect(() => {
    if (displayedPercentage !== percentage) animateValue(displayedPercentage, percentage, duration / 2);
  }, [percentage]);

  return (
    <ProgressBarStyled className={className} primaryColor={primaryColor} secondaryColor={secondaryColor} percentage={percentage} duration={duration}>
      <div />
      <span>{`${displayedPercentage || 0}%`}</span>
    </ProgressBarStyled>
  );
}

ProgressBar.propTypes = {
  /**
   * Define as classes CSS da progress bar.
   */
  className: PropTypes.string,
  /**
   * Define em qual posição da barra de progresso estará a indicação da porcentagem,
   * além de ser o valor do span que a destaca.
   */
  percentage: PropTypes.number.isRequired,
  /**
   * Define o cor do background do trecho preenchido da barra de progresso e do span que destaca a porcentagem.
   */
  primaryColor: PropTypes.string,
  /**
   * Define a cor da borda e do texto do span que destaca a porcentagem.
   */
  secondaryColor: PropTypes.string,
  /**
   * Duração, em ms, da animação de transição que ocorre quando a porcentagem muda.
   */
  duration: PropTypes.number,
};

ProgressBar.defaultProps = {
  className: null,
  primaryColor: '#E26226',
  secondaryColor: '#fff',
  duration: 1000,
};

export default ProgressBar;