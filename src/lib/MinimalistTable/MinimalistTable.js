/* eslint react/no-array-index-key: 0 */

import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const MinimalistTableStyled = styled.table`
  width: 100%;
  border-spacing: 0px;
  border-collapse: collapse;

  & thead tr th {
    text-align: center;
    border: 0px;
    padding: 14px 2px;
    font-weight: ${(props) => (props.boldHeadTitle ? "bold" : "inherit")};
    font-size: ${(props) =>
      (props.fontSizeHead && props.fontSizeHead) || "12px"};
    color: ${(props) =>
      (props.fontColorHead && props.fontColorHead) || "#e26126"};
    &:first-child {
      text-align: start;
    }
  }

  & tbody:before {
    height: 3px;
    display: table-row;
    content: "";
  }

  & tbody tr {
    border-bottom: 1px solid
      ${(props) =>
        (props.dividerColor && props.dividerColor) || "#0000001a"};
  }

  & tbody tr:last-of-type {
    border-bottom: none;
  }

  & tbody tr td {
    padding: 14px 2px;
    text-align: center;
    color: ${(props) =>
      (props.fontColorCell && props.fontColorCell) || "#444444"};
    font-size: ${(props) =>
      (props.fontSizeCell && props.fontSizeCell) || "14px"};
    &:first-child {
      text-align: start;
    }
  }
`;

/**
  * Component: MinimalistTable
  * @component
  */
function MinimalistTable(props) {
  const {
    className,
    children,
    boldHeadTitle,
    fontColorHead,
    fontSizeHead,
    fontSizeCell,
    fontColorCell,
    dividerColor,
  } = props;

  return (
    <MinimalistTableStyled
      className={className}
      boldHeadTitle={boldHeadTitle}
      fontSizeHead={fontSizeHead}
      fontSizeCell={fontSizeCell}
      dividerColor={dividerColor}
      fontColorHead={fontColorHead}
      fontColorCell={fontColorCell}
    >     
      {children}
    </MinimalistTableStyled>
  );
}

MinimalistTable.propTypes = {
  /**
   * Define as classes CSS da MinimalistTable
   */
  className: PropTypes.string,
  /**
   * Deve receber um elemento React ou função (que devolva um DOM/React), essa propriedade define o que será renderizado dentro deste componente.
   */
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  /**
   * Define se os títulos do cabeçalho será exibido no estilo 'bold' ou não
   */
  boldHeadTitle: PropTypes.bool,
  /**
   * Passa uma nova cor para os títulos do cabeçalho
   */
  fontColorHead: PropTypes.string,
   /**
   * Passa um novo tamanho de fonte aos títulos do cabeçalho
   */
  fontSizeHead: PropTypes.string,
   /**
   * Passa um novo tamanho de fonte as células do corpo da tabela
   */
  fontSizeCell: PropTypes.string,
   /**
   * Passa uma nova cor de fonte as células do corpo da tabela
   */
  fontColorCell: PropTypes.string,
   /**
   * Altera a cor da divisória entre as linhas da tabela
   */
  dividerColor: PropTypes.string,
};

MinimalistTable.defaultProps = { 
  className: null,
  boldHeadTitle: false,
  fontColorHead: '#e26126',
  fontSizeHead: '12px',
  fontSizeCell: '14px',
  fontColorCell: '#444444',
  dividerColor: '#0000001a',
};

export default MinimalistTable;