import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import FilteringHeader from './FilteringHeader';

Enzyme.configure({ adapter: new EnzymeAdapter() });


const setup = (defaultProps) => {
  const {
    text = 'Test',
    onClickUp = () => { },
    onClickDown = () => { },
    onReset = () => { },
    children = null,
    activeFilter = null,
    className = ''
  } = defaultProps;

  const props = {
    text,
    onClickUp,
    onClickDown,
    onReset,
    children,
    activeFilter,
    className
  };

  return {
    props,
    wrapper: shallow(
      <FilteringHeader {...props}>
        {children}
      </FilteringHeader>
    ),
  }
}

describe('Funcionalidades básicas', () => {

  it('O texto passado deve ser o mesmo renderizado', () => {
    const { wrapper } = setup({ text: 'Testando' });
    const element = wrapper.find('div').at(0);

    expect(element.text()).toEqual('Testando');
  });

  it('Função deve ser chamada apenas uma vez quando o botão é clicado', () => {
    const clickDown = jest.fn();
    const { wrapper } = setup({ onClickDown: clickDown });
    wrapper.find('.chicon-arrow-bottom').simulate('click');

    expect(clickDown).toHaveBeenCalledTimes(1);
  });

  it('A classe non-active deve ser atribuida na arrow Bottom quando arrow Up é clicado', () => {
    const { wrapper } = setup({});
    wrapper.find('.chicon-arrow-top').simulate('click');
    const toTest = wrapper.find('.chicon-arrow-bottom');

    expect(toTest.hasClass('non-active')).toBeTruthy();
  });

  it('Fidelidade do filho renderizado', () => {
    const { wrapper } = setup({ children: <p>olá</p> });
    const array = wrapper.find('p').getElements();

    expect(array[0].type).toBe('p');
  })

  it('Deve chamar a função onReset 1 vez quando o mesmo botão é clicado duas vezes', () => {
    const reset = jest.fn();
    const { wrapper } = setup({ onReset: reset });
    wrapper.find('.chicon-arrow-bottom').simulate('click');
    wrapper.find('.chicon-arrow-bottom').simulate('click');

    expect(reset).toHaveBeenCalledTimes(1);
  })

  it('Deve adicionar os tributos da propriedade _className_ quando for passado', () => {
    const { wrapper } = setup({ className: 'teste' });
    expect(wrapper.hasClass('teste')).toBeTruthy();
  })

  it('Define o filtro passado como inicial', () => {
    const useStateSpy = jest.spyOn(React, 'useState');
    const { wrapper } = setup({ activeFilter: 'up' });

    expect(useStateSpy).toHaveBeenCalledWith('up');
  })

  it('Deve iniciar o filtro com both como valor inicial', () => {
    const useStateSpy = jest.spyOn(React, 'useState');
    const wrapper = setup({});

    expect(useStateSpy).toHaveBeenCalledWith('both');
  })
});