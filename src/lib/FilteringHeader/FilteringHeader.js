/* eslint react/no-array-index-key: 0 */

import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const FilteringHeaderStyle = styled.th`

  & > div {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    height: fit-content;
    width: fit-content;
  }

  & > div > div {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    margin-left: 0.5rem;
  }

  & i {
    width: 1em;
    height: 1em;
    font-size: smaller;
    position: relative;
    cursor: pointer;
    opacity: 1;
  }

  & i.non-active {
    opacity: 0.3;
  }

  & i.non-active:hover {
    opacity: 1;
  }

  & i:before {
    top: 0;
    left: 0;
    bottom: 0;
    position: absolute;
  }
`;

export default function FilteringHeader(props) {
  const {
    children, className, onClickUp, onClickDown, onReset, text, activeFilter,
  } = props;

  const [active, setActive] = React.useState(activeFilter || 'both');
  const didMountRef = useRef(false);

  function activate(kind) {
    if (active === kind) {
      setActive('both');
      onReset();
    } else {
      setActive(kind);
      if (kind === 'up') {
        onClickUp();
      } else {
        onClickDown();
      }
    }
  }

  useEffect(() => {
    if (didMountRef.current) {
      activate(activeFilter);
    } else if (!didMountRef.current) {
      didMountRef.current = true;
    }
  }, [activeFilter]);

  return (
    <FilteringHeaderStyle className={className}>
      <div>
        {children || text}
        <div>
          <i className={`chicon-arrow-top ${(active === 'down') && 'non-active'}`} onClick={() => { activate('up'); }} />
          <i className={`chicon-arrow-bottom ${(active === 'up') && 'non-active'}`} onClick={() => { activate('down'); }} />
        </div>
      </div>
    </FilteringHeaderStyle>
  );
}

FilteringHeader.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  onClickUp: PropTypes.func.isRequired,
  onClickDown: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  text: PropTypes.string,
  activeFilter: PropTypes.string,
};

FilteringHeader.defaultProps = {
  className: null,
  children: null,
  text: null,
  activeFilter: null,
};
