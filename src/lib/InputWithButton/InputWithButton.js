import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import MaskedInput from "react-text-mask";
import Tooltip from "../Tooltip/Tooltip";

/**
 * Component InputWithButton
 * @component
 */

const InputWithButton = styled.div`
  position: relative;
  min-width: fit-content;
  z-index: 1;

  & > button > i {
    width: 1rem;
    height: 1rem;
    position: absolute;
    top: calc(50% - 0.4rem);
    left: 0;
    color: ${(props) => props.iconColour};
    transform: ${(props) =>
      props.iconRotate ? `rotate(${props.iconRotate})` : ""};
    font-size: 0.8rem;
  }

  & button {
    width: 1rem;
    height: 1rem;
    position: absolute;
    right: 10px;
    top: calc(50% - 0.5rem);
    cursor: ${(props) => (props.disabled ? "default" : "pointer")};
    background-color: transparent;
    border: none;
    display: ${(props) => (props.disabled ? "none" : "default")};
  }

  & button:focus {
    outline: none;
    box-shadow: none;
  }

  & input {
    padding: ${(props) => props.theme?.input?.padding || "4px 10px"};
    padding-right: calc(1rem + 10px);
    border: 1px solid;
    border-color: ${(props) =>
      props.error
        ? props.theme?.colors?.error?.backgroundColor || "#ea2d2d"
        : props.theme?.input?.borderColor || "#DDDDDD"};
    border-radius: ${(props) => props.theme?.input?.borderRadius || "15px"};
    margin: 0;
    width: 100%;
    height: ${(props) => props.theme?.input?.height || "100%"};
    color: ${(props) => props.theme?.input?.color || "#777"};
    box-sizing: border-box;
    cursor: ${(props) => (props.readOnly ? "pointer" : "auto")};
    transition: ${(props) =>
      props.theme?.input?.focusBorderBottom ? "0.3s border" : "none"};
    font: ${(props) =>
      `${props.theme?.input?.fontWeight || "normal"} ${
        props.theme?.input?.fontSize || "inherit"
      } ${props.theme?.input?.fontFamily || "inherit"}`};
  }

  & input::placeholder {
    color: ${(props) => props.theme?.input?.placeholder || "#777"};
  }

  & input:disabled {
    background-color: ${(props) =>
      props.theme?.input?.backgroundColorDisabled ||
      "rgba(255, 255, 255, 0.5)"};
    color: ${(props) => props.theme?.input?.fontColorDisabled || "#545454"};
    cursor: default;
    & ~ i {
      display: none;
    }
  }

  & input:focus {
    outline: none;
    border: ${(props) => props.theme?.input?.borderWeightFocus || "1px"} solid
      ${(props) => props.theme?.input?.borderColorFocus || "red"};
  }
`;

export default function InputWithButtonComponent(props) {
  // TODO exibição de errors -> touched
  const {
    type,
    disabled,
    placeholder,
    handleChange,
    handleBlur,
    handleFocus,
    value,
    id,
    touched,
    errors,
    onButtonClick,
    buttonIcon,
    className,
    extraAttributes,
    iconColour,
    readOnly,
    iconRotate,
    onInputClick,
    mask,
    autocomplete,
    isValidating,
    hasHideTooltip,
  } = props;

  const [hideTooltip, setHideTooltip] = useState(false);

  useEffect(() => {
    if (hasHideTooltip) setHideTooltip(false);
  }, [isValidating, errors[id]]);

  return (
    <>
      {touched && errors && touched[id] && errors[id] && (
        <>
          <Tooltip
            message={errors[id]}
            hideTooltip={hideTooltip}
            setHideTooltip={setHideTooltip}
            hasHideTooltip={hasHideTooltip}
          />
        </>
      )}
      <InputWithButton
        className={className}
        disabled={disabled}
        iconColour={iconColour}
        readOnly={readOnly}
        iconRotate={iconRotate}
        error={touched && errors && touched[id] && errors[id]}
      >
        {mask ? (
          <MaskedInput
            error={errors}
            touched={touched}
            type={type}
            placeholder={placeholder}
            value={value}
            onChange={(e) => {
              handleChange(e);
            }}
            onBlur={handleBlur}
            onFocus={() => {
              handleFocus();
            }}
            disabled={disabled}
            readOnly={readOnly}
            id={id}
            onClick={onInputClick}
            mask={mask}
            autocomplete={autocomplete}
            {...extraAttributes}
          />
        ) : (
          <input
            type={type}
            placeholder={placeholder}
            value={value}
            onChange={handleChange}
            onBlur={handleBlur}
            onFocus={() => {
              handleFocus();
            }}
            disabled={disabled}
            readOnly={readOnly}
            id={id}
            onClick={onInputClick}
            autoComplete={autocomplete}
            {...extraAttributes}
          />
        )}
        <button type="button" onClick={!disabled ? onButtonClick : () => {}}>
          <i className={buttonIcon} />
        </button>
      </InputWithButton>
    </>
  );
}

InputWithButtonComponent.propTypes = {
  /**
   * Define as classes CSS do componente.
   */
  className: PropTypes.string,
  /**
   * Define tipo do input (conforme as propriedades do HTML).
   */
  type: PropTypes.string,
  /**
   * Propriedade que define se o componente está desabilitado ou não.
   */
  disabled: PropTypes.bool,
  /**
   * Essa propriedade deve receber uma string que será apresentada no input.
   */
  placeholder: PropTypes.string,
  /**
   * Deve receber o valor que o usuário digitou e guardará o valor dentro do objeto "value" [vide Formik].
   */
  handleChange: PropTypes.func,
  /**
   * Essa propriedade é executada quando o usuário clica fora do input.
   */
  handleBlur: PropTypes.func,
  /**
   * A função é chamada sempre que o componente é focado.
   */
  handleFocus: PropTypes.func,
  /**
   * Recebe o valor que deve ser guardado.
   */
  value: PropTypes.string,
  /**
   * Identificador do input.
   */
  id: PropTypes.string,
  /**
   * Objeto com os IDs dos campos do formulário que já foram clicados pelo usuário, essa propriedade é obrigatória para o funcionamento do formik.
   */
  touched: PropTypes.object,
  /**
   * A propriedade recebe um objeto que contém os erros relacionados ao seu componente.
   */
  errors: PropTypes.object,
  /**
   * Altera a cor do icone do botão.
   */
  iconColour: PropTypes.string,
  /**
   * Função disparada ao clicar no componente.
   */
  onButtonClick: PropTypes.func,
  /**
   * Objetos de propriedades válidas para o input do HTML.
   */
  extraAttributes: PropTypes.object,
  /**
   * Este atributo deve receber o nome do ícone que será exibido no componente.
   */
  buttonIcon: PropTypes.string.isRequired,
  /**
   *  Propriedade (boleano) que restringe a digitação dentro do componente.
   */
  readOnly: PropTypes.bool,
  /**
   *  Propriedade (boleano) que define se ícone irá rotacionar ou não.
   */
  iconRotate: PropTypes.string,
  /**
   * Recebe uma função que será disparada quando o campo for clicado.
   */
  onInputClick: PropTypes.func,
  /**
   * Propriedade para exibir a máscara no padrão regex.
   */
  mask: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  /**
   * Representa o atributo HTML "autocomplete" do input (recebe "on" ou "off").
   */
  autocomplete: PropTypes.string,
  /**
   * Prop própria do formik, deve ser true quando o formulário estiver validando
   */
  isValidating: PropTypes.bool,
  /**
   * Essa propriedade define se o tooltip que mostra erro deve ter um botão para fechá-lo
   */
  hasHideTooltip: PropTypes.bool,
};

InputWithButtonComponent.defaultProps = {
  className: null,
  onButtonClick: () => {},
  handleChange: () => {},
  handleBlur: () => {},
  handleFocus: () => {},
  value: "",
  iconColour: "#aaa",
  disabled: false,
  type: "text",
  placeholder: null,
  id: null,
  touched: null,
  errors: {},
  extraAttributes: null,
  readOnly: false,
  iconRotate: null,
  onInputClick: () => {},
  mask: null,
  autocomplete: "on",
};
