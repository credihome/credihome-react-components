import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import InputWithButton from './InputWithButton';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('InputWithButton - testando funcionalidades avançadas', () => {

  it('verficando se existe um ícone de exibição', () => {
    const wrapper = shallow(<InputWithButton buttonIcon="chicon-arrow-calendar" />);
    expect(wrapper.find('i.chicon-arrow-calendar')).toHaveLength(1);
  })

  it('verficando se existe um imput', () => {
    const wrapper = shallow(<InputWithButton buttonIcon="chicon-arrow-calendar" />);
    // console.log(wrapper.html())
    expect(wrapper.find('input')).toHaveLength(1);
  })

  it('verificando o placeholder do input', () => {
    const wrapper = shallow(<InputWithButton buttonIcon="chicon-arrow-calendar" placeholder="batata" />);
    expect(wrapper.find('input[placeholder="batata"]')).toHaveLength(1);
  });

  it('verficando se função de onClick funciona', () => {
    const onActionButton = jest.fn();
    const button = shallow(<InputWithButton onButtonClick={onActionButton} buttonIcon="chicon-arrow-calendar" />);
    button.find('button').simulate('click');
    // button.find('button').simulate('click');
    // expect(onActionButton.mock.calls.length).toEqual(1);
    expect(onActionButton).toHaveBeenCalledTimes(1);
  })
})