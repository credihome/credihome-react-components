/* eslint react/no-array-index-key: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ColoredCircleStyled = styled.div`
  width: ${(props) => (props.size ? props.size : '1rem')};;
  background-color: ${(props) => (props.color ? props.color : '#DDDDDD')};
  border-radius: 50%;
  &:after {
    content: "";
    display: block;
    padding-bottom: 100%;
  }
`;

export default function ColoredCircle(props) {
  const {
    className, color, size,
  } = props;

  return (
    <ColoredCircleStyled className={className} color={color} size={size} />
  );
}

ColoredCircle.propTypes = {
  className: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.string,
};

ColoredCircle.defaultProps = {
  className: null,
  color: '#DDDDDD',
  size: '1rem',
};
