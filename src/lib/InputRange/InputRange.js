import React, {
  memo, useRef, useEffect, useState,
} from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-text-mask';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import styled from 'styled-components';
import Tooltip from '../Tooltip/Tooltip';

const DivMultipleRange = styled.div`
height: ${(props) => props.height || '8px'};
&::after {
  background: ${(props) => props.theme?.colors?.primary || '#000'};
  content: '';
  height: ${(props) => props.height || '8px'};
  width: ${(props) => props.sizeBar}px;
  margin-left: ${(props) => props.marginLeftBar}px;
  position: absolute;
  }
  background-color: ${(props) => props.theme?.inputRange?.backgroundInputRange || '#afafaf'};
  border-radius: 5px;
  position: relative;
  text-align: center;
  & input {
  position: absolute;
  background-color: transparent;
  z-index: 1;
  }
  & > output {
    color: #AFAFAF;
    font-size: 13px;
    position: absolute;
    top: ${(props) => `${(props.sizeBalls.replace(/\D+/g, '') / 2) + 5}px`};
    display: flex;
    flex-direction: ${(props) => props.sizeBar <= 80 && props.sizeBar !== 0 && 'column'};
    transform: translateX(-50%);
    align-items: center;
    & > span {
      height: 14px;
      width: max-content;
      &:first-child {
        margin-right: 2.5px;
      }
    }
  }
  @-moz-document url-prefix() {
    output {
      top: ${(props) => `${(props.sizeBalls.replace(/\D+/g, '') / 2) + 5}px`};
    }
  }
  & .outputOne {
    margin-left: ${(props) => props.marginLeftBar}px;
  }
  & .outputTwo {
    margin-left: calc(${(props) => props.marginLeftBar}px + ${(props) => props.sizeBar}px);
  }
`;

const DivRangeStyled = styled.div`
  width: 100%;
  margin: 0 0 7px 0;
  & > div {
    display: flex;
    justify-content: space-between;
    align-items: center;
    & label {
      font-family: 'Open Sans', 'Arial', sans-serif;
      color: ${(props) => props.theme?.inputRange?.fontColorLabel || '#000'};
      font-size: ${(props) => props.theme?.inputRange?.fontSizeLabel || '14px'};
      margin: 0 5px 5px 0;
      font-weight: bold;
      @media (max-width: 375px) {
        width: calc(100% - 171px);
      }
    }
    & > div {
      position: relative;
      display: flex;
      flex-wrap: nowrap;
      align-items: flex-end;
      transition: all 0.1s;
      max-width: 200px;
      color: ${(props) => props.theme?.colors?.primary || '#000'};
      &::before,
      &::after {
        font-size: ${(props) => props.theme?.inputRange?.fontSizeAfterBefore || '12px'};
        margin: 0 0 5px 0;
      }
      ${(props) => {
    if (props.format === 'currency') {
      return `
        &::before {
          font-family: 'Open Sans', 'Arial', sans-serif;
          content: "${props.schedule[props.format].prefix}";
        }`;
    } return `
      &::after {
        font-family: 'Open Sans', 'Arial', sans-serif;
        content: "${props.schedule[props.format]?.prefix || undefined}";
      }`;
  }}
      & > div {
        & div {
          width: max-content!important;
        }
        position: absolute;
        top: 10px;
        background-color: red;
        right: 0;
      }
      & input {
        font-family: 'Open Sans', 'Arial', sans-serif;
        background-color: inherit;
        border: none;
        font-size: ${(props) => props.theme?.inputRange?.fontSizeInput || '30px'};
        width: calc(100% - ${(props) => (props.format === 'currency' ? '15px' : '40px')});
        font-weight: bold;
        text-align: right;
        color: ${(props) => props.theme?.colors?.primary || '#000'};
        padding: 0;
    }
  }
}
`;

const InputRangeStyled = styled.input`
  pointer-events: none;
  outline: none;
  position: relative;
  width: 100%;
  appearance: none;
  background-color: ${(props) => !props.multiple && (props.theme?.inputRange?.backgroundInputRange || '#B5B5B5')};
  height: ${(props) => props.height || '8px'};
  border-radius: 10px;
  margin: 0;
  &:focus {
    outline: none;
  }
  &::-ms-track {
    outline: none;
    position: relative;
    width: 100%;
    appearance: none;
    background-color: ${(props) => !props.multiple && (props.theme?.inputRange?.backgroundInputRange || '#B5B5B5')};
    height: 0px;
    border-radius: 10px;
  }
  &::-moz-range-track {
    outline: none;
    position: relative;
    width: 100%;
    appearance: none;
    background-color: ${(props) => !props.multiple && (props.theme?.inputRange?.backgroundInputRange || '#B5B5B5')};
    height: 0px;
    border-radius: 10px;
  }
  &::-webkit-slider-runnable-track {
    background: none;
  }
  &::-webkit-slider-thumb {
    appearance: none;
    border: none;
    cursor: pointer;
    width: ${(props) => props.sizeBalls};
    height: ${(props) => props.sizeBalls};
    border-radius: 100%;
    pointer-events: auto;
    background-color: ${(props) => props.theme?.colors?.primary || '#000'};
    }
  &::-moz-range-thumb {
    appearance: none;
    border: none;
    cursor: pointer;
    width: ${(props) => props.sizeBalls};
    height: ${(props) => props.sizeBalls};
    border-radius: 100%;
    pointer-events: auto;
    background-color: ${(props) => props.theme?.colors?.primary || '#000'};
  }
  &::-ms-thumb {
    appearance: none;
    border: none;
    cursor: pointer;
    width: ${(props) => props.sizeBalls};
    height: ${(props) => props.sizeBalls};
    border-radius: 100%;
    pointer-events: auto;
    background-color: ${(props) => props.theme?.colors?.primary || '#000'};
  }
  &:active, &:hover {
    &::-webkit-slider-thumb {
      -webkit-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);
      -moz-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);
      box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);
    }
    &::-moz-range-thumb {
      -webkit-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);
      -moz-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);
      box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);
    }
    &::-ms-thumb {
      -webkit-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);
      -moz-box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);
      box-shadow: 0px 0px 0px 4px rgba(255,140,0, 0.5);
    }
  }
`;

const numberMask = () => createNumberMask({
  prefix: '',
  thousandsSeparatorSymbol: '.',
  requireDecimal: false,
  allowDecimal: false,
  suffix: '',
});

function InputRange({
  label, placeholder, name, value, error, setFieldValue, min, max, step, format, notMask, multiple, labels, sizeBalls, height, handleChange,
}) {
  const [monted, setMonted] = useState(false);
  const [errorInput, setErrorInput] = useState(false);

  const [arrItems, setArrItems] = useState(value);
  const [sizeBar, setSizeBar] = useState(0);
  const [marginLeftBar, setMarginLeftBar] = useState(0);

  useEffect(() => setMonted(true), []);

  const a = useRef(null);
  const b = useRef(null);
  const divMultipleRange = useRef(undefined);
  const divRangeWidth = divMultipleRange.current?.clientWidth;

  const schedule = {
    currency: {
      prefix: 'R$',
      rem: '19',
    },
    monthly: {
      prefix: 'meses',
      rem: '25',
    },
  };

  function reCalcWidth(v) {
    if (b.current) {
      b.current.style.width = `${(v?.toString().replace(/\D+/g, '').length + 1) * schedule[format]?.rem}px`;
    }
  }

  const calculateSize = () => {
    // Calculos para preencher com cor o espaço entre os ranges
    let isSmallerValue = Math.min(arrItems[0], arrItems[1], max);
    let isBiggerValue = Math.max(arrItems[0], arrItems[1], min);
    const newSizeBalls = sizeBalls.replace(/\D+/g, '');

    if (isSmallerValue < min) {
      isSmallerValue = min;
    }
    if (isBiggerValue > max) {
      isBiggerValue = max;
    }

    setSizeBar(((isBiggerValue - isSmallerValue) / (max - min)) * (divRangeWidth - newSizeBalls));
    setMarginLeftBar((newSizeBalls / 2) + (((isSmallerValue - min) / (max - min)) * (divRangeWidth - newSizeBalls)));
  };


  useEffect(() => {
    if (monted) reCalcWidth(a.current?.props?.value);
    calculateSize();

    return () => {};
  });

  function testValue(
    key, val, comp, type,
  ) {
    const types = {
      biggerThan: (v, c) => v >= c && (key !== 8 && key !== 46 && key !== 37 && key !== 39),
      lessThan: (v, c) => v < c && (key !== 8 && key !== 46 && key !== 37 && key !== 39),
    };

    return types[type](val, comp);
  }

  useEffect(() => {
    setArrItems(value);
  }, [value]);

  useEffect(() => {
    calculateSize();
  }, [arrItems]);

  function toDefineLabelName(v) {
    if (v === 1) { return labels.singular ? labels.singular : undefined; }
    if ((v > 1 && v < max) || v === 0) { return labels.plural ? labels.plural : undefined; }
    if (v === max) { return labels.last ? labels.last : labels.plural; }
    return v;
  }

  return (
    <DivRangeStyled schedule={schedule} format={format}>
      {multiple ? (
        <>
          {(error[name]) && (
          <Tooltip message={error[name]} />
          )}

          <DivMultipleRange
            multiple
            sizeBar={sizeBar}
            sizeBalls={sizeBalls}
            marginLeftBar={marginLeftBar > 0 ? marginLeftBar : (marginLeftBar * -1)}
            ref={divMultipleRange}
            height={height}
          >
            <InputRangeStyled
              sizeBalls={sizeBalls}
              name={name}
              type="range"
              onChange={(e) => { setArrItems([parseInt(e.target.value, 10), arrItems[1]]); handleChange([parseInt(e.target.value, 10), value[1]]); }}
              value={arrItems[0]}
              min={min}
              max={max}
              step={step}
              height={height}
            />
            <output className="outputOne">
              <span>{arrItems[0] > arrItems[1] ? arrItems[1] : arrItems[0]}</span>
              <span>
                {labels && toDefineLabelName(arrItems[0] > arrItems[1] ? arrItems[1] : arrItems[0])}
              </span>
            </output>
            <InputRangeStyled
              sizeBalls={sizeBalls}
              name={name}
              type="range"
              onChange={(e) => { setArrItems([arrItems[0], parseInt(e.target.value, 10)]); handleChange([value[0], parseInt(e.target.value, 10)]); }}
              value={arrItems[1]}
              min={min}
              max={max}
              step={step}
            />
            <output className="outputTwo">
              <span>{arrItems[1] > arrItems[0] ? arrItems[1] : arrItems[0]}</span>
              <span>
                {labels && toDefineLabelName(arrItems[1] > arrItems[0] ? arrItems[1] : arrItems[0])}
              </span>
            </output>
          </DivMultipleRange>
        </>
      )
        : (
          <>
            <div>
              <label htmlFor={`${name}_txt`}>
                {label}
              </label>

              <div ref={b}>
                {(((value < min || value > max) && errorInput) || error[name]) && (
                <Tooltip message={error[name] || errorInput} />
                )}

                <MaskedInput
                  name={name}
                  id={`${name}_txt`}
                  type="text"
                  ref={a}
                  value={value}
                  mask={!notMask && numberMask()}
                  onChange={(e) => {
                    const _value = parseInt(e.target.value.replace(/\D+/g, ''), 10);

                    if (Number.isNaN(_value)) return setFieldValue(name, 0);

                    return setFieldValue(name, _value);
                  }}
                  onKeyDown={(e) => {
                    const _value = parseInt(e.target.value.replace(/\D+/g, ''), 10);

                    if (testValue(e.keyCode, _value, max, 'biggerThan')) {
                      setErrorInput(`Valor máximo permitido: ${max}`);
                      return e.preventDefault();
                    }

                    if (testValue(e.keyCode, _value, min, 'lessThan')) {
                      setErrorInput(`Valor mínimo permitido: ${min}`);
                    }

                    if (Number.isNaN(_value)) return setFieldValue(name, 0);

                    if (errorInput) setErrorInput(false);

                    return true;
                  }}
                  min={min}
                  max={max}
                  step={step}
                  placeholder={placeholder}
                />

              </div>
            </div>
            <InputRangeStyled
              name={name}
              type="range"
              value={value}
              onChange={(e) => { reCalcWidth(e.target.value); return setFieldValue(name, parseInt(e.target.value, 10)); }}
              min={min}
              max={max}
              step={step}
              height={height}
              sizeBalls={sizeBalls}
            />
          </>
        )}
    </DivRangeStyled>
  );
}

InputRange.propTypes = {
  /**
   * Este atributo será o rótulo do input de valor único
   */
  label: PropTypes.string.isRequired,
  /**
   * Rever a existência deste atributo
   */  
  placeholder: PropTypes.string,
  /**
   * Atributo utilizado para identificação
   */
  name: PropTypes.string.isRequired,
  /**
   * Valor que a input deve assumir
   */
  value: PropTypes.any.isRequired,
  /** 
   * Objeto com os IDs dos campos do formulário setados como error true, essa propriedade é obrigatória para o funcionamento do formik
   */
  error: PropTypes.object.isRequired,
  /**
   * Atributo utilizado no Formik para atribuir valores dentro do Formik
   */
  setFieldValue: PropTypes.func.isRequired,
    /**
   * Atributo responsável por definir qual será o tipo de máscara <b>monthly (meses)</b> ou <b>currency (valores em dinheiro)</b>
   */
  format: PropTypes.string.isRequired,
  /**
   * Valor mínimo do range
   */
  min: PropTypes.number.isRequired,
  /**
   * Valor máximo do range
   */
  max: PropTypes.number.isRequired,
  /**
   * Intervalo de valores da bolinha
   */
  step: PropTypes.number,
  /**
   * Um Objeto de rótulos para multiplas bolinhas com chaves definidas para <b>singular</b>, <b>plural</b> e <b>last</b>
   * Onde: </br> - singular é para caso de valor único [opcional] </br> - plural para caso de valor acima de 1 [opcional]
   * </br> - last para último valor do range [opcional]
   */
  labels: PropTypes.object,
    /**
   * Atributo responsável por <b>NÃO</b> atribuir máscara de valores, <b>obs:</b> o tipo de máscara é definido pela propriedade <b>format</b> 
   */
  notMask: PropTypes.bool,
  /**
   * Define se o range é de uma bolinha ou duas.
   */
  multiple: PropTypes.bool,
  /**
   * Propriedade que define a altura da barra.
   */  
  height: PropTypes.string,
  /**
   * Definição de tamanho dos dots (bolinhas).
   */
  sizeBalls: PropTypes.string,
  /**
   * Função disparada no evento de mudança no valor do input
   */
  handleChange: PropTypes.func,
};

InputRange.defaultProps = {
  label: null,
  step: 100,
  placeholder: null,
  labels: {},
  notMask: false,
  multiple: false,
  height: '8px',
  sizeBalls: '21px',
  handleChange: () => {},
};

export default memo(InputRange);
