/* eslint react/no-array-index-key: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const BlueHeaderTableStyled = styled.table`
  width: 100%;
  border-spacing: 0px;
  border-collapse: collapse;

  & thead tr {
    text-transform: uppercase;
    background-color: #0C173B;
    color: #fff;
    font: Bold 12px/14px Arial;
  }

  & thead tr th {
    text-align: center;
    border: 0px;
    padding: 5px;
  }

  & thead tr th:first-of-type {
    border-top-left-radius: 20px;
    border-bottom-left-radius: 20px;
  }

  & thead tr th:last-of-type {
    border-top-right-radius: 20px;
    border-bottom-right-radius: 20px;
  }

  & tbody:before {
    height: 3px;
    display: table-row;
    content: '';
  }

  & tbody tr {
    border-bottom: 2px solid #F1F1F1;
  }

  & tbody tr:last-of-type {
    border-bottom: none;
  }

  & tbody tr:hover {
    background-color: #F1F1F1;
  }

  & tbody tr td {
    padding: 8px;
    text-align: center;
    vertical-align: center;
    color: #444444;
  }
`;

export default function BlueHeaderTable(props) {
  const {
    className, children,
  } = props;

  return (
    <BlueHeaderTableStyled className={className}>
      {children}
    </BlueHeaderTableStyled>
  );
}

BlueHeaderTable.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

BlueHeaderTable.defaultProps = {
  className: null,
};
