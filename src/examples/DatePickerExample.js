/* eslint-disable react/destructuring-assignment */
/* eslint react/no-array-index-key: 0 */

import React from 'react';
import { DatePicker, InputWithButton } from '../lib';
import Modal from './Modal';
import { Formik } from 'formik';

class DatePickerExample extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: false,
      selectedDate1: '31/02/1999',
      selectedDate2: '1997-fevereiro-06',
    };
  }

  // eslint-disable-next-line react/no-access-state-in-setstate
  handleModal = () => this.setState({ visibleModal: !this.state.visibleModal })

  render() {
    const { selectedDate1, selectedDate2 } = this.state;
    return (
      <div>
        <h3>Date Picker:</h3>
        <h4>Using a Text Input:</h4>
        <Formik
          initialValues={{date:undefined}}
        >
          
        {({
         values,
         errors,
         touched,
         setFieldError,
         setFieldTouched,
         setFieldValue
       }) => (
         <>
  
         <DatePicker  touched={touched} handleBlur={() => setFieldTouched('date',true,false)}  errors={errors} id='date' setFieldError={setFieldError} initYear={2020} endYear={2018} handleChange={(v) => setFieldValue('date', v)} value={values.date}  padZeros placeholder="DD/MM/YYYY" displayFormat="DD/MM/YYYY" valueFormat="DD MM YYYY" mask={[/[0-3]/, /\d/, '/', /[0-1]/, /\d/, '/', /[1-2]/, /\d/, /\d/, /\d/]} />
        </>
       )}
        </Formik>

        <p style={{ textAlign: 'right' }}>
          Selected date:
          {selectedDate1}
        </p>
        <h4>Using a Date Input:</h4>
        <DatePicker disabled handleChange={(newdate) => { this.setState({ selectedDate2: newdate }); console.log(newdate); }} value={selectedDate2} dateInput padZeros={false} placeholder="always locale default" displayFormat="always locale default" valueFormat="YYYY-MMMM-DD" />
        <p style={{ textAlign: 'right' }}>
          Selected date:
          {selectedDate2}
        </p>
        <hr />
        Datepicker dentro de um Modal
        <button type="button" onClick={() => this.handleModal()}>Simulação do DatePicker dentro do modal</button>

        <Modal visible={this.state.visibleModal} handleModal={() => this.handleModal()}>
          <DatePicker dynamicAlign handleChange={(newdate) => { this.setState({ selectedDate2: newdate }); console.log(newdate); }} value={selectedDate2} dateInput padZeros={false} placeholder="always locale default" displayFormat="always locale default" valueFormat="YYYY-MMMM-DD" />

          <div style={{
            height: '400px', width: '30px', backgroundColor: 'red', margin: '15px 0',
          }}
          />
          <DatePicker dynamicAlign handleChange={(newdate) => { this.setState({ selectedDate2: newdate }); console.log(newdate); }} value={selectedDate2} dateInput padZeros={false} placeholder="always locale default" displayFormat="always locale default" valueFormat="YYYY-MMMM-DD" />
        </Modal>
      </div>
    );
  }
}

export default DatePickerExample;
