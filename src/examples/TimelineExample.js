import React from 'react';
import {Timeline} from '../lib';
function TimelineExample() {
  var today = new Date();
  return (
      <div style={{fontFamily:'arial', margin: '0px'}}>
          <h3>Timeline:</h3>
          <Timeline events={[{text: 'teste realizado', user: 'Lucas', date: today},{text: 'teste realizado', user: 'Lucas', date: today}]} date="date" text="text" user="user" />
      </div>
  );
}

export default TimelineExample;