import React from 'react';
import { WhiteHeaderTable, FilteringHeader } from '../lib';

export default function WhiteHeaderTableExample() {
  return (
    <div>
      <h3>WhiteHeaderTable with FilteringHeader:</h3>
      <WhiteHeaderTable>
        <thead>
          <tr>
            <th />
            <FilteringHeader onClickUp={() => { alert('Filtering up'); }} onClickDown={() => { alert('Filtering down'); }} onReset={() => { alert('Reset'); }}>
              Arquivo
            </FilteringHeader>
            <FilteringHeader activeFilter="up" onClickUp={() => { alert('Filtering up'); }} onClickDown={() => { alert('Filtering down'); }} onReset={() => { alert('Reset'); }}>
              Categoria
            </FilteringHeader>
            <FilteringHeader activeFilter="down" onClickUp={() => { alert('Filtering up'); }} onClickDown={() => { alert('Filtering down'); }} onReset={() => { alert('Reset'); }}>
              Tipo de Documento
            </FilteringHeader>
            <FilteringHeader onClickUp={() => { alert('Filtering up'); }} onClickDown={() => { alert('Filtering down'); }} onReset={() => { alert('Reset'); }}>
              Status
            </FilteringHeader>
            <th>
              Ações
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              value 0
            </td>
            <td>
              value 1
            </td>
            <td>
              value 2
            </td>
            <td>
              value 3
            </td>
            <td>
              value 4
            </td>
            <td>
              value 5
            </td>
          </tr>
          <tr>
            <td>
              value 6
            </td>
            <td>
              value 7
            </td>
            <td>
              value 8
            </td>
            <td>
              value 9
            </td>
            <td>
              value 10
            </td>
            <td>
              value 11
            </td>
          </tr>
          <tr>
            <td>
              value 12
            </td>
            <td>
              value 13
            </td>
            <td>
              value 14
            </td>
            <td>
              value 15
            </td>
            <td>
              value 16
            </td>
            <td>
              value 17
            </td>
          </tr>
        </tbody>
      </WhiteHeaderTable>
    </div>
  );
}
