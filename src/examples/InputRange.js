/* eslint-disable no-alert */
import React, { memo } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { InputRange } from '../lib';


function InputRangeExample() {
  return (
    <div>
      <h3>InputRange:</h3>
      <p>Warning: Using formik</p>
      <Formik
        initialValues={{
          mortgageValue: 40000,
          realtyPrice: 100000,
          term: 12,
          period: [0, 0],
          period10: [50, 80],
        }}
        onSubmit={(values) => {
          console.log(values);
        }}
        validationSchema={Yup.object().shape({
        })}
      >
        {({
          values,
          errors,
          touched,
          handleSubmit,
          setFieldValue,
          handleChange,
        }) => (
          <form onSubmit={handleSubmit} noValidate>
            <InputRange
              name="realtyPrice"
              label="Valor do imóvel:"
              handleChange={handleChange}
              setFieldValue={setFieldValue}
              value={values.realtyPrice}
              error={errors}
              touched={touched}
              min={100000}
              max={20000000}
              format="currency"
            />

            <InputRange
              name="mortgageValue"
              label="Valor do financiamento:"
              handleChange={handleChange}
              setFieldValue={setFieldValue}
              value={values.mortgageValue}
              error={errors}
              touched={touched}
              percentage={0.85}
              min={40000}
              max={0.85 * values.realtyPrice}
              format="currency"
            />

            <InputRange
              name="term"
              label="Prazo para pagamento:"
              handleChange={handleChange}
              setFieldValue={setFieldValue}
              value={values.term}
              error={errors}
              touched={touched}
              min={12}
              max={360}
              step={1}
              format="monthly"
            />

            <p>Período:</p>
            <InputRange
              name="period"
              handleChange={(v) => setFieldValue('period', v)}
              value={values.period}
              error={errors}
              touched={touched}
              multiple
              min={1}
              max={6}
              step={1}
              labels={{ last: '+' }}
              notMask
              height="5px"
              sizeBalls="13px"
            />
            <br />
            <button type="button" onClick={() => setFieldValue('period', [0, 0])}>Limpar</button>
            <br />
            <p>Período de 10 em 10:</p>
            <InputRange
              name="period10"
              handleChange={(v) => setFieldValue('period10', v)}
              value={values.period10}
              error={errors}
              touched={touched}
              multiple
              min={0}
              max={360}
              step={10}
              labels={{ singular: 'mês', plural: 'meses', last: '+ meses' }}
              notMask
            />
            <br />
            <button type="submit">Enviar</button>
          </form>
        )}
      </Formik>
    </div>
  );
}

export default memo(InputRangeExample);
