import React from 'react';

export default function Modal(props) {
  const { visible, children, handleModal } = props;
  return (
    <div style={{
      display: visible ? 'flex' : 'none',
      backgroundColor: 'rgba(115, 114, 105, 0.5)',
      position: 'fixed',
      top: 0,
      width: '100%',
      height: '100%',
      zIndex: 9,
      left: 0,
      justifyContent: 'center',
      alignItems: 'center',
    }}
    >

      <div style={{
        borderRadius: '8px',
        backgroundColor: 'white',
        position: 'fixed',
        width: '50vw',
        padding: '15px',
        overflow: 'auto',
        maxHeight: '500px',
      }}
      >
        <button
          type="button"
          style={{
            backgroundColor: 'red', color: 'white', border: 'none', position: 'fixed',
          }}
          onClick={handleModal}
        >
          Fechar modal
        </button>

        <h5>
          O objetivo desse modal é apenas simular o comportamento de alguns componentes dentro
          de um modal para teste e, não necessariamente deixá-lo disponível para uso oficial.
          Além disso, o motivo desse texto aqui é gerar scroll e deixar um recado à Família Dev. CH:
          <u> 6 São top</u>
        </h5>
        <hr />
        <div>
          {children}
        </div>
      </div>

    </div>
  );
}
