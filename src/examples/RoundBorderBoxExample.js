/* eslint react/no-array-index-key: 0 */

import React from 'react';
import { RoundBorderBox } from '../lib';

export default function RoundBorderBoxExample() {
  return (
    <div>
      <h3>Round Border Box:</h3>
      <div style={{ backgroundColor: '#EFEFEF', padding: '1rem' }}>
        <RoundBorderBox>
          <p style={{ textAlign: 'right' }}>
          Hey! Hey Listen!
          </p>
        </RoundBorderBox>
        <p>Com padding e position diferentes:</p>
        <RoundBorderBox padding="2rem 2rem" position="relative">
          <p style={{ textAlign: 'center' }}>
          Hey! Hey Listen!
          </p>
          <p style={{ position: 'absolute', top: '-10%', left: '5%' }}>Hey!</p>
          <p style={{ position: 'absolute', top: '30%', left: '15%' }}>Hey!</p>
          <p style={{ position: 'absolute', bottom: '10%', left: '30%' }}>Hey!</p>
          <p style={{ position: 'absolute', bottom: '40%', left: '25%' }}>Hey!</p>
          <p style={{ position: 'absolute', top: '30%', right: '30%' }}>Hey!</p>
          <p style={{ position: 'absolute', top: '10%', right: '40%' }}>Hey!</p>
          <p style={{ position: 'absolute', bottom: '20%', right: '10%' }}>Hey!</p>
          <p style={{ position: 'absolute', bottom: '50%', right: '20%' }}>Hey!</p>
        </RoundBorderBox>
      </div>
    </div>
  );
}
