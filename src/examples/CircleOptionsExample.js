import React, { useState, } from 'react';
import CircleOptions from '../lib/CircleOptions/CircleOptions';
import ModalConfirmation from '../lib/ModalConfirmation/ModalConfirmation'

export default function CircleOptionsExample() {
  const [visible, setVisible] = useState(false);
  const [confirmModal, setConfirmModal] = useState(false);
  const [options, setOptions] = useState([
    { label: 'Luciana Rafael' },
    { label: 'Bjorn Jarnisida' },
    { label: 'Lagertha Karen' }
  ]);

  const names = ['Felipe Fleury', 'Luciana Oliveira', 'Karen Ochoa', 'Willen Goulart', 'Rafael Mota']

  return (
    <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'flex-start', flexWrap: 'wrap', flexDirection: 'column', backgroundColor: 'white', width: '500px', padding: '17px' }}>
      <h3 style={{ color: 'darkblue' }}>CircleOptions:</h3>
      {confirmModal !== false && (
        <div style={{ border: '1px solid orange', backgroundColor: 'white', color: 'black', width: '300px', height: 'auto', padding: '45px 10px 45px 10px', position: 'absolute', left: '38%', fontSize: '18px', textAlign: 'center', borderRadius: '7px', zIndex: '100' }}>
          <p style={{ fontSize: '17px', textAlign: 'center', margin: '0 auto', fontFamily: 'arial' }}>Deseja excluir este item?</p>
          <button onClick={() => { setOptions(confirmModal); return setConfirmModal(false) }} style={{ marginRight: '7px', borderRadius: '11px', width: '88px', backgroundColor: '#E26226', border: 'none', color: 'white', padding: '3px', marginTop: '2rem' }}>Sim</button>
          <button onClick={() => setConfirmModal(false)} style={{ borderRadius: '11px', width: '88px', backgroundColor: '#E26226', border: 'none', color: 'white', padding: '3px', marginTop: '2rem' }}>Não</button>
        </div>
      )}

      <CircleOptions
        items={options}
        maxItems={10}
        activeItem={1}
        onAddAction={(items) => setOptions([...items, { label: names[Math.floor(Math.random() * Math.floor(5))] }])}
        onDeleteAction={({ items }) => setOptions(items)}
        confirmDelete={({ items }) => setConfirmModal(items)}
      />
    </div>

  );
}