import React from 'react';
import { BlueHeaderTable, ColoredCircle, FilteringHeader } from '../lib';

export default function BlueHeaderTableExample() {
  return (
    <div>
      <h3>BlueHeaderTable:</h3>
      <BlueHeaderTable>
        <thead>
          <tr>
            <FilteringHeader onClickUp={() => {}} onClickDown={() => {}} onReset={() => {}}>
              Lead
            </FilteringHeader>
            <th>
              Contato
            </th>
            <th>
              Financiamento
            </th>
            <th>
              Produto
            </th>
            <th>
              Tentativas
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              value 0
            </td>
            <td>
              value 1
            </td>
            <td>
              value 2
            </td>
            <td>
              value 3
            </td>
            <td>
              <div style={{ margin: '0 auto', width: 'fit-content' }}>
                <ColoredCircle />
              </div>
            </td>
          </tr>
          <tr>
            <td>
              value 5
            </td>
            <td>
              value 6
            </td>
            <td>
              value 7
            </td>
            <td>
              value 8
            </td>
            <td>
              <div style={{ margin: '0 auto', width: 'fit-content' }}>
                <ColoredCircle color="#FFCC00" />
              </div>
            </td>
          </tr>
          <tr>
            <td>
              value 10
            </td>
            <td>
              value 11
            </td>
            <td>
              value 12
            </td>
            <td>
              value 13
            </td>
            <td>
              <div style={{ margin: '0 auto', width: 'fit-content' }}>
                <ColoredCircle color="#EB0000" />
              </div>
            </td>
          </tr>
        </tbody>
      </BlueHeaderTable>
    </div>
  );
}
