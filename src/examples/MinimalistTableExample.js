import React from "react";
import { MinimalistTable } from '../lib';

export default function MinimalistTableExample() {
  const headTable = ["Título 1", "Título 2", "Título 3", "Título 4"];

  return (
    <div>
      <h3>MinimalistTable:</h3>
      <MinimalistTable
        boldHeadTitle
        dividerColor={"red"}
        fontColorHead={'green'}
      >
        <thead>
          <tr>
            {headTable && headTable.map((item) => <th key={item}>{item}</th>)}
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>value 0</td>
            <td>value 1</td>
            <td>value 2</td>
            <td>value 3</td>
          </tr>
          <tr>
            <td>value 5</td>
            <td>value 6</td>
            <td>value 7</td>
            <td>value 8</td>
          </tr>
          <tr>
            <td>value 10</td>
            <td>value 11</td>
            <td>value 12</td>
            <td>value 13</td>
          </tr>
        </tbody>
      </MinimalistTable>
    </div>
  );
}
