/* eslint react/no-array-index-key: 0 */

import React from 'react';
import { ProgressBar } from '../lib';

class ProgressBarExample extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      percentage: 0,
    };
  }

  render() {
    return (
      <div>
        <ProgressBar percentage={this.state.percentage} duration={2000} />
        <button type="button" onClick={() => this.setState((prevState) => ({ percentage: prevState.percentage + 10 }))}>Add percentage</button>
        <button type="button" onClick={() => this.setState((prevState) => ({ percentage: prevState.percentage - 10 }))}>Remove percentage</button>
      </div>
    );
  }
}

export default ProgressBarExample;
