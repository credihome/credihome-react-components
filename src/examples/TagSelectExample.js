/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/destructuring-assignment */
/* eslint react/no-array-index-key: 0 */

import React from "react";
import { Formik } from "formik";
import styled from "styled-components";
import * as Yup from "yup";
import { Tag, TagSelect, Select } from "../lib";
import Modal from "./Modal";

const Form = styled.form`
  & > div.variousSelects {
    display: flex;
  }
`;

class TagSelectExample extends React.Component {
  constructor(props) {
    super(props);
    const myFunc = (selected) => {
      alert("Fruta selecionada: " + selected.label);
    };
    this.state = {
      visibleModal: false,
      values: ["banana", "caqui"],
      options: [
        { label: "Banana", key: "banana", onOptionButtonClick: myFunc },
        { label: "Caqui", key: "caqui", onOptionButtonClick: myFunc },
        { label: "Kiwi", key: "Kiwi", onOptionButtonClick: myFunc },
        { label: "Banana com manga", key: "banana-manga" },
        { label: "Uva", key: "uva" },
        { label: "Super salada de frutas vermelhas", key: "frutas-vermelhas" },
        {
          label: "Super duper salada de frutas vermelhas com leite condensado",
          key: "frutas-vermelhas-leite",
        },
      ],
    };
  }

  exampleAsyncFilter = async (v) => {
    console.log(v);
    await console.log("Await working");
    return [
      { label: "banana", key: "banana" },
      { label: "banana alterada", key: "banana-alterada" },
      { label: "caqui", key: "caqui" },
    ];
  };

  handleModal = () => this.setState({ visibleModal: !this.state.visibleModal });

  render() {
    return (
      <div>
        <h3>Tag Select:</h3>
        <h4>Tag:</h4>
        <Tag
          text="hello"
          onClick={() => {
            alert("I don't wanna die");
          }}
        />
        <h4>Tag Custom Icon:</h4>
        <Tag
          text="hello"
          tagIcon={"chicon-trash"}
          onClick={() => {
            alert("I don't wanna be archived");
          }}
        />
        <h4>Input with tag:</h4>
        <TagSelect
          options={this.state.options}
          value={this.state.values}
          handleChange={(value) => {
            this.setState({ values: value });
          }}
        />
        <p>{this.state.values}</p>
        <button type="button" onClick={() => this.setState({ values: [] })}>
          Reset
        </button>
        <h4>Input without tag:</h4>
        <p>ReadOnly:</p>
        <Select
          handleChange={(selected) => {
            console.log(selected);
          }}
          options={this.state.options}
          value="banana-manga"
          readOnly
          onButtonClick={(item) => {
            alert(item.label);
          }}
        />
        <p>Select ReadOnly Disabled:</p>
        <Select
          disabled
          handleChange={(selected) => {
            console.log(selected);
          }}
          options={this.state.options}
          value="banana-manga"
          readOnly
        />
        <b>Formik:</b>
        <Formik
          enableReinitialize
          initialValues={{
            simpleSelect: undefined,
          }}
          onSubmit={(values) => alert(JSON.stringify(values))}
          validationSchema={Yup.object().shape({
            simpleSelect: Yup.string().required("REQUERIDO").matches("caqui", "ESCOLHA CAQUI"),
          })}
        >
          {({
            values,
            errors,
            touched,
            handleSubmit,
            setFieldValue,
            isValidating,
          }) => (
            <Form onSubmit={handleSubmit} noValidate>
              {errors.simpleSelect}
              <p>Tag Select in formik:</p>
              <TagSelect
                id="tagSelect"
                errors={errors}
                isValidating={isValidating}
                touched={touched}
                options={this.state.options}
                value={values.tagSelect}
                handleChange={(v) => setFieldValue("tagSelect", v)}
              />
              <p>Mini selects</p>
              <div className="variousSelects">
                <Select
                  handleChange={(v) => setFieldValue("simpleSelect", v)}
                  options={this.state.options}
                  value={values.simpleSelect}
                  isValidating={isValidating}
                  id="simpleSelect"
                  errors={errors}
                  touched={touched}
                  maxHeight="150px;"
                  noOptionString={"Welp, there's nothing here!"}
                  allowOverflow
                  hasHideTooltip
                />
                <Select
                  handleChange={(v) => setFieldValue("simpleSelect2", v)}
                  options={this.state.options}
                  value={values.simpleSelect2}
                  isValidating={isValidating}
                  id="simpleSelect2"
                  errors={errors}
                  touched={touched}
                  maxHeight="150px;"
                  noOptionString={"Welp, there's nothing here!"}
                  addOnEnter
                  noUp
                />
                <Select
                  handleChange={(v) => setFieldValue("simpleSelect", v)}
                  options={this.state.options}
                  value={values.simpleSelect}
                  isValidating={isValidating}
                  id="simpleSelect"
                  errors={errors}
                  touched={touched}
                  maxHeight="150px;"
                  noOptionString={"Welp, there's nothing here!"}
                />
              </div>
              <p>Read n Write:</p>
              <Select
                handleChange={(v) => setFieldValue("simpleSelect", v)}
                options={this.state.options}
                value={values.simpleSelect}
                isValidating={isValidating}
                id="simpleSelect"
                errors={errors}
                touched={touched}
                maxHeight="150px;"
                noOptionString={"Welp, there's nothing here!"}
              />
              <button type="submit">Simulate submit</button>
              <p>Async Filter</p>
              <Select
                id="asyncSelect"
                handleChange={(v) => setFieldValue("asyncSelect", v)}
                asyncHandleFilter={(v) => this.exampleAsyncFilter(v)}
                options={this.state.options}
                value={values.asyncSelect}
                errors={errors.simpleSelect}
                touched={touched.simpleSelect}
                noOptionString={"Welp, there's nothing here!"}
              />
              <button type="submit">Simulate submit</button>
              <hr />
              <h5>BOTÃO PARA ABRIR MODAL E SIMULAR COMPORTAMENTO DO ITEM</h5>
              <button type="button" onClick={() => this.handleModal()}>
                Simulação de select dentro do modal
              </button>
              <Modal
                visible={this.state.visibleModal}
                handleModal={() => this.handleModal()}
              >
                <Select
                  handleChange={(v) => setFieldValue("simpleSelect", v)}
                  options={this.state.options}
                  value={values.simpleSelect}
                  id="simpleSelect"
                  errors={errors}
                  touched={touched}
                  maxHeight="150px;"
                  dynamicAlign
                  noOptionString={"Welp, there's nothing here!"}
                />
                <div
                  style={{
                    height: "400px",
                    width: "30px",
                    backgroundColor: "red",
                    margin: "15px 0",
                  }}
                />
                <Select
                  handleChange={(v) => setFieldValue("otherSelect", v)}
                  options={this.state.options}
                  value={values.otherSelect}
                  id="otherSelect"
                  errors={errors}
                  touched={touched}
                  maxHeight="150px;"
                  dynamicAlign
                  noOptionString={"Welp, there's nothing here!"}
                />
              </Modal>
              <small>
                <hr />
                ps* O comportamento do select fora do modal pode ser avalidado
                por qualquer um select anterior pois é um comportamento default
                deles agora
              </small>
            </Form>
          )}
        </Formik>
      </div>
    );
  }
}

export default TagSelectExample;
