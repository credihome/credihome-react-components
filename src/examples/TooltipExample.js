import React, { useState } from 'react';
import { Tooltip } from '../lib';

export default function TooltipExample() {
  const [type, setType] = useState('error');
  const [typeOptions] = useState(['error', 'warning', 'undefined']);

  return (
    <div>
      <h3>Tooltip</h3>
      <text>Select a type demo: </text>
      {typeOptions.map((item) => (
        <button type="button" onClick={() => setType(item)} key={item}>
          {item}
        </button>
      ))}
      <Tooltip message={type} type={type} visibleTooltip />
      <p>*Tooltip default type: Error</p>
      <hr />
      <br />
      <text>A custom Tooltip demo </text>
      <Tooltip message="Custom Tooltip" backgroundColor="green" textColor="yellow" visibleTooltip />
    </div>
  );
}
